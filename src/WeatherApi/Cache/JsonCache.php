<?php

namespace WeatherApi\Cache;

/**
 * Class FileCache
 * @version 1.0.1
 * @author Guilherme Santos - guilhermedossantos91@gmail.com
 * @package WeatherApi\Cache
 */
final class JsonCache
{

    /**
     * @var string
     */
    const SUFFIX = "-cache.json";

    /**
     * @var string
     */
    private $dir;

    /**
     * @param string $dir
     * @throws \Exception
     */
    public function __construct($dir)
    {
        if (!is_dir($dir)) {
            throw new \Exception("Cache path not found");
        }

        $this->dir = $dir;
    }

    /**
     * @param $id $hash
     * @throws \Exception
     * @return string
     */
    private function getPathFile($id)
    {
        return $this->dir . DIRECTORY_SEPARATOR . $id . self::SUFFIX;
    }

    /**
     * @param string $id
     * @param string $data
     * @throws \Exception
     */
    public function save($id, $data = null)
    {
        $path = $this->getPathFile($id);
        $file = @fopen($path, "w+");

        if (!is_string($data)) {
            $data = json_encode($data);
        }

        if ($file !== false) {
            if (is_string($data)) {
                @fwrite($file, $data);
            }
            @fclose($file);
        }
        unset($file);

        Wait::load($path)->remove();
    }


    /**
     * @param string $id
     * @return null|\stdClass
     */
    public function fetch($id)
    {
        $path = $this->getPathFile($id);

        if (!file_exists($path)) {
            return null;
        }
        if (($data = @file_get_contents($path)) === false) {
            return null;
        }
        if (($data = @json_decode($data)) === false || is_null($data)) {
            return null;
        }

        return $data;
    }

    /**
     * @param string $id
     * @param int $time
     * @return bool
     */
    public function isExpired($id, $time)
    {
        $file = $this->getPathFile($id);

        if (Wait::load($file)->waiting()) {
            return false;
        }

        if (!file_exists($file) || (time() - @filemtime($file)) > $time) {
            Wait::load($file)->generate();
            return true;
        }

        return false;
    }

    /**
     * @param mixed $id
     * @return bool
     */
    public function clear($id)
    {
        $file = $this->getPathFile($id);
        if (file_exists($file)) {
            @unlink($file);
            clearstatcache();
            if (!file_exists($file)) {
                return true;
            }
        }

        return false;
    }

    /**
     * TODO: Implements
     */
    public function clearAll()
    {
        //exec("rm -Rf {$this->dir}/*" . self::SUFFIX);
        exec("find {$this->dir} -name \"*" . self::SUFFIX . "\" -exec rm -rf {} \;&");
    }

    /**
     * @param string $id
     * @return bool
     */
    public function exists($id)
    {
        return @file_exists($this->getPathFile($id));
    }
}