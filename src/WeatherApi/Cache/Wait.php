<?php

namespace WeatherApi\Cache;

/**
 * Class FileWait
 *
 * @author  Guilherme Santos - guilhermedossantos91@gmail.com
 * @package Pasoka\Component\Cache
 */
final class Wait
{
    /**
     * Tempo limite para esperar o arquivo ser liberado
     *
     * @var int
     */
    const TIMEOUT = 30;

    /**
     * 2 minutos
     *
     * @var int
     */
    const MAX_WAIT = 120;

    /**
     * @var string
     */
    private $file;

    /**
     * @var int
     */
    private $waiting;

    /**
     * @var array
     */
    private static $instances = [];

    /**
     * @param $file
     * @throws \Exception
     */
    private function __construct($file)
    {
        $this->waiting = 0;
        $this->file = $file . ".lock";
    }

    /**
     * @param string $file
     * @return $this
     */
    public static function load($file)
    {
        $hash = md5($file);

        if (array_key_exists($hash, self::$instances) !== false) {
            return self::$instances[$hash];
        }

        return self::$instances[] = new self($file);
    }

    /**
     *
     */
    public function generate()
    {
        @fclose(@fopen($this->file, "w+"));
    }

    /**
     * @return bool
     */
    public function remove()
    {
        if (file_exists($this->file)) {
            $remove = @unlink($this->file);
            clearstatcache();
            return $remove;
        }
        return true;
    }

    /**
     *
     */
    public function wait()
    {
        if (file_exists($this->file) && $this->waiting < self::TIMEOUT) {
            sleep(1);
            $this->waiting++;
            $this->wait();
        } else {
            $this->waiting = 0;
        }
        return $this;
    }


    /**
     * @return bool
     */
    public function waiting()
    {
        $exists = @file_exists($this->file);

        if(!$exists) {
            return false;
        }

        if((time() - @filemtime($this->file)) > self::MAX_WAIT) {
            $this->remove();
            return false;
        }

        return  true;
    }

} 