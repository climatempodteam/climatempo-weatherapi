<?php

namespace WeatherApi\Error;

use Exception;

/**
 * Classe permite obter dados de erro da API ao capturar exececao
 *
 * @package WeatherApi\Error
 * @author Lucas de Oliveira <lucas.oliveira@climatempo.com.br>
 * @version 1.0.0
 */
class WeatherApiException extends Exception
{

    /**
     * @var array
     */
    private $data;

    /**
     * @param string $message
     * @param array $data
     */
    public function __construct($message = "", array $data)
    {
        parent::__construct($message);

        $this->data = $data;
    }

    /**
     * @return array
     */
    public function getDataResponse()
    {
        return $this->data;
    }
}
