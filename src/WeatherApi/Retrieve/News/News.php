<?php

namespace WeatherApi\Retrieve\News;

use WeatherApi\Retrieve\AbstractRetrieve;

/**
 * Class News
 * @package WeatherApi\Retrieve\News
 * @author Guilherme Santos <guilhermedossantos91@gmail.com>
 */
class News extends AbstractRetrieve
{
    /**
     * @param string $type
     * @param bool|false $forceUpdate
     * @return \stdClass|null
     */
    public function getCall($type, $forceUpdate = false)
    {
        $queryString = "?typeCall={$type}";

        return $this
            ->setRouter(['news', 'calls'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }

    /**
     * @param bool|false $forceUpdate
     * @return null|\stdClass
     */
    public function getFeatured($forceUpdate = false)
    {
        return $this
            ->setRouter(['news', 'featured'])
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__
                ),
                $forceUpdate
            );
    }


    /**
     * @param string $slug
     * @param bool|false $forceUpdate
     * @return mixed
     */
    public function getBySlug($slug, $forceUpdate = false)
    {
        $queryString = "?slug={$slug}";

        return $this
            ->setRouter(['news'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }


    /**
     * @param string $category
     * @param int $limit
     * @param int $paged
     * @param bool|false $forceUpdate
     * @return \stdClass|null
     */
    public function getByCategory($category, $limit, $paged, $forceUpdate = false)
    {
        $queryString = "?category={$category}&limit={$limit}&paged={$paged}";

        return $this
            ->setRouter(['news'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }

    /**
     * @param string $tag
     * @param bool|false $forceUpdate
     * @return null|\stdClass
     */
    public function getByTag($tag, $forceUpdate = false)
    {
        $queryString = "?tag={$tag}";

        return $this
            ->setRouter(['news'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }

    /**
     * @param int $idType
     * @param int $limit
     * @param int $current
     * @param bool|false $forceUpdate
     * @return \stdClass|null
     */
    public function getByType($idType, $limit = 5, $current = 0, $forceUpdate = false)
    {
        $queryString = "?typeNews={$idType}&limit={$limit}&current={$current}";

        return $this
            ->setRouter(['news'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }


    /**
     * @param int $idContent
     * @param bool|false $forceUpdate
     * @return \stdClass|null
     */
    public function getByIdContent($idContent, $forceUpdate = false)
    {
        $queryString = "?idconteudo={$idContent}";

        return $this
            ->setRouter(['news'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }


    /**
     * @param string[] $categories
     * @param int $limit
     * @param int $paged
     * @param bool|false $forceUpdate
     * @return \stdClass|null
     */
    public function getByCategories(
        array $categories,
        $limit = null,
        $paged = null,
        $forceUpdate = false
    )
    {
        $queryString = '';

        foreach ($categories as $key => $value) {
            if ($key == 0) {
                $queryString .= '?category[]=' . $value;
            } else {
                $queryString .= '&category[]=' . $value;
            }
        }

        if (!is_null($limit)) {
            $queryString .= "&limit={$limit}";
        }
        if (!is_null($paged)) {
            $queryString .= "&paged={$paged}";
        }

        return $this
            ->setRouter(['news'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }

    /**
     * @param string[] $tags
     * @param int $limit
     * @param int $paged
     * @param bool $forceUpdate
     * @return mixed
     */
    public function getByTags(
        array $tags,
        $limit = null,
        $paged = null,
        $forceUpdate = false
    )
    {
        $queryString = '';
        foreach ($tags as $key => $value) {
            if ($key == 0) {
                $queryString .= '?tag[]=' . $value;
            } else {
                $queryString .= '&tag[]=' . $value;
            }
        }

        if (!is_null($limit)) {
            $queryString .= "&limit={$limit}";
        }
        if (!is_null($paged)) {
            $queryString .= "&paged={$paged}";
        }

        return $this
            ->setRouter(['news'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }

    /**
     * @param string[] $categories
     * @param string[] $tags
     * @param int $limit
     * @param int $paged
     * @param bool|false $forceUpdate
     * @return mixed
     */
    public function getByCategoriesAndTags(
        array $categories,
        array $tags,
        $limit = null,
        $paged = null,
        $forceUpdate = false
    )
    {
        $queryString = '';
        foreach ($tags as $key => $value) {
            if ($key == 0) {
                $queryString .= '?tag[]=' . $value;
            } else {
                $queryString .= '&tag[]=' . $value;
            }
        }

        foreach ($categories as $key => $value) {
            if ($key == 0) {
                $queryString .= '&category[]=' . $value;
            } else {
                $queryString .= '&category[]=' . $value;
            }
        }

        if (!is_null($limit)) {
            $queryString .= "&limit={$limit}";
        }
        if (!is_null($paged)) {
            $queryString .= "&paged={$paged}";
        }

        return $this
            ->setRouter(['news'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }
}