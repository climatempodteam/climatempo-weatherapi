<?php

namespace WeatherApi\Retrieve\Forecast;

use WeatherApi\Retrieve\AbstractRetrieve;

/**
 * Class VisibilityForecast
 *
 * @package WeatherApi\Retrieve\Forecast
 * @author Guilherme Santos <guilhermedossantos91@gmail.com>
 * @author Gustavo Santos  <gustavo.santos@climatempo.com.br>
 * @version 1.1.0
 */
class VisibilityForecast extends AbstractRetrieve
{

    /**
     * Retorna dados de previsao de visibilidade para 15 dias por localidade
     *
     * @param number $idLocale
     * @param bool $forceUpdate
     * @param int $onlyMapServer
     * @return mixed
     */
    public function getDaily15D($idLocale, $forceUpdate = false, $onlyMapServer = 0)
    {
        $queries = [
            'idlocale' => $idLocale
        ];

        if ($onlyMapServer === 1) {
            $queries['onlyMapServer'] = $onlyMapServer;
        }

        $queryString = '?'.http_build_query($queries);

        return $this
            ->setRouter(['forecast', '15days', 'visibility'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }


    /**
     * Retorna dados horarios de previsao de visibilidade para 72 horas por localidade
     *
     * @param number $idLocale
     * @param string $timezone
     * @param bool $forceUpdate
     * @return mixed
     */
    public function getHourly72H($idLocale, $timezone = null, $forceUpdate = false)
    {
        $queryString = "?idlocale={$idLocale}";

        if (!is_null($timezone)) {
            $queryString .= "&timezone={$timezone}";
        }

        return $this
            ->setRouter(['forecast', '72hours', 'visibility'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }


    /**
     * Retorna dados de previsao de visibilidade horario por localidade e periodo
     *
     * @param number $idLocale
     * @param \DateTime $begin
     * @param \DateTime|null $end
     * @param string $timezone
     * @param bool $forceUpdate
     * @return mixed
     */
    public function getHourly(
        $idLocale,
        \DateTime $begin,
        \DateTime $end = null,
        $timezone = null,
        $forceUpdate = false
    )
    {
        $queryString = "?idlocale={$idLocale}&dateBegin={$begin->format("Y-m-d")}";

        if (!is_null($end)) {
            $queryString .= "&dateEnd={$end->format("Y-m-d")}";
        }
        if (!is_null($timezone)) {
            $queryString .= "&timezone={$timezone}";
        }

        return $this
            ->setRouter(['forecast', 'hourly', 'visibility'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }

    /**
     * Retorna dados climaticos de visibilidade por localidade e periodo
     *
     * @param number $idLocale
     * @param \DateTime $begin
     * @param \DateTime|null $end
     * @param $forceUpdate
     * @return mixed
     */
    public function getClimate($idLocale, \DateTime $begin, \DateTime $end = null, $forceUpdate = false)
    {
        $queryString = "?idlocale={$idLocale}&dateBegin={$begin->format("Y-m-d")}";

        if (!is_null($end)) {
            $queryString .= "&dateEnd={$end->format("Y-m-d")}";
        }

        return $this
            ->setRouter(['climate', 'visibility'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }


    /**
     * Salva dados de visibilidade
     * 
     * @param number $idLocale 
     * @param \DateTime $forecastDate
     * @param $visibility
     */
    public function saveVisibility(
        $idLocale,
        \DateTime $forecastDate,
        $visibility
    )
    {
        $queryString = '?' . http_build_query([
            'localeId' => $idLocale,
            'forecastDate' => $forecastDate->format('Y-m-d H:i:s'),
            'visibility' => $visibility
        ]);

        return $this
            ->setRouter(['forecast','hourly','visibility','edit'])
            ->addQueryString($queryString)
            ->request();
    }

}