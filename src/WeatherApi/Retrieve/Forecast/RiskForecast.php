<?php

namespace WeatherApi\Retrieve\Forecast;

use WeatherApi\Retrieve\AbstractRetrieve;

/**
 * Class RiskForecast
 *
 * @package WeatherApi\Retrieve\Forecast
 *
 * @author  Guilherme Santos <guilhermedossantos91@gmail.com>
 * @author  Gustavo Santos <gustavo.santos@climatempo.com.br>
 * @author  Giovanni Cruz <giovanni.cruz@climatempo.com.br>
 *
 * @version 1.2.0
 */
class RiskForecast extends AbstractRetrieve
{

    /**
     * Obtem riscos chuva, vento, granizo, geada, nevoeiro e raios por localidade e periodo
     *
     * @author Guilherme Santos <guilhermedossantos91@gmail.com>
     * @param int $idLocale
     * @param \DateTime $begin
     * @param \DateTime $end
     * @param bool|false $forceUpdate
     * @return \stdClass
     */
    public function getDaily(
        $idLocale,
        \DateTime $begin,
        \DateTime $end = null,
        $forceUpdate = false
    ) {
        $queryString = "?idlocale={$idLocale}&dateBegin={$begin->format("Y-m-d")}";

        if (!is_null($end)) {
            $queryString .= "&dateEnd={$end->format("Y-m-d")}";
        }

        return $this
            ->setRouter(['forecast', 'period', 'risk'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }


    /**
     * Obtem riscos chuva, vento, granizo, geada, nevoeiro e raios por localidade para 15 dias
     *
     * @author Guilherme Santos <guilhermedossantos91@gmail.com>
     * @param int $idLocale
     * @param bool|false $forceUpdate
     * @param int $onlyMapServer
     * @return \stdClass
     */
    public function getDaily15D(
        $idLocale,
        $forceUpdate = false,
        $onlyMapServer = 0
    ) {
        $begin = new \DateTime("now");
        $end = new \DateTime('NOW + 14days');

        $queries = [
            'idlocale' => $idLocale,
            'dateBegin' => $begin->format("Y-m-d"),
            'dateEnd' => $end->format("Y-m-d")
        ];

        if ($onlyMapServer == 1) {
            $queries['onlyMapServer'] = $onlyMapServer;
        }

        $queryString = '?'.http_build_query($queries);

        return $this
            ->setRouter(['forecast', 'period', 'risk'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }

    /**
     * Retorna dados de risco geolocalizado
     *
     * @author  Gustavo Santos <gustavo.santos@climatempo.com.br>
     *
     * @param $latitude
     * @param $longitude
     * @param \DateTime $begin
     * @param \DateTime $end
     * @param bool $forceUpdate
     * @return null|\stdClass
     */
    public function getDailyGeo($latitude, $longitude, \DateTime $begin, \DateTime $end, $forceUpdate = false)
    {
        $queryString = "?" .http_build_query([
                'latitude' => $latitude,
                'longitude' => $longitude,
                'dateBegin' => $begin->format("Y-m-d"),
                'dateEnd' => $end->format("Y-m-d")
            ]);

        return $this
            ->setRouter(['forecast', 'period', 'risk'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }
}
