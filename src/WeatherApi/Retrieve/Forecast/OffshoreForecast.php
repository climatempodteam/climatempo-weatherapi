<?php

namespace WeatherApi\Retrieve\Forecast;

use WeatherApi\Retrieve\AbstractRetrieve;

/**
 * Class MoonForecast
 *
 * Classe para obter dados de previsao para localidades de offshore
 *
 * @package WeatherApi\Retrieve\Forecast
 * @author Guilherme Santos <guilhermedossantos91@gmail.com>
 * @version 1.2.0
 */
class OffshoreForecast extends AbstractRetrieve
{

    /**
     * Ativa dados do ultimo processamento e desativa dados dos
     * processamentos anteriores para a variavel de onda.
     *
     * @author Guilherme Santos <guilhermedossantos91@gmail.com>
     */
    public function activeWave()
    {
        return $this
            ->setRouter(['forecast', 'offshore', 'hourly', 'wave', 'active'])
            ->manageCache($this->formatCacheName(__METHOD__), true);
    }

    /**
     * Ativa dados do ultimo processamento e desativa dados dos
     * processamentos anteriores para a variavel de vento.
     *
     * @author Guilherme Santos <guilhermedossantos91@gmail.com>
     */
    public function activeWind()
    {
        return $this
            ->setRouter(['forecast', 'offshore', 'hourly', 'wind', 'active'])
            ->manageCache($this->formatCacheName(__METHOD__), true);
    }

    /**
     * Ativa dados do ultimo processamento e desativa dados dos
     * processamentos anteriores para a variavel de chuva.
     *
     * @author Guilherme Santos <guilhermedossantos91@gmail.com>
     */
    public function activeRain()
    {
        return $this
            ->setRouter(['forecast', 'offshore', 'hourly', 'rain', 'active'])
            ->manageCache($this->formatCacheName(__METHOD__), true);
    }

    /**
     * Ativa dados do ultimo processamento e desativa dados dos
     * processamentos anteriores para a variavel de temperatura.
     *
     * @author Guilherme Santos <guilhermedossantos91@gmail.com>
     */
    public function activeTemperature()
    {
        return $this
            ->setRouter(['forecast', 'offshore', 'hourly', 'temperature', 'active'])
            ->manageCache($this->formatCacheName(__METHOD__), true);
    }

    /**
     * Ativa dados do ultimo processamento e desativa dados dos
     * processamentos anteriores para a variavel de icone periodo.
     *
     * @author Guilherme Santos <guilhermedossantos91@gmail.com>
     */
    public function activeIcon()
    {
        return $this
            ->setRouter(['forecast', 'offshore', 'hourly', 'icon', 'active'])
            ->manageCache($this->formatCacheName(__METHOD__), true);
    }

    /**
     * Ativa dados do ultimo processamento e desativa dados dos
     * processamentos anteriores para a variavel de pressao.
     *
     * @author Guilherme Santos <guilhermedossantos91@gmail.com>
     */
    public function activePressure()
    {
        return $this
            ->setRouter(['forecast', 'offshore', 'hourly', 'pressure', 'active'])
            ->manageCache($this->formatCacheName(__METHOD__), true);
    }

    /**
     * Ativa dados do ultimo processamento e desativa dados dos
     * processamentos anteriores para a variavel de umidade.
     *
     * @author Guilherme Santos <guilhermedossantos91@gmail.com>
     */
    public function activeHumidity()
    {
        return $this
            ->setRouter(['forecast', 'offshore', 'hourly', 'humidity', 'active'])
            ->manageCache($this->formatCacheName(__METHOD__), true);
    }

    /**
     * Ativa dados do ultimo processamento e desativa dados dos
     * processamentos anteriores para a variavel de swell e windsea.
     *
     * @author Guilherme Santos <guilhermedossantos91@gmail.com>
     */
    public function activeSwellwindsea()
    {
        return $this
            ->setRouter(['forecast', 'offshore', 'hourly', 'swellwindsea', 'active'])
            ->manageCache($this->formatCacheName(__METHOD__), true);
    }

    /**
     * Ativa dados do ultimo processamento e desativa dados dos
     * processamentos anteriores para a variavel de visibilidade.
     *
     * @author Guilherme Santos <guilhermedossantos91@gmail.com>
     */
    public function activeVisibility()
    {
        return $this
            ->setRouter(['forecast', 'offshore', 'hourly', 'visibility', 'active'])
            ->manageCache($this->formatCacheName(__METHOD__), true);
    }


    /**
     * Retorna dados horarios de onda para localidades de offshore por periodo
     *
     * @author Guilherme Santos <guilhermedossantos91@gmail.com>
     * @author Michel Araujo <michelaraujopinto@gmail.com>
     * @param int $idLocale id da localidade
     * @param \DateTime $begin data inicial da busca
     * @param \DateTime|null $end data final da busca
     * @param bool $flag flag para definir se deve retornar dados do ativos (true:ativos)
     * @param null|string $timezone fuso horário
     * @param bool|false $forceUpdate forcar atualizacao
     * @return null|\stdClass
     */
    public function getWaveHourly(
        $idLocale,
        \DateTime $begin,
        \DateTime $end = null,
        $flag = true,
        $timezone = null,
        $forceUpdate = false
    )
    {
        $queryString = "?idlocale={$idLocale}&dateBegin={$begin->format("Y-m-d")}";

        if (!is_null($end)) {
            $queryString .= "&dateEnd={$end->format("Y-m-d")}";
        }
        if (!is_null($timezone)) {
            $queryString .= "&timezone={$timezone}";
        }

        $queryString .= "&flag=" . ($flag === true ? "true" : "false");

        return $this
            ->setRouter(['forecast', 'offshore', 'hourly', 'wave'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }


    /**
     * Retorna dados horarios de onda para localidades de offshore para 72 horas
     *
     * @author Guilherme Santos <guilhermedossantos91@gmail.com>
     * @param int $idLocale id da localidade
     * @param bool $flag flag para definir se deve retornar dados do ativos (true:ativos)
     * @param null|string $timezone fuso horario
     * @param bool|false $forceUpdate forcar atualizacao
     * @return null|\stdClass
     */
    public function getWaveHourly72H(
        $idLocale,
        $flag = true,
        $timezone = null,
        $forceUpdate = false
    )
    {
        $queryString = "?idlocale={$idLocale}";

        if (!is_null($timezone)) {
            $queryString .= "&timezone={$timezone}";
        }

        $queryString .= "&flag=" . ($flag === true ? "true" : "false");

        return $this
            ->setRouter(['forecast', 'offshore', '72hours', 'wave'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }

    /**
     * Retorna dados horarios de vento para localidades de offshore por periodo
     *
     * @author Guilherme Santos <guilhermedossantos91@gmail.com>
     * @param int $idLocale id da localidade
     * @param \DateTime $begin data inicial da busca
     * @param \DateTime|null $end data final da busca
     * @param bool $flag flag para definir se deve retornar dados do ativos (true:ativos)
     * @param null|string $timezone fuso horário
     * @param bool|false $forceUpdate forcar atualizacao
     * @param null|int $distanceId Se ativo os dados serão buscados na tabela analise_perfil.vento filtrado pelo ID do perfil de vento
     * @return null|\stdClass
     */
    public function getWindHourly(
        $idLocale,
        \DateTime $begin,
        \DateTime $end = null,
        $flag = true,
        $timezone = null,
        $forceUpdate = false,
        $distanceId = null
    )
    {
        $queryString = "?idlocale={$idLocale}&dateBegin={$begin->format("Y-m-d")}";

        if (!is_null($end)) {
            $queryString .= "&dateEnd={$end->format("Y-m-d")}";
        }
        if (!is_null($timezone)) {
            $queryString .= "&timezone={$timezone}";
        }

        $queryString .= "&flag=" . ($flag === true ? "true" : "false");
        $queryString .= is_null($distanceId) ? '' : '&distanceId='.$distanceId;

        return $this
            ->setRouter(['forecast', 'offshore', 'hourly', 'wind'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }

    /**
     * Retorna dados horarios de vento para localidades de offshore para 72 horas
     *
     * @author Guilherme Santos <guilhermedossantos91@gmail.com>
     * @param int $idLocale id da localidade
     * @param bool $flag flag para definir se deve retornar dados do ativos (true:ativos)
     * @param null|string $timezone fuso horario
     * @param bool|false $forceUpdate forcar atualizacao
     * @return null|\stdClass
     */
    public function getWindHourly72H(
        $idLocale,
        $flag = true,
        $timezone = null,
        $forceUpdate = false
    )
    {
        $queryString = "?idlocale={$idLocale}";

        if (!is_null($timezone)) {
            $queryString .= "&timezone={$timezone}";
        }

        $queryString .= "&flag=" . ($flag === true ? "true" : "false");

        return $this
            ->setRouter(['forecast', 'offshore', '72hours', 'wind'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }

    /**
     * Retorna dados horarios de chuva para localidades de offshore por periodo
     *
     * @author Guilherme Santos <guilhermedossantos91@gmail.com>
     * @param int $idLocale id da localidade
     * @param \DateTime $begin data inicial da busca
     * @param \DateTime|null $end data final da busca
     * @param bool $flag flag para definir se deve retornar dados do ativos (true:ativos)
     * @param null|string $timezone fuso horario
     * @param bool|false $forceUpdate forcar atualizacao
     * @return null|\stdClass
     */
    public function getRainHourly(
        $idLocale,
        \DateTime $begin,
        \DateTime $end = null,
        $flag = true,
        $timezone = null,
        $forceUpdate = false
    )
    {
        $queryString = "?idlocale={$idLocale}&dateBegin={$begin->format("Y-m-d")}";

        if (!is_null($end)) {
            $queryString .= "&dateEnd={$end->format("Y-m-d")}";
        }
        if (!is_null($timezone)) {
            $queryString .= "&timezone={$timezone}";
        }

        $queryString .= "&flag=" . ($flag === true ? "true" : "false");

        return $this
            ->setRouter(['forecast', 'offshore', 'hourly', 'rain'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }

    /**
     * Retorna dados horarios de chuva para localidades de offshore para 72 horas
     *
     * @author Guilherme Santos <guilhermedossantos91@gmail.com>
     * @param int $idLocale id da localidade
     * @param bool $flag flag para definir se deve retornar dados do ativos (true:ativos)
     * @param null|string $timezone fuso horario
     * @param bool|false $forceUpdate forcar atualizacao
     * @return null|\stdClass
     */
    public function getRainHourly72H(
        $idLocale,
        $flag = true,
        $timezone = null,
        $forceUpdate = false
    )
    {
        $queryString = "?idlocale={$idLocale}";

        if (!is_null($timezone)) {
            $queryString .= "&timezone={$timezone}";
        }

        $queryString .= "&flag=" . ($flag === true ? "true" : "false");

        return $this
            ->setRouter(['forecast', 'offshore', '72hours', 'rain'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }


    /**
     * Retorna dados horarios de temperatura para localidades de offshore por periodo
     *
     * @author Guilherme Santos <guilhermedossantos91@gmail.com>
     * @param int $idLocale id da localidade
     * @param \DateTime $begin data inicial da busca
     * @param \DateTime|null $end data final da busca
     * @param bool $flag flag para definir se deve retornar dados do ativos (true:ativos)
     * @param null|string $timezone fuso horario
     * @param bool|false $forceUpdate forcar atualizacao
     * @return null|\stdClass
     */
    public function getTemperatureHourly(
        $idLocale,
        \DateTime $begin,
        \DateTime $end = null,
        $flag = true,
        $timezone = null,
        $forceUpdate = false
    )
    {
        $queryString = "?idlocale={$idLocale}&dateBegin={$begin->format("Y-m-d")}";

        if (!is_null($end)) {
            $queryString .= "&dateEnd={$end->format("Y-m-d")}";
        }
        if (!is_null($timezone)) {
            $queryString .= "&timezone={$timezone}";
        }

        $queryString .= "&flag=" . ($flag === true ? "true" : "false");

        return $this
            ->setRouter(['forecast', 'offshore', 'hourly', 'temperature'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }

    /**
     * Retorna dados horarios de temperatura para localidades de offshore para 72 horas
     *
     * @author Guilherme Santos <guilhermedossantos91@gmail.com>
     * @param int $idLocale id da localidade
     * @param bool $flag flag para definir se deve retornar dados do ativos (true:ativos)
     * @param null|string $timezone fuso horario
     * @param bool|false $forceUpdate forcar atualizacao
     * @return null|\stdClass
     */
    public function getTemperatureHourly72H(
        $idLocale,
        $flag = true,
        $timezone = null,
        $forceUpdate = false
    )
    {
        $queryString = "?idlocale={$idLocale}";

        if (!is_null($timezone)) {
            $queryString .= "&timezone={$timezone}";
        }

        $queryString .= "&flag=" . ($flag === true ? "true" : "false");

        return $this
            ->setRouter(['forecast', 'offshore', '72hours', 'temperature'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }

    /**
     * Retorna dados horarios de visibilidade para localidades de offshore por periodo
     *
     * @author Guilherme Santos <guilhermedossantos91@gmail.com>
     * @param int $idLocale id da localidade
     * @param \DateTime $begin data inicial da busca
     * @param \DateTime|null $end data final da busca
     * @param bool $flag flag para definir se deve retornar dados do ativos (true:ativos)
     * @param null|string $timezone fuso horario
     * @param bool|false $forceUpdate forcar atualizacao
     * @return null|\stdClass
     */
    public function getVisibilityHourly(
        $idLocale,
        \DateTime $begin,
        \DateTime $end = null,
        $flag = true,
        $timezone = null,
        $forceUpdate = false
    )
    {
        $queryString = "?idlocale={$idLocale}&dateBegin={$begin->format("Y-m-d")}";

        if (!is_null($end)) {
            $queryString .= "&dateEnd={$end->format("Y-m-d")}";
        }
        if (!is_null($timezone)) {
            $queryString .= "&timezone={$timezone}";
        }

        $queryString .= "&flag=" . ($flag === true ? "true" : "false");

        return $this
            ->setRouter(['forecast', 'offshore', 'hourly', 'visibility'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }

    /**
     * Retorna dados horarios de visibilidade para localidades de offshore para 72 horas
     *
     * @author Guilherme Santos <guilhermedossantos91@gmail.com>
     * @param int $idLocale id da localidade
     * @param bool $flag flag para definir se deve retornar dados do ativos (true:ativos)
     * @param null|string $timezone fuso horario
     * @param bool|false $forceUpdate forcar atualizacao
     * @return null|\stdClass
     */
    public function getVisibilityHourly72H(
        $idLocale,
        $flag = true,
        $timezone = null,
        $forceUpdate = false
    )
    {
        $queryString = "?idlocale={$idLocale}";

        if (!is_null($timezone)) {
            $queryString .= "&timezone={$timezone}";
        }

        $queryString .= "&flag=" . ($flag === true ? "true" : "false");

        return $this
            ->setRouter(['forecast', 'offshore', '72hours', 'visibility'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }

    /**
     * Retorna dados horarios de pressao para localidades de offshore por periodo
     *
     * @author Guilherme Santos <guilhermedossantos91@gmail.com>
     * @param int $idLocale id da localidade
     * @param \DateTime $begin data inicial da busca
     * @param \DateTime|null $end data final da busca
     * @param bool $flag flag para definir se deve retornar dados do ativos (true:ativos)
     * @param null|string $timezone fuso horario
     * @param bool|false $forceUpdate forcar atualizacao
     * @return null|\stdClass
     */
    public function getPressureHourly(
        $idLocale,
        \DateTime $begin,
        \DateTime $end = null,
        $flag = true,
        $timezone = null,
        $forceUpdate = false
    )
    {
        $queryString = "?idlocale={$idLocale}&dateBegin={$begin->format("Y-m-d")}";

        if (!is_null($end)) {
            $queryString .= "&dateEnd={$end->format("Y-m-d")}";
        }
        if (!is_null($timezone)) {
            $queryString .= "&timezone={$timezone}";
        }

        $queryString .= "&flag=" . ($flag === true ? "true" : "false");

        return $this
            ->setRouter(['forecast', 'offshore', 'hourly', 'pressure'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }

    /**
     * Retorna dados horarios de pressao para localidades de offshore para 72 horas
     *
     * @author Guilherme Santos <guilhermedossantos91@gmail.com>
     * @param int $idLocale id da localidade
     * @param bool $flag flag para definir se deve retornar dados do ativos (true:ativos)
     * @param null|string $timezone fuso horario
     * @param bool|false $forceUpdate forcar atualizacao
     * @return null|\stdClass
     */
    public function getPressureHourly72H(
        $idLocale,
        $flag = true,
        $timezone = null,
        $forceUpdate = false
    )
    {
        $queryString = "?idlocale={$idLocale}";

        if (!is_null($timezone)) {
            $queryString .= "&timezone={$timezone}";
        }

        $queryString .= "&flag=" . ($flag === true ? "true" : "false");

        return $this
            ->setRouter(['forecast', 'offshore', '72hours', 'pressure'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }

    /**
     * Retorna dados horarios de umidade para localidades de offshore por periodo
     *
     * @author Guilherme Santos <guilhermedossantos91@gmail.com>
     * @param int $idLocale id da localidade
     * @param \DateTime $begin data inicial da busca
     * @param \DateTime|null $end data final da busca
     * @param bool $flag flag para definir se deve retornar dados do ativos (true:ativos)
     * @param null|string $timezone fuso horario
     * @param bool|false $forceUpdate forcar atualizacao
     * @return null|\stdClass
     */
    public function getHumidityHourly(
        $idLocale,
        \DateTime $begin,
        \DateTime $end = null,
        $flag = true,
        $timezone = null,
        $forceUpdate = false
    )
    {
        $queryString = "?idlocale={$idLocale}&dateBegin={$begin->format("Y-m-d")}";

        if (!is_null($end)) {
            $queryString .= "&dateEnd={$end->format("Y-m-d")}";
        }
        if (!is_null($timezone)) {
            $queryString .= "&timezone={$timezone}";
        }

        $queryString .= "&flag=" . ($flag === true ? "true" : "false");

        return $this
            ->setRouter(['forecast', 'offshore', 'hourly', 'humidity'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }

    /**
     * Retorna dados horarios de umidade para localidades de offshore para 72 horas
     *
     * @author Guilherme Santos <guilhermedossantos91@gmail.com>
     * @param int $idLocale id da localidade
     * @param bool $flag flag para definir se deve retornar dados do ativos (true:ativos)
     * @param null|string $timezone fuso horario
     * @param bool|false $forceUpdate forcar atualizacao
     * @return null|\stdClass
     */
    public function getHumidityHourly72H(
        $idLocale,
        $flag = true,
        $timezone = null,
        $forceUpdate = false
    )
    {
        $queryString = "?idlocale={$idLocale}";

        if (!is_null($timezone)) {
            $queryString .= "&timezone={$timezone}";
        }

        $queryString .= "&flag=" . ($flag === true ? "true" : "false");

        return $this
            ->setRouter(['forecast', 'offshore', '72hours', 'humidity'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }

    /**
     * Retorna dados horarios de icone periodo para localidades de offshore por periodo
     *
     * @author Guilherme Santos <guilhermedossantos91@gmail.com>
     * @param int $idLocale id da localidade
     * @param \DateTime $begin data inicial da busca
     * @param \DateTime|null $end data final da busca
     * @param bool $flag flag para definir se deve retornar dados do ativos (true:ativos)
     * @param null|string $timezone fuso horario
     * @param bool|false $forceUpdate forcar atualizacao
     * @return null|\stdClass
     */
    public function getIconHourly(
        $idLocale,
        \DateTime $begin,
        \DateTime $end = null,
        $flag = true,
        $timezone = null,
        $forceUpdate = false
    )
    {
        $queryString = "?idlocale={$idLocale}&dateBegin={$begin->format("Y-m-d")}";

        if (!is_null($end)) {
            $queryString .= "&dateEnd={$end->format("Y-m-d")}";
        }
        if (!is_null($timezone)) {
            $queryString .= "&timezone={$timezone}";
        }

        $queryString .= "&flag=" . ($flag === true ? "true" : "false");

        return $this
            ->setRouter(['forecast', 'offshore', 'hourly', 'icon'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }

    /**
     * Retorna dados horarios de icone periodo para localidades de offshore para 72 horas
     *
     * @author Guilherme Santos <guilhermedossantos91@gmail.com>
     * @param int $idLocale id da localidade
     * @param bool $flag flag para definir se deve retornar dados do ativos (true:ativos)
     * @param null|string $timezone fuso horario
     * @param bool|false $forceUpdate forcar atualizacao
     * @return null|\stdClass
     */
    public function getIconHourly72H(
        $idLocale,
        $flag = true,
        $timezone = null,
        $forceUpdate = false
    )
    {
        $queryString = "?idlocale={$idLocale}";

        if (!is_null($timezone)) {
            $queryString .= "&timezone={$timezone}";
        }

        $queryString .= "&flag=" . ($flag === true ? "true" : "false");

        return $this
            ->setRouter(['forecast', 'offshore', '72hours', 'icon'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }

    /**
     * Retorna dados horarios de swell e windsea para localidades de offshore por periodo
     *
     * @author Guilherme Santos <guilhermedossantos91@gmail.com>
     * @param int $idLocale id da localidade
     * @param \DateTime $begin data inicial da busca
     * @param \DateTime|null $end data final da busca
     * @param bool $flag flag para definir se deve retornar dados do ativos (true:ativos)
     * @param null|string $timezone fuso horario
     * @param bool|false $forceUpdate forcar atualizacao
     * @return null|\stdClass
     */
    public function getSwellwindseaHourly(
        $idLocale,
        \DateTime $begin,
        \DateTime $end = null,
        $flag = true,
        $timezone = null,
        $forceUpdate = false
    )
    {
        $queryString = "?idlocale={$idLocale}&dateBegin={$begin->format("Y-m-d")}";

        if (!is_null($end)) {
            $queryString .= "&dateEnd={$end->format("Y-m-d")}";
        }
        if (!is_null($timezone)) {
            $queryString .= "&timezone={$timezone}";
        }

        $queryString .= "&flag=" . ($flag === true ? "true" : "false");

        return $this
            ->setRouter(['forecast', 'offshore', 'hourly', 'swellwindsea'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }

    /**
     * Retorna dados horarios de swell e windsea para localidades de offshore para 72 horas
     *
     * @author Guilherme Santos <guilhermedossantos91@gmail.com>
     * @param int $idLocale id da localidade
     * @param bool $flag flag para definir se deve retornar dados do ativos (true:ativos)
     * @param null|string $timezone fuso horario
     * @param bool|false $forceUpdate forcar atualizacao
     * @return null|\stdClass
     */
    public function getSwellwindseaHourly72H(
        $idLocale,
        $flag = true,
        $timezone = null,
        $forceUpdate = false
    )
    {
        $queryString = "?idlocale={$idLocale}";

        if (!is_null($timezone)) {
            $queryString .= "&timezone={$timezone}";
        }

        $queryString .= "&flag=" . ($flag === true ? "true" : "false");

        return $this
            ->setRouter(['forecast', 'offshore', '72hours', 'swellwindsea'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }

    /**
     * Retorna dados horarios de Corrente Marítima
     *
     * @author Michel Araujo
     * @author Michael Felix Dias <michael@climatempo.com.br>
     * @param int $idLocale Id da localidade
     * @param \DateTime $begin data inicial da busca
     * @param \DateTime|null $end data final da busca
     * @param bool $flag flag para definir se deve retornar dados do ativos (true:ativos)
     * @param null|string $timezone fuso horario
     * @param bool $forceUpdate forcar atualizacao
     * @return null|\stdClass
     */
    public function getOceanCurrentHourly(
        $idLocale,
        \DateTime $begin,
        \DateTime $end = null,
        $flag = true,
        $timezone = null,
        $forceUpdate = false
    )
    {
        $queryString = "?idlocale={$idLocale}&dateBegin={$begin->format("Y-m-d")}";

        if (!is_null($end)) {
            $queryString .= "&dateEnd={$end->format("Y-m-d")}";
        }

        if (!is_null($timezone)) {
            $queryString .= "&timezone={$timezone}";
        }

        $queryString .= "&flag=" . ($flag === true ? "true" : "false");

        return $this
            ->setRouter(['forecast', 'offshore', 'hourly', 'ocean-current'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }

    /**
     * Retorna dados horarios de Corrente Marítima para 72 horas
     *
     * @author Michael Felix Dias <michael@climatempo.com.br>
     * @param int $idLocale Id da localidade
     * @param bool $flag flag para definir se deve retornar dados do ativos (true:ativos)
     * @param null|string $timezone fuso horario
     * @param bool $forceUpdate forcar atualizacao
     * @return null|\stdClass
     */
    public function getOceanCurrentHourly72H(
        $idLocale,
        $flag = true,
        $timezone = null,
        $forceUpdate = false
    )
    {
        $queryString = "?idlocale={$idLocale}";

        if (!is_null($timezone)) {
            $queryString .= "&timezone={$timezone}";
        }

        $queryString .= "&flag=" . ($flag === true ? "true" : "false");

        return $this
            ->setRouter(['forecast', 'offshore', '72hours', 'ocean-current'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }
}