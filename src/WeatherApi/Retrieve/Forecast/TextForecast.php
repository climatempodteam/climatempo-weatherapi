<?php

namespace WeatherApi\Retrieve\Forecast;

use WeatherApi\Retrieve\AbstractRetrieve;

/**
 * Class TextForecast
 *
 * @author Danilo Luz <dlo.luzani@climatempo.com.br>
 * @version 1.0.1
 */
class TextForecast extends AbstractRetrieve
{
    /**
     * Retorna texto analise Brasil
     *
     * @param $dateBegin
     * @return mixed
     */
    public function findAnalysisBrazil($dateBegin)
    {
        $queryString = '?' . http_build_query([
                'dateBegin' => $dateBegin->format('Y-m-d')
            ]);

        return $this
            ->setRouter(['text', 'analysis', 'brazil'])
            ->addQueryString($queryString)
            ->request();
    }

    /**
     * Retorna texto destaque Brasil
     *
     * @param $dateBegin
     * @return mixed
     */
    public function findFeaturedBrazil($dateBegin)
    {
        $queryString = '?' . http_build_query([
                'dateBegin' => $dateBegin->format('Y-m-d')
            ]);

        return $this
            ->setRouter(['text', 'featured', 'brazil'])
            ->addQueryString($queryString)
            ->request();
    }
}