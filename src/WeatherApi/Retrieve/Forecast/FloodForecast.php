<?php

namespace WeatherApi\Retrieve\Forecast;

use WeatherApi\Retrieve\AbstractRetrieve;
use DateTime;

/**
 * Class FloodForecast
 *
 * @package WeatherApi\Retrieve\Forecast
 *
 * @author  Willian Dener <willian.dener@climatempo.com.br>
 * @version 1.0
 */

class FloodForecast extends AbstractRetrieve
{

    /**
     * Retorna dados de risco de enchente Diário
     *
     * @author  Willian Dener <willian.dener@climatempo.com.br>
     *
     * @param $latitude
     * @param $longitude 
     * @param $type     
     */

    public function getDailyFlood($latitude, $longitude, $forceUpdate = false)
    {
        $queryString = "?" . http_build_query([
            'latitude' => $latitude,
            'longitude' => $longitude,
            'periodType' => 'daily'
        ]);

        return $this
            ->setRouter(['forecast', 'flood'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }
}
