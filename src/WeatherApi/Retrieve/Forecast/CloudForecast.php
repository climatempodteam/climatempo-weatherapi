<?php

namespace WeatherApi\Retrieve\Forecast;

use WeatherApi\Retrieve\AbstractRetrieve;

/**
 * Class CloudForecast
 *
 * @package WeatherApi\Retrieve\Forecast
 */
class CloudForecast extends AbstractRetrieve
{

    /**
     * @param int $idLocale
     * @param \DateTime $begin
     * @param \DateTime $end
     * @param bool|false $forceUpdate
     * @return mixed
     */
    public function getHourly($idLocale, \DateTime $begin, \DateTime $end = null, $forceUpdate = false)
    {
        $queryString = "?idlocale={$idLocale}&dateBegin={$begin->format("Y-m-d")}";

        if (!is_null($end)) {
            $queryString .= "&dateEnd={$end->format("Y-m-d")}";
        }

        return $this
            ->setRouter(['forecast', 'hourly', 'cloud'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }
}