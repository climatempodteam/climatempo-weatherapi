<?php

namespace WeatherApi\Retrieve\Forecast;

use WeatherApi\Retrieve\AbstractRetrieve;

/**
 * Class GroundTemperatureForecast
 *
 * @package WeatherApi\Retrieve\Forecast
 * @author Adilson Ferreira <adilson@climatempo.com.br>
 * @version 1.0.0
 */
class GroundTemperatureForecast extends AbstractRetrieve
{

    /**
     * @param int $idLocale
     * @param \DateTime $begin
     * @param \DateTime $end
     * @param bool|false $forceUpdate
     * @return mixed
     */
    public function getDaily($idLocale, \DateTime $begin, \DateTime $end = null, $forceUpdate = false)
    {
        $queryStringData = [
            'idlocale' => $idLocale,
            'dateBegin' => $begin->format("Y-m-d")
        ];

        if (!is_null($end)) {
            $queryStringData['dateEnd'] = $end->format("Y-m-d");
        }

        $queryString = "?".http_build_query($queryStringData);

        return $this
            ->setRouter(['forecast', 'period', 'temperature', 'ground'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }
}