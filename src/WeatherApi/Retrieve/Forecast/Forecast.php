<?php

namespace WeatherApi\Retrieve\Forecast;

use WeatherApi\Config;
use WeatherApi\Retrieve\AbstractRetrieve;

/**
 * Class Forecast
 * @package WeatherApi\Retrieve\Forecast
 * @author Guilherme Santos <guilhermedossantos91@gmail.com>
 * @author Gustavo Santos <gustavo.santos@climatempo.com.br>
 * @author Giovanni Cruz <giovanni.cruz@climatempo.com.br>
 *
 * @version 1.4.0
 */
class Forecast extends AbstractRetrieve
{

    /**
     * Retorna dados de previsao diaria geo referenciada para um ponto por periodo longos
     *
     * @author Diego Hideky <diego.lima@climatempo.com.br>
     *
     * @param number $latitude
     * @param number $longitude
     * @param \DateTime $begin
     * @param \DateTime|null $end
     * @param bool $forceUpdate
     * @return object
     */
    public function getDailyGeo(
        $latitude,
        $longitude,
        \DateTime $begin,
        \DateTime $end = null,
        $forceUpdate = false
    ) {
        $queryStringData = [
            'latitude' => $latitude,
            'longitude' => $longitude,
            'dateBegin' => $begin->format("Y-m-d")
        ];

        if (!is_null($end)) {
            $queryStringData['dateEnd'] = $end->format("Y-m-d");
        }

        $queryString = "?".http_build_query($queryStringData);

        return $this
            ->setRouter(['forecast', 'period', 'daily', 'long'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }


    /**
     * Retorna dados de previsao diaria para uma localidade por periodo longos
     * Retorna os primeiros 15 dias com dados do banco de dados e os demais
     * Com dados do MapServer
     *
     * @author Diego Hideky <diego.lima@climatempo.com.br>
     *
     * @param number $idLocale
     * @param \DateTime $begin
     * @param \DateTime|null $end
     * @param bool $forceUpdate
     * @return object
     */
    public function getDailyLong(
        $idLocale,
        \DateTime $begin,
        \DateTime $end = null,
        $forceUpdate = false
    ) {
        $queryString = "?idlocale={$idLocale}&dateBegin={$begin->format("Y-m-d")}";

        if (!is_null($end)) {
            $queryString .= "&dateEnd={$end->format("Y-m-d")}";
        }

        return $this
            ->setRouter(['forecast', 'period', 'daily', 'long'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }

    /**
     * Retorna dados de previsao diaria para uma localidade por periodo
     *
     * @param number $idLocale
     * @param \DateTime $begin
     * @param \DateTime|null $end
     * @param bool $forceUpdate
     * @return object
     */
    public function getDaily(
        $idLocale,
        \DateTime $begin,
        \DateTime $end = null,
        $forceUpdate = false
    ) {
        $queryString = "?idlocale={$idLocale}&dateBegin={$begin->format("Y-m-d")}";

        if (!is_null($end)) {
            $queryString .= "&dateEnd={$end->format("Y-m-d")}";
        }
        return $this
            ->setRouter(['forecast', 'period', 'daily'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }


    /**
     * @param int $idLocale
     * @param \DateTime $begin
     * @param \DateTime|null $end
     * @param bool|false $forceUpdate
     * @return mixed
     */
    public function getBeachDaily(
        $idLocale,
        \DateTime $begin,
        \DateTime $end = null,
        $forceUpdate = false
    ) {
        $queryString = "?idlocale={$idLocale}&dateBegin={$begin->format('Y-m-d')}";

        if (!is_null($end)) {
            $queryString .= "&dateEnd={$end->format('Y-m-d')}";
        }

        return $this
            ->setRouter(['forecast', 'beach', 'period', 'daily'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }

    /**
     * Retorna dados de previsao horaria para uma localidade por periodo
     *
     * @param number $idLocale
     * @param \DateTime $begin
     * @param \DateTime|null $end
     * @param string $timezone
     * @param bool $forceUpdate
     * @return object
     */
    public function getHourly(
        $idLocale,
        \DateTime $begin,
        \DateTime $end = null,
        $timezone = null,
        $forceUpdate = false
    ) {
        $queryString = "?idlocale={$idLocale}&dateBegin={$begin->format("Y-m-d")}";

        if (!is_null($end)) {
            $queryString .= "&dateEnd={$end->format("Y-m-d")}";
        }
        if (!is_null($timezone)) {
            $queryString .= "&timezone={$timezone}";
        }

        return $this
            ->setRouter(['forecast', 'period', 'hourly'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }

    /**
     * Retorna dados de previsao horaria para uma localidade por periodo
     *
     * @param array $localesIds
     * @param \DateTime $begin
     * @param \DateTime|null $end
     * @param string $timezone
     * @param array $variables
     * @param int $onlyMapServer
     * @param bool $forceUpdate
     * @return object
     */
    public function getHourlyForVariables(
        $localesIds,
        \DateTime $begin,
        \DateTime $end,
        $timezone,
        $variables = [],
        $onlyMapServer = 0,
        $forceUpdate = false
    ) {
        $queries = [
            'localesIds' => implode(',', $localesIds),
            'dateBegin' => $begin->format('Y-m-d'),
            'dateEnd' => $end->format('Y-m-d'),
            'timezone' => $timezone,
            'vars' => implode(',', $variables)
        ];

        if ($onlyMapServer == 1) {
            $queries['onlyMapServer'] = $onlyMapServer;
        }

        if ($forceUpdate) {
            $queries['timenow'] = time();
        }

        $queryString = '?'.http_build_query($queries);

        return $this
            ->setRouter(['forecast', 'period', 'hourly', 'variables'])
            ->addQueryString($queryString)
            ->request();
    }


    /**
     * Retorna dados climaticos de previsao por localidade e periodo
     *
     * @param $idLocale
     * @param bool $forceUpdate
     * @return mixed
     */
    public function getClimate($idLocale, $forceUpdate = false)
    {
        $queryString = "?idlocale={$idLocale}";

        return $this
            ->setRouter(['climate'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }

    /**
     * Retorna dados da camada de mistura por localidade e período
     *
     * @param string $localeIds
     * @param \DateTime $dateBegin
     * @param \DateTime $dateEnd
     * @param bool $forceUpdate
     * @return object
     */
    public function getLayerMix(
        string $localeIds,
        \DateTime $dateBegin,
        \DateTime $dateEnd,
        bool $forceUpdate = false
    )
    {
        $queryString = "?localeIds={$localeIds}&dateBegin={$dateBegin->format("Y-m-d")}&dateEnd={$dateEnd->format("Y-m-d")}";

        return $this
            ->setRouter(['forecast', 'hourly', 'layer', 'mix'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }

    /**
     * Retorna dados da tabela Pasquill por localidade e período
     *
     * @param string $localeIds
     * @param \DateTime $dateBegin
     * @param \DateTime $dateEnd
     * @param bool $forceUpdate
     * @return object
     */
    public function getLayerPasquill(
        string $localeIds,
        \DateTime $dateBegin,
        \DateTime $dateEnd,
        bool $forceUpdate = false
    )
    {
        $queryString = "?localeIds={$localeIds}&dateBegin={$dateBegin->format("Y-m-d")}&dateEnd={$dateEnd->format("Y-m-d")}";

        return $this
            ->setRouter(['forecast', 'hourly', 'pasquill'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }

    /**
     * Retorna dados de bacia
     *
     * @param array $localeIds
     * @param string $dateBegin
     * @param string $dateEnd
     * @param array $variables
     * @param bool $forceUpdate
     * @return object
     */
    public function getBasinDataForVariables(
        array $localeIds,
        string $dateBegin,
        string $dateEnd,
        array $variables,
        bool $forceUpdate = false
    )
    {
        $payload = [
            'localeIds' => implode(',', $localeIds),
            'dateBegin' => $dateBegin,
            'dateEnd' => $dateEnd,
            'variables' => implode(',', $variables)
        ];

        $queryString = '?'.http_build_query($payload);

        return $this
            ->setRouter(['forecast', 'hydrology', 'basin'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }
}
