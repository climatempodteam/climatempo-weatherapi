<?php

namespace WeatherApi\Retrieve\Forecast;

use WeatherApi\Retrieve\AbstractRetrieve;

/**
 * Class TemperatureForecast
 *
 * @package WeatherApi\Retrieve\Forecast
 */
class TemperatureForecast extends AbstractRetrieve
{

    /**
     * Retorna dados de previsao de chuva para 15 dias por localidade
     *
     * @param number $idLocale
     * @param bool $forceUpdate
     * @param int $onlyMapServer
     * @return mixed
     */
    public function getDaily15D($idLocale, $forceUpdate = false, $onlyMapServer = 0)
    {
        $queries = [
            'idlocale' => $idLocale
        ];

        if ($onlyMapServer === 1) {
            $queries['onlyMapServer'] = $onlyMapServer;
        }

        $queryString = '?'.http_build_query($queries);

        return $this
            ->setRouter(['forecast', '15days', 'temperature'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }


    /**
     * Retorna dados horarios de previsao de chuva para 72 horas por localidade
     *
     * @param number $idLocale
     * @param string $timezone
     * @param bool $forceUpdate
     * @return mixed
     */
    public function getHourly72H($idLocale, $timezone = null, $forceUpdate = false)
    {
        $queryString = "?idlocale={$idLocale}";

        if (!is_null($timezone)) {
            $queryString .= "&timezone={$timezone}";
        }

        return $this
            ->setRouter(['forecast', '72hours', 'temperature'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }


    /**
     * Retorna dados de previsao de chuva horario por localidade e periodo
     *
     * @param number $idLocale
     * @param \DateTime $begin
     * @param \DateTime|null $end
     * @param string $timezone
     * @param bool $forceUpdate
     * @return mixed
     */
    public function getHourly(
        $idLocale,
        \DateTime $begin,
        \DateTime $end = null,
        $timezone = null,
        $forceUpdate = false
    ) {
        $queryString = "?idlocale={$idLocale}&dateBegin={$begin->format("Y-m-d")}";

        if (!is_null($end)) {
            $queryString .= "&dateEnd={$end->format("Y-m-d")}";
        }
        if (!is_null($timezone)) {
            $queryString .= "&timezone={$timezone}";
        }

        return $this
            ->setRouter(['forecast', 'hourly', 'temperature'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }

    /**
     * Retorna dados climaticos de chuva por localidade e periodo
     *
     * @param number $idLocale
     * @param \DateTime $begin
     * @param \DateTime|null $end
     * @param $forceUpdate
     * @return mixed
     */
    public function getClimate($idLocale, \DateTime $begin, \DateTime $end = null, $forceUpdate = false, $onlyMapServer = 0)
    {
        $queries = [
            'idlocale' => $idLocale,
            'dateBegin' => $begin->format("Y-m-d")
        ];

        if (!is_null($end)) {
            $queries['dateEnd'] = $end->format("Y-m-d");
        }

        if ($onlyMapServer == 1) {
            $queries['onlyMapServer'] = $onlyMapServer;
        }

        $queryString = "?".http_build_query($queries);

        return $this
            ->setRouter(['climate', 'temperature'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }
}
