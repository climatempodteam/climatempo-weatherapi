<?php

namespace WeatherApi\Retrieve\Forecast;

use WeatherApi\Retrieve\AbstractRetrieve;
use DateTime;

/**
 * Class FireForecast
 *
 * @package WeatherApi\Retrieve\Forecast
 *
 * @author  Willian Dener <willian.dener@climatempo.com.br>
 * @version 1.0
 */

class FireForecast extends AbstractRetrieve
{

    /**
     * Retorna dados de risco queimada Diário
     * dados coletados da api da Somar
     *
     * @param $latitude
     * @param $longitude 
     * @param $periodType    
     */

    public function getDailyFire($latitude, $longitude, $forceUpdate = false)
    {
        $queryString = "?" . http_build_query([
            'latitude' => $latitude,
            'longitude' => $longitude,
            'periodType' => 'daily'
        ]);

        return $this
            ->setRouter(['forecast', 'fire'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }
}
