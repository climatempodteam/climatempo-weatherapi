<?php

namespace WeatherApi\Retrieve\Forecast;
use WeatherApi\Retrieve\AbstractRetrieve;

/**
 * Class TsmForecast
 *
 * Classe para obter dados de TSM (temperatura da superfície do mar )
 *
 * @package WeatherApi\Retrieve\Forecast
 * @author Guilherme Santos <guilhermedossantos91@gmail.com>
 * @version 1.0.0
 */
class TsmForecast extends AbstractRetrieve
{

    /**
     * Retorna dados de TSM horario por periodo e id localidade.
     *
     * @param int $idLocale id da localidade
     * @param \DateTime $begin data inicial
     * @param \DateTime|null $end data final
     * @param string|null $timezone fuso horario
     * @param bool|false $forceUpdate forca atualizacao dos dados
     * @return mixed
     */
    public function getHourly(
        $idLocale,
        \DateTime $begin,
        \DateTime $end = null,
        $timezone = null,
        $forceUpdate = false
    )
    {
        $queryString = "?localeId={$idLocale}&dateBegin={$begin->format("Y-m-d")}";

        if (!is_null($end)) {
            $queryString .= "&dateEnd={$end->format("Y-m-d")}";
        }
        if (!is_null($timezone)) {
            $queryString .= "&timezone={$timezone}";
        }

        return $this
            ->setRouter(['forecast', 'hourly', 'tsm'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }

}