<?php

namespace WeatherApi\Retrieve\Forecast;

use WeatherApi\Retrieve\AbstractRetrieve;
use DateTime;

/**
 * Class LandingForecast
 *
 * @package WeatherApi\Retrieve\Forecast
 *
 * @author luiz Gonçalves <luiz.goncalves@climatempo.com.br>
 * @author Gustavo Santos <gustavo.santos@climatempo.com.br>
 * @version 1.1.0
 */
class LandingForecast extends AbstractRetrieve
{

    /**
     * Retorna dados de previsao de teto para 15 dias por localidade
     *
     * @param number $idLocale
     * @param bool $forceUpdate
     * @param int $onlyMapServer
     * @return mixed
     */
    public function getDaily15D($idLocale, $forceUpdate = false, $onlyMapServer = 0)
    {
        $queries = [
            'idlocale' => $idLocale
        ];

        if ($onlyMapServer === 1) {
            $queries['onlyMapServer'] = $onlyMapServer;
        }

        $queryString = '?'.http_build_query($queries);

        return $this
            ->setRouter(['forecast', '15days', 'landing'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }

    /**
     * Salva dados de teto
     * 
     * @param number $idLocale 
     * @param DateTime $forecastDate
     * @param DateTime $processingDate
     * @param float $landing
     * @param bool $flag
     */
    public function saveLanding(
        $idLocale,
        DateTime $forecastDate,
        DateTime $processingDate,
        $landing,
        $flag
    )
    {
        $queryString = '?' . http_build_query([
            'localeId' => $idLocale,
            'forecastDate' => $forecastDate->format('Y-m-d H:i:s'),
            'processingDate' => $processingDate->format('Y-m-d H:i:s'),
            'landing' => $landing,
            'flag' => $flag ? 'true' : 'false'
        ]);

        return $this
            ->setRouter(['forecast','offshore','hourly','landing','edit'])
            ->addQueryString($queryString)
            ->request();
    }

}