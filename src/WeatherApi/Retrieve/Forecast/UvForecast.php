<?php

namespace WeatherApi\Retrieve\Forecast;

use WeatherApi\Retrieve\AbstractRetrieve;

/**
 * Class UvForecast
 *
 * @package WeatherApi\Retrieve\Forecast
 * @author  Guilherme Santos <guilhermedossantos91@gmail.com>
 * @author  Danilo Luz <danilo.luz@climatempo.com.br>
 * @version 1.0.0
 */
class UvForecast extends AbstractRetrieve
{
    /**
     * @param int $idLocale
     * @param $dateBegin
     * @param $dateEnd
     * @param bool|false $forceUpdate
     * @return mixed
     */
    public function getDaily($idLocale, $dateBegin, $dateEnd, $forceUpdate = false)
    {
        $queryString = "?idlocale={$idLocale}&dateBegin={$dateBegin->format("Y-m-d")}";

        if (!is_null($dateEnd)) {
            $queryString .= "&dateEnd={$dateEnd->format("Y-m-d")}";
        }

        return $this
            ->setRouter(['forecast', 'period', 'uv'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }

    /**
     * @param int $idLocale
     * @param bool|false $forceUpdate
     * @return mixed
     */
    public function getDaily15D($idLocale, $forceUpdate = false)
    {
        $queryString = "?idlocale={$idLocale}";

        return $this
            ->setRouter(['forecast', '15days', 'uv'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }


    /**
     * @param int $idLocale
     * @param \DateTime $begin
     * @param \DateTime|null $end
     * @param null $timezone
     * @param bool|false $forceUpdate
     * @return null|\stdClass
     */
    public function getHourly(
        $idLocale,
        \DateTime $begin,
        \DateTime $end = null,
        $timezone = null,
        $forceUpdate = false
    )
    {
        $queryString = "?idlocale={$idLocale}&dateBegin={$begin->format("Y-m-d")}";

        if (!is_null($end)) {
            $queryString .= "&timezone={$end->format("Y-m-d")}";
        }

        if (!is_null($timezone)) {
            $queryString .= "&timezone={$timezone}";
        }

        return $this
            ->setRouter(['forecast', 'hourly', 'uv'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }


    /**
     * @param int $idLocale
     * @param string|null $timezone
     * @param bool|false $forceUpdate
     * @return null|\stdClass
     */
    public function getHourly72H($idLocale, $timezone = null, $forceUpdate = false)
    {
        $queryString = "?idlocale={$idLocale}";

        if (!is_null($timezone)) {
            $queryString .= "&timezone={$timezone}";
        }

        return $this
            ->setRouter(['forecast', '72hours', 'uv'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }
}