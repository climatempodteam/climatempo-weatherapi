<?php

namespace WeatherApi\Retrieve\Forecast;

use WeatherApi\Retrieve\AbstractRetrieve;

/**
 * Class WaveForecast
 *
 * @package WeatherApi\Retrieve\Forecast
 * @author  Gustavo Vitorino <gustavo.vittorino@climatempo.com.br>
 * @version 1.0.0
 */
class WaveForecast extends AbstractRetrieve
{

    /**
     * @param int $idBeach
     * @param \DateTime $begin
     * @param \DateTime|null $end
     * @param string|null $timezone
     * @param bool|false $forceUpdate
     * @return mixed
     */
    public function getDailyByIdBeach(
        $idBeach,
        \DateTime $begin,
        \DateTime $end = null,
        $timezone = null,
        $forceUpdate = false
    ) {
        $queryString = '?' . http_build_query([
            'idBeach' => $idBeach ? $idBeach : '',
            'dateBegin' => $begin ? $begin->format('Y-m-d') : '',
            'dateEnd' => $end ? $end->format('Y-m-d') : '',
            'timezone' => $timezone ? $timezone : ''
        ]);

        return $this
            ->setRouter(['forecast', 'period', 'beach', 'wave'])
            ->addQueryString($queryString)
            ->request();
    }
}