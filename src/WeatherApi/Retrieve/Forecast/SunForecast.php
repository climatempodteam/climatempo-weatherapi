<?php

namespace WeatherApi\Retrieve\Forecast;

use WeatherApi\Retrieve\AbstractRetrieve;

/**
 * Class SunForecast
 * @package WeatherApi\Retrieve\Forecast
 * @version 1.1.0
 * @author Guilherme Santos <guilhermedossantos91@gmail.com>
 */
class SunForecast extends AbstractRetrieve
{
    /**
     * @param int $idLocale
     * @param bool|false $forceUpdate
     * @return mixed
     */
    public function getDaily15D($idLocale, $forceUpdate = false)
    {
        return $this
            ->setRouter(['forecast', '15days', 'sun'])
            ->addQueryString("?idlocale={$idLocale}")
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    [
                        "id" => $idLocale
                    ]
                ),
                $forceUpdate
            );
    }


    /**
     * Retorna dados horarios de nascer do sol
     *
     * @author Guilherme Santos <guilhermedossantos91@gmail.com>
     * @param int $idLocale id da localidade
     * @param null|string $timezone fuso horario
     * @param bool|false $forceUpdate forcar atualizacao
     * @return null|\stdClass
     */
    public function getHourly72H(
        $idLocale,
        $timezone = null,
        $forceUpdate = false
    )
    {
        $queryString = "?idlocale={$idLocale}";
        if (!is_null($timezone)) {
            $queryString .= "&timezone={$timezone}";
        }

        return $this
            ->setRouter(['forecast', '72hours', 'sunshine-duration'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }

}