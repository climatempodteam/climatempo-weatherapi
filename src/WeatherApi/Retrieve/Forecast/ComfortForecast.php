<?php

namespace WeatherApi\Retrieve\Forecast;

use WeatherApi\Retrieve\AbstractRetrieve;

/**
 * Class ComfortForecast
 *
 * @package WeatherApi\Retrieve\Forecast
 * @author Danilo Luz <danilo.luz@climatempo.com.br>
 * @version 1.0.0
 */
class ComfortForecast extends AbstractRetrieve
{
    /**
     * @param int $idLocale
     * @param \DateTime $begin
     * @param \DateTime|null $end
     * @param bool|false $forceUpdate
     * @return mixed
     */
    public function getDaily(
        $idLocale,
        \DateTime $begin,
        \DateTime $end = null,
        $forceUpdate = false
    )
    {
        $queryString = "?idlocale={$idLocale}&dateBegin={$begin->format("Y-m-d")}";

        if (!is_null($end)) {
            $queryString .= "&dateEnd={$end->format("Y-m-d")}";
        }

        return $this
            ->setRouter(['forecast', 'period', 'comfort'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }
}