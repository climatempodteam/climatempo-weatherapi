<?php

namespace WeatherApi\Retrieve\Forecast\Redemet;

use WeatherApi\Retrieve\AbstractRetrieve;

/**
 * Class MetarForecast
 *
 * @package WeatherApi\Retrieve\Forecast
 * @author Danilo Luz <danilo.luz@climatempo.com.br>
 * @version 1.0.0
 */
class MetarForecast extends AbstractRetrieve
{
    /**
     * Request to return Redemet's METAR data
     * 
     * @param string $icaos
     * @param \DateTime $begin
     * @param \DateTime|null $end
     * @param bool|false $forceUpdate
     * @return mixed
     */
    public function getMETARHourly($icaos, \DateTime $begin, \DateTime $end = null, $forceUpdate = false)
    {
        $queryStringData = [
            'icao' => $icaos,
            'dateBegin' => $begin->format("Y-m-d")
        ];

        if (!is_null($end)) {
            $queryStringData['dateEnd'] = $end->format("Y-m-d");
        }

        $queryString = "?" . http_build_query($queryStringData);

        return $this
            ->setRouter(['forecast', 'redemet', 'metar'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }
}