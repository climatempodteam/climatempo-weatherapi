<?php

namespace WeatherApi\Retrieve\Forecast;

use WeatherApi\Retrieve\AbstractRetrieve;

/**
 * Class LightningForecast
 *
 * @package WeatherApi\Retrieve\Forecast
 */
class LightningForecast extends AbstractRetrieve
{

    /**
     * @param int $idLocale
     * @param bool|false $forceUpdate
     * @return mixed
     */
    public function getDailyRisk15D($idLocale, $forceUpdate = false)
    {
        $queryString = "?idlocale={$idLocale}";

        return $this
            ->setRouter(['forecast', '15days', 'lightning', 'risk'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }

    /**
     * @param int $idLocale
     * @param \DateTime $begin
     * @param \DateTime $end
     * @param bool|false $forceUpdate
     * @return mixed
     */
    public function getHourly($idLocale, \DateTime $begin, \DateTime $end = null, $forceUpdate = false)
    {
        $queryString = "?idlocale={$idLocale}&dateBegin={$begin->format("Y-m-d")}";

        if (!is_null($end)) {
            $queryString .= "&dateEnd={$end->format("Y-m-d")}";
        }

        return $this
            ->setRouter(['forecast', 'hourly', 'lightning', 'risk'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }
}