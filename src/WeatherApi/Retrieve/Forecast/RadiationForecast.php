<?php

namespace WeatherApi\Retrieve\Forecast;

use WeatherApi\Retrieve\AbstractRetrieve;

/**
 * Class RadiationForecast
 *
 * Classe de radiação para previsão
 *
 *
 * @package WeatherApi\Retrieve\Forecast
 * @version 1.0.0
 * @author Gustavo Santos <gustavo.santos@climatempo.com.br>
 */
class RadiationForecast extends AbstractRetrieve
{

    /**
     * @param number $idLocale
     * @param string $timezone
     * @param bool $forceUpdate
     * @return mixed
     */
    public function getHourly72H($idLocale, $timezone = null, $forceUpdate = false)
    {
        $queryString = "?".http_build_query([
            "idlocale" => $idLocale,
            "timezone" => $timezone
        ]);

        return $this
            ->setRouter(['forecast', '72hours', 'radiation'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }

    /**
     * @param int $idLocale
     * @param \DateTime $begin
     * @param \DateTime|null $end
     * @param string|null $timezone
     * @param bool|false $forceUpdate
     * @return mixed
     */
    public function getHourly(
        $idLocale,
        \DateTime $begin,
        \DateTime $end = null,
        $timezone = null,
        $forceUpdate = false
    )
    {
        $queryString = "?".http_build_query([
            "idlocale" => $idLocale,
            "timezone" => $timezone,
            "dateBegin" => $begin->format("Y-m-d"),
            "dateEnd" => $end->format("Y-m-d")
        ]);

        return $this
            ->setRouter(['forecast', 'hourly', 'radiation'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }

}