<?php

namespace WeatherApi\Retrieve\Forecast;

use WeatherApi\Retrieve\AbstractRetrieve;

/**
 * Class FlowRateForecast
 *
 * @package WeatherApi\Retrieve\Forecast
 * @author Danilo Luz <danilo.luz@climatempo.com.br>
 * @version 1.0.0
 */
class FlowRateForecast extends AbstractRetrieve
{

    /**
     * @param $idLocale
     * @param bool $forceUpdate
     * @return mixed
     */
    public function getDaily15D($idLocale, $forceUpdate = false)
    {
        $queryString = "?idlocale={$idLocale}";

        return $this
            ->setRouter(['forecast', '15days', 'flowrate'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }
}