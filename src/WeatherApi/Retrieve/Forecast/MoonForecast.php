<?php

namespace WeatherApi\Retrieve\Forecast;
use WeatherApi\Retrieve\AbstractRetrieve;

/**
 * Class MoonForecast
 * @package WeatherApi\Retrieve\Forecast
 * @author Guilherme Santos <guilhermedossantos91@gmail.com>
 */
class MoonForecast extends AbstractRetrieve
{
    /**
     * @param bool|false $forceUpdate
     * @return mixed
     */
    public function getPhases15D($forceUpdate = false)
    {
        return $this
            ->setRouter(['forecast', '15days', 'moon'])
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__
                ),
                $forceUpdate
            );
    }


    /**
     * @param \DateTime $begin
     * @param \DateTime|null $end
     * @param bool|false $forceUpdate
     * @return null|\stdClass
     */
    public function getDaily(
        \DateTime $begin,
        \DateTime $end = null,
        $forceUpdate = false
    )
    {
        $queryString = "?&dateBegin={$begin->format("Y-m-d")}";

        if (!is_null($end)) {
            $queryString .= "&dateEnd={$end->format("Y-m-d")}";
        }

        return $this
            ->setRouter(['forecast', 'period', 'moon'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }
}