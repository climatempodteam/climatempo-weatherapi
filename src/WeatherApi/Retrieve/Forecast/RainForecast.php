<?php

namespace WeatherApi\Retrieve\Forecast;

use WeatherApi\Retrieve\AbstractRetrieve;

/**
 * Class RainForecast
 *
 * @package WeatherApi\Retrieve\Forecast
 * @author Adilson <adilson@climatempo.com.br>
 * @author Danilo Luz <danilo.luz@climatempo.com.br>
 * @author Giovanni Cruz <giovanni.cruz@climatempo.com.br>
 *
 * @version 1.4.0
 */
class RainForecast extends AbstractRetrieve
{

    /**
     * @param $idLocale
     * @param bool $forceUpdate
     * @param int $onlyMapServer
     * @return mixed
     */
    public function getDaily15D($idLocale, $forceUpdate = false, $onlyMapServer = 0)
    {
        $queries = [
            'idlocale' => $idLocale
        ];

        if ($onlyMapServer === 1) {
            $queries['onlyMapServer'] = $onlyMapServer;
        }

        $queryString = '?'.http_build_query($queries);

        return $this
            ->setRouter(['forecast', '15days', 'rain'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }

    /**
     * @param int $idLocale
     * @param \DateTime $begin
     * @param \DateTime|null $end
     * @param string|null $timezone
     * @param bool|false $forceUpdate
     * @return mixed
     */
    public function getDaily(
        $idLocale,
        \DateTime $begin,
        \DateTime $end = null,
        $timezone = null,
        $forceUpdate = false
    ) {
        $queryString = "?idlocale={$idLocale}&dateBegin={$begin->format("Y-m-d")}";

        if (!is_null($end)) {
            $queryString .= "&dateEnd={$end->format("Y-m-d")}";
        }
        if (!is_null($timezone)) {
            $queryString .= "&timezone={$timezone}";
        }

        return $this
            ->setRouter(['forecast', 'period', 'rain'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }

    /**
     * @param number $idLocale
     * @param string $timezone
     * @param bool $forceUpdate
     * @return mixed
     */
    public function getHourly72H($idLocale, $timezone = null, $forceUpdate = false)
    {
        $queryString = "?idlocale={$idLocale}";

        if (!is_null($timezone)) {
            $queryString .= "&timezone={$timezone}";
        }

        return $this
            ->setRouter(['forecast', '72hours', 'rain'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }

    /**
     * @param int $idLocale
     * @param \DateTime $begin
     * @param \DateTime|null $end
     * @param string|null $timezone
     * @param bool|false $forceUpdate
     * @return mixed
     */
    public function getHourly(
        $idLocale,
        \DateTime $begin,
        \DateTime $end = null,
        $timezone = null,
        $forceUpdate = false
    ) {
        $queryString = "?idlocale={$idLocale}&dateBegin={$begin->format("Y-m-d")}";

        if (!is_null($end)) {
            $queryString .= "&dateEnd={$end->format("Y-m-d")}";
        }
        if (!is_null($timezone)) {
            $queryString .= "&timezone={$timezone}";
        }

        return $this
            ->setRouter(['forecast', 'hourly', 'rain'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }


    /**
     * @param $idLocale
     * @param \DateTime $begin
     * @param \DateTime|null $end
     * @param bool $forceUpdate
     * @param int $onlyMapServer
     * @return \stdClass|null
     */
    public function getClimate(
        $idLocale,
        \DateTime $begin,
        \DateTime $end = null,
        $forceUpdate = false,
        $onlyMapServer = 0
    ) {
        $queries = [
            'idlocale' => $idLocale,
            'dateBegin' => $begin->format("Y-m-d")
        ];

        if (!is_null($end)) {
            $queries['dateEnd'] = $end->format("Y-m-d");
        }

        if ($onlyMapServer == 1 || $onlyMapServer == 0) {
            $queries['onlyMapServer'] = $onlyMapServer;
        }

        $queryString = "?".http_build_query($queries);

        return $this
            ->setRouter(['climate', 'rain'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }

    /**
     * Rota para salvar dado de chuva semanal
     *
     * @param $idLocale
     * @param \DateTime $dateBegin
     * @param \DateTime $dateEnd
     * @param $ger
     * @param $rain
     * @param bool $forceUpdate
     * @return null|\stdClass
     */
    public function saveWeeklyRain(
        $idLocale,
        \DateTime $dateBegin,
        \DateTime $dateEnd,
        $ger,
        $rain,
        $forceUpdate = false
    ) {
        $queryString = "?".http_build_query([
                "idLocale" => $idLocale,
                "ger" => $ger,
                "rain" => $rain,
                "dateBegin" => $dateBegin->format("Y-m-d"),
                "dateEnd" => $dateEnd->format("Y-m-d")
            ]);

        return $this
            ->setRouter(['mrs', 'rain', 'week', 'save'])
            ->addQueryString($queryString)
            ->request();
    }

    /**
     * Rota para retornar dado de chuva semanal
     *
     * @param \DateTime $dateBegin
     * @param \DateTime $dateEnd
     * @param bool $forceUpdate
     * @return null|\stdClass
     */
    public function getWeeklyRain(
        \DateTime $dateBegin,
        \DateTime $dateEnd,
        $forceUpdate = false
    ) {
        $queryString = "?".http_build_query([
                "dateBegin" => $dateBegin->format("Y-m-d"),
                "dateEnd" => $dateEnd->format("Y-m-d")
            ]);

        return $this
            ->setRouter(['mrs', 'rain', 'week'])
            ->addQueryString($queryString)
            ->request();
    }

    /**
     * Rota para editar dado de chuva climática mensal
     *
     * @param $idLocale
     * @param \DateTime $month
     * @param $forecastRain
     * @param bool $forceUpdate
     * @return null|\stdClass
     */
    public function editClimateRain(
        $idLocale,
        \DateTime $month,
        $forecastRain,
        $forceUpdate = false
    ) {
        $queryString = "?".http_build_query([
                "idLocale" => $idLocale,
                "month" => $month->format("Y-m"),
                "forecastRain" => $forecastRain
            ]);

        return $this
            ->setRouter(['climate', 'rain', 'edit'])
            ->addQueryString($queryString)
            ->request();
    }
}
