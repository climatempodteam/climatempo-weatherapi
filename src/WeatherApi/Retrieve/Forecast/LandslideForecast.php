<?php

namespace WeatherApi\Retrieve\Forecast;

use WeatherApi\Retrieve\AbstractRetrieve;
use DateTime;

/**
 * Class LandslideForecast
 *
 * @package WeatherApi\Retrieve\Forecast
 *
 * @author  Willian Dener <willian.dener@climatempo.com.br>
 * @version 1.0
 */

class LandslideForecast extends AbstractRetrieve
{

    /**
     * Retorna dados de risco deslizamento Diário
     * dados coletados da api da Somar
     *
     * @param $latitude
     * @param $longitude 
     * @param $periodType     
     */

    public function getDailyLandslide($latitude, $longitude, $forceUpdate = false)
    {
        $queryString = "?" . http_build_query([
            'latitude' => $latitude,
            'longitude' => $longitude,
            'periodType' => 'daily'
        ]);

        return $this
            ->setRouter(['forecast', 'landslide'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }


     /**
     * Retorna dados de risco deslizamento horario
     * dados coletados da api da Somar
     *
     * @param $latitude
     * @param $longitude
     * @param $periodType     
     */

    public function getHourlyLandslide($latitude, $longitude, $forceUpdate = false)
    {
        $queryString = "?" . http_build_query([
            'latitude' => $latitude,
            'longitude' => $longitude,
            'periodType' => 'hourly'
        ]);

        return $this
            ->setRouter(['forecast', 'landslide'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }

    /**
     * Retorna dados de risco deslizamento no periodo de 6 horas
     * dados coletados da api da Somar
     *
     * @param $latitude
     * @param $longitude   
     * @param $periodType   
     */

    public function get6HoursLandslide($latitude, $longitude, $forceUpdate = false)
    {
        $queryString = "?" . http_build_query([
            'latitude' => $latitude,
            'longitude' => $longitude,
            'periodType' => '6hours'
        ]);

        return $this
            ->setRouter(['forecast', 'landslide'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }

}
