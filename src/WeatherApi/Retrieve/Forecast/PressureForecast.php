<?php

namespace WeatherApi\Retrieve\Forecast;

use WeatherApi\Retrieve\AbstractRetrieve;

/**
 * Class PressureForecast
 *
 * @package WeatherApi\Retrieve\Forecast
 * @author  Guilherme Santos <guilhermedossantos91@gmail.com>
 * @author  Danilo Luz <danilo.luz@climatempo.com.br>
 * @version 1.1.0
 */
class PressureForecast extends AbstractRetrieve
{

    /**
     * @param int $idLocale
     * @param bool $forceUpdate
     * @param int $onlyMapServer
     * @return mixed
     */
    public function getDaily15D($idLocale, $forceUpdate = false, $onlyMapServer = 0)
    {
        $queries = [
            'idlocale' => $idLocale
        ];

        if ($onlyMapServer === 1) {
            $queries['onlyMapServer'] = $onlyMapServer;
        }

        $queryString = '?'.http_build_query($queries);

        return $this
            ->setRouter(['forecast', '15days', 'pressure'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }


    /**
     * @param int $idLocale
     * @param string|null $timezone
     * @param bool|false $forceUpdate
     * @return null|\stdClass
     */
    public function getHourly72H(
        $idLocale,
        $timezone = null,
        $forceUpdate = false
    )
    {
        $queryString = "?idlocale={$idLocale}";

        if (!is_null($timezone)) {
            $queryString .= "&timezone={$timezone}";
        }

        return $this
            ->setRouter(['forecast', '72hours', 'pressure'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }


    /**
     * @param int $idLocale
     * @param \DateTime $begin
     * @param \DateTime|null $end
     * @param null $timezone
     * @param bool|false $forceUpdate
     * @return null|\stdClass
     */
    public function getHourly(
        $idLocale,
        \DateTime $begin,
        \DateTime $end = null,
        $timezone = null,
        $forceUpdate = false
    )
    {
        $queryString = "?idlocale={$idLocale}&dateBegin={$begin->format("Y-m-d")}";

        if (!is_null($end)) {
            $queryString .= "&dateEnd={$end->format("Y-m-d")}";
        }
        if (!is_null($timezone)) {
            $queryString .= "&timezone={$timezone}";
        }
        return $this
            ->setRouter(['forecast', 'hourly', 'pressure'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }

    /**
     * @param int $idLocale
     * @param \DateTime $begin
     * @param \DateTime|null $end
     * @param string|null $timezone
     * @param bool|false $forceUpdate
     * @return mixed
     */
    public function getDaily(
        $idLocale,
        \DateTime $begin,
        \DateTime $end = null,
        $timezone = null,
        $forceUpdate = false
    )
    {
        $queryString = '?' . http_build_query([
            'idlocale' => $idLocale ? $idLocale : '',
            'dateBegin' => $begin ? $begin->format('Y-m-d') : '',
            'dateEnd'   => $end ? $end->format('Y-m-d') : '',
            'timezone' => $timezone ? $timezone : ''
        ]);

        return $this
            ->setRouter(['forecast', 'period', 'pressure'])
            ->addQueryString($queryString)
            ->request();
    }
}