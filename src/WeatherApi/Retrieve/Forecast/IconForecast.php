<?php

namespace WeatherApi\Retrieve\Forecast;

use WeatherApi\Retrieve\AbstractRetrieve;

/**
 * Class IconForecast
 *
 * @package WeatherApi\Retrieve\Forecast
 */
class IconForecast extends AbstractRetrieve
{

    /**
     * @param int $idLocale
     * @param bool|false $forceUpdate
     * @param int|0 $onlyMapServer
     * @return mixed
     */
    public function getDaily($idLocale, $forceUpdate = false, $onlyMapServer = 0)
    {
        $queries = [
            'idlocale' => $idLocale
        ];

        if ($onlyMapServer === 1) {
            $queries['onlyMapServer'] = $onlyMapServer;
        }

        $queryString = '?'.http_build_query($queries);

        return $this
            ->setRouter(['forecast', '15days', 'icon'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }
}
