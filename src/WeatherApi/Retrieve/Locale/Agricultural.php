<?php

namespace WeatherApi\Retrieve\Locale;

use WeatherApi\Retrieve\AbstractRetrieve;

/**
 * Class Agricultural
 * @package WeatherApi\Retrieve\Locale
 */
class Agricultural extends AbstractRetrieve
{

    /**
     * @param int $idLocale
     * @param bool $forceUpdate
     * @return mixed
     */
    public function getCulture($idLocale, $forceUpdate = false)
    {
        return $this
            ->setRouter(['crops', 'locale'])
            ->addQueryString("?idLocale={$idLocale}")
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    [
                        "id" => $idLocale
                    ]
                ),
                $forceUpdate
            );
    }
}