<?php

namespace WeatherApi\Retrieve\Locale;

use WeatherApi\Retrieve\AbstractRetrieve;

/**
 * Class Locale
 * @package WeatherApi\Retrieve\Locale
 */
class Locale extends AbstractRetrieve
{

    /**
     * @param int $idLocale
     * @param bool $forceUpdate
     * @return mixed
     */
    public function getLocaleById($idLocale, $forceUpdate = false)
    {
        $queryString = "?" . http_build_query([
            'idLocale' => $idLocale
        ]);
        
        return $this
            ->setRouter(['locale'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(__METHOD__, $queryString),
                $forceUpdate
            );
    }
}