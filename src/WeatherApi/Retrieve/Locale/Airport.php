<?php

namespace WeatherApi\Retrieve\Locale;

use WeatherApi\Retrieve\AbstractRetrieve;

/**
 * @author Lucas de Oliveira <lucas.oliveira@climatempo.com.br>
 * @version 1.0.0
 */
class Airport extends AbstractRetrieve
{

    /**
     * @param bool $forceUpdate
     * @return mixed
     */
    public function getAll($forceUpdate = false)
    {
        return $this
            ->setRouter(['locales', 'airports', 'runways', 'thresholds'])
            ->manageCache(
                $this->formatCacheName(__METHOD__),
                $forceUpdate
            );
    }

    /**
     * @param int $localeId
     * @param bool $forceUpdate
     * @return null|\stdClass
     */
    public function getOneById($localeId, $forceUpdate = false)
    {
        $queryString = [ 'localeId' => $localeId ];

        return $this
            ->setRouter(['locales', 'airports', 'runways', 'thresholds?'])
            ->setQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(__METHOD__, http_build_query($queryString)),
                $forceUpdate
            );
    }
}
