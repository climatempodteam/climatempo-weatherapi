<?php

namespace WeatherApi\Retrieve\Locale;

use WeatherApi\Retrieve\AbstractRetrieve;

/**
 * Class City
 * @package WeatherApi\Retrieve\Locale
 * @author Adilson Ferreira <adilson@climatempo.com.br>
 * @version 1.0.0
 */
class City extends AbstractRetrieve
{

    /**
     * @param bool $forceUpdate
     * @return mixed
     */
    public function getAllCities($forceUpdate = false)
    {
        return $this
            ->setRouter(['locales', 'cities'])
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__
                ),
                $forceUpdate
            );
    }
}