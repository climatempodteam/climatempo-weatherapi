<?php


namespace WeatherApi\Retrieve\Occurred;


use WeatherApi\Retrieve\AbstractRetrieve;

/**
 * Class OccuredRain
 *
 * @package WeatherApi\Retrieve\Occurred
 * @author Gustavo Vitorino - gustavo.vittorino@climatempo.com.br
 * @author Danilo Luz - danilo.luz@climatempo.com.br
 * @version 1.1.1
 */
class OccuredRain extends AbstractRetrieve
{

    /**
     * @param $idLocale
     * @param \DateTime $begin
     * @param \DateTime|null $end
     * @param bool $forceUpdate
     * @return null|\stdClass
     */
    public function getDaily(
        $idLocale,
        \DateTime $begin,
        \DateTime $end = null,
        $forceUpdate = false
    )
    {
        $queryString = "?idLocale={$idLocale}&dateBegin={$begin->format("Y-m-d")}";

        if (!is_null($end)) {
            $queryString .= "&dateEnd={$end->format("Y-m-d")}";
        }

        return $this
            ->setRouter(['occurred', 'rain'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }

    /**
     * Utilizado no projeto Previsao Acerto 5 dias
     *
     * @param $idLocale
     * @param \DateTime $occurredDate
     * @param $rain
     * @param bool $forceUpdate
     * @return null|\stdClass
     */
    public function save(
        $idLocale,
        \DateTime $occurredDate,
        $rain,
        $forceUpdate = false
    )
    {
        $queryString = "?idLocale={$idLocale}&occurredDate={$occurredDate->format("Y-m-d")}";
        $queryString.= "&rain={$rain}";

        return $this
            ->setRouter(['occurred', 'rain', 'save'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }
}