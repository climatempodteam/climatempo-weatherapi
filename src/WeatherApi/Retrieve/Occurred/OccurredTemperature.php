<?php

namespace WeatherApi\Retrieve\Occurred;

use WeatherApi\Retrieve\AbstractRetrieve;

/**
 * Class OccurredTemperature
 *
 * @author Gustavo Vitorino <gustavo.vittorino@climatempo.com.br>
 * @author Danilo Luz - danilo.luz@climatempo.com.br
 * @version 1.2.1
 */
class OccurredTemperature extends AbstractRetrieve
{

    /**
     * @param $idStation
     * @param \DateTime $begin
     * @param \DateTime|null $end
     * @param bool $forceUpdate
     * @return null|\stdClass
     */
    public function getDaily(
        $idStation,
        \DateTime $begin,
        \DateTime $end = null,
        $forceUpdate = false
    )
    {
        $queryString = "?idStation={$idStation}&dateBegin={$begin->format("Y-m-d")}";

        if (!is_null($end)) {
            $queryString .= "&dateEnd={$end->format("Y-m-d")}";
        }

        return $this
            ->setRouter(['occurred', 'temperature'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }

    /**
     * Obtem dados ocorridos de temperatura sem a prioridade de estações
     *
     * @param $idLocale
     * @param \DateTime $begin
     * @param \DateTime|null $end
     * @param bool $forceUpdate
     * @return null|\stdClass
     * @internal param $idStation
     */
    public function getAccuracyDaily(
        $idLocale,
        \DateTime $begin,
        \DateTime $end = null,
        $forceUpdate = false
    )
    {
        $queryString = "?idLocale={$idLocale}&dateBegin={$begin->format("Y-m-d")}";

        if (!is_null($end)) {
            $queryString .= "&dateEnd={$end->format("Y-m-d")}";
        }

        return $this
            ->setRouter(['accuracy', 'occurred', 'temperature'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }

    /**
     * Utilizado no projeto Previsao Acerto 5 dias
     *
     * @param $idLocale
     * @param String $type
     * @param \DateTime $occurredDate
     * @param $temperature
     * @param bool $forceUpdate
     * @return null|\stdClass
     */
    public function save(
      $idLocale,
      $type,
      \DateTime $occurredDate,
      $temperature,
      $forceUpdate = false
    )
    {
        $queryString = "?idLocale={$idLocale}&occurredDate={$occurredDate->format("Y-m-d")}";
        $queryString.= "&type={$type}&temperature={$temperature}";

        return $this
            ->setRouter(['occurred', 'temperature', 'save'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }
}