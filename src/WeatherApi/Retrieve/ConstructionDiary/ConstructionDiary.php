<?php

namespace WeatherApi\Retrieve\ConstructionDiary;

use WeatherApi\Retrieve\AbstractRetrieve;

/**
 * @author Lucas de Oliveira <lucas.oliveira@climatempo.com.br>
 * @author Lucas Porto de Deus <lucas.deus@climatempo.com.br>
 * @version 1.1.0
 */
class ConstructionDiary extends AbstractRetrieve
{

    /**
     * @param int $stationId
     * @param int $userId
     * @param \DateTime $observedDate
     * @param string $comment
     * @return \stdClass
     */
    public function save($stationId, $userId, \DateTime $observedDate, $comment)
    {
        $postData = [
            'stationId'    => (int) $stationId,
            'clientId'     => (int) $userId,
            'dateObserved' => $observedDate->format('Y-m-d'),
            'comment'      => (string) $comment
        ];

        $queryString = '?' . http_build_query($postData);

        return $this
            ->setRouter(['observed', 'construction-diary', 'save'])
            ->addQueryString($queryString)
            ->request();
    }

    /**
     * @param int $userId Id do usuário
     * @param int $stationId Id da estação
     * @param string $dateBegin Data de início (YYYY-MM-DD)
     * @param string $dateEnd Data de fim (YYYY-MM-DD)
     * @return NULL|\stdClass
     */
    public function getComments(
        $userId,
        $stationId,
        $dateBegin,
        $dateEnd
    ) {
        $queryString = '?' . http_build_query([
            'clientId'  => $userId,
            'stationId' => $stationId,
            'dateBegin' => $dateBegin,
            'dateEnd'   => $dateEnd
        ]);

        return $this
            ->setRouter(['observed', 'construction-diary', 'show'])
            ->addQueryString($queryString)
            ->request();
    }
}
