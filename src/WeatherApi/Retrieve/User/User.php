<?php

namespace WeatherApi\Retrieve\User;

use WeatherApi\Retrieve\AbstractRetrieve;

/**
 * Class User
 *
 * @package WeatherApi\Retrieve\User
 * @author  Guilherme Santos <guilhermedossantos91@gmail.com>
 * @author  Lucas de Oliveira <lucas.oliveira@climatempo.com.br>
 * @author  Gustavo Santos <gustavo.santos@climatempo.com.br>
 * @author  Luiz Goncalves <luiz.goncalves@climatempo.com.br>
 * @author  Mauricio Ribeiro <mauricio.ribeiro@climatempo.com.br>
 * @version 1.6.0
 */
class User extends AbstractRetrieve
{
    /**
     * @param string $username
     * @param string $password
     * @param int $product
     *
     * @return mixed
     */
    public function authenticate(string $username, string $password, int $product)
    {
        $queryString = "?username={$username}&password={$password}&product={$product}";

        return $this
            ->setRouter(['jwt', 'auth'])
            ->addQueryString($queryString)
            ->request();
    }

    /**
     * @param string $email
     * @param int $clientId
     *
     * @return mixed
     */
    public function authenticateByEmail(string $email, int $clientId)
    {
        $queryString = "?targetUserEmail={$email}&targetClientId={$clientId}";

        return $this
            ->setRouter(['jwt', 'auth', 'to', 'email'])
            ->addQueryString($queryString)
            ->request();
    }

    /**
     * @return mixed
     */
    public function authenticationAlive()
    {
        return $this
            ->setRouter(['jwt', 'alive'])
            ->request();
    }

    /**
     * @return mixed
     */
    public function authenticationStatus()
    {
        return $this
            ->setRouter(['jwt', 'validate'])
            ->request();
    }

    /**
     * @param string $token
     * @param string $refreshToken
     * @return mixed
     */
    public function refreshToken(string $token, string $refreshToken)
    {
        return $this
            ->setRouter(['jwt', 'refresh'])
            ->post(['authToken' => $token, 'refreshToken' => $refreshToken])
            ->exec();
    }

    /**
     * @param string $username
     * @param array
     *
     * @return mixed
     */
    public function getTemporaryToken(string $username)
    {
        $queryString = "?username={$username}";

        return $this
            ->setRouter(['user', 'generate-token'])
            ->addQueryString($queryString)
            ->request();
    }

    /**
     * @param string $tempToken
     * @param string $newPassword
     * @param array
     *
     * @return mixed
     */
    public function updatePassword(string $tempToken, string $newPassword)
    {
        $queryString = "?tempToken={$tempToken}&newPassword={$newPassword}";

        return $this
            ->setRouter(['user', 'change-password'])
            ->addQueryString($queryString)
            ->request();
    }

    /**
     * @param bool|false $forceUpdate
     * @return null|\stdClass
     */
    public function get($forceUpdate = false)
    {

        $queryString = "?sct=" . hash("adler32", $this->config->getSecret());

        return $this
            ->setRouter(['user'])
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }
    
    /**
     * @param string $email
     * @param string $pwd
     * @param string $productId
     * @param bool|false $forceUpdate
     * @return null | \stdClass
     */
    public function getPayment($email = null, $pwd = null, $productId = null, $forceUpdate = false)
    {
        $query = [
              "email" => $email,
              "password" => $pwd,
              "time" => time()
        ];
  
        if (is_int($productId)) {
            $query['productId'] = $productId;
        }

        $queryString = "?".http_build_query($query);

        return $this
            ->setRouter(['payment', 'user'])
            ->addQueryString($queryString)
            ->request();
    }
    
    /**
     * Retorna dados de um usuario atraves do token e id do plano
     *
     * @param null $token
     * @param null $planId
     * @param bool $forceUpdate
     * @return mixed
     */
    public function getUserToken($token = null, $planId = null, $forceUpdate = false)
    {
        $queryString = "?token={$token}";

        if (is_int($planId)) {
            $queryString .= "&planId={$planId}&time=" . time();
        }

        return $this
            ->setRouter(['payment', 'user', 'token', 'auth'])
            ->addQueryString($queryString)
            ->request();
    }

    /**
     * @param int $moduleId
     * @param bool|false $forceUpdate
     * @return null|\stdClass
     */
    public function getLocales($moduleId, $forceUpdate = false)
    {
        $queryString = "?id={$moduleId}";

        return $this
            ->setRouter(['module', 'locales'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }

    /**
     * @param int $localeId
     * @param bool|false $forceUpdate
     * @return null|\stdClass
     */
    public function getLocale($localeId, $forceUpdate = false)
    {
        $queryString = "?localeId={$localeId}";

        return $this
            ->setRouter(['locale'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );


    }

    /**
     * @param $planId
     * @param int $alluser
     * @param bool $forceUpdate
     * @return null|\stdClass
     */
    public function getbyPlan($planId, $alluser = 0,  $forceUpdate = false)
    {
        $queryString = "?filter=1&planId={$planId}&alluser={$alluser}";

        return $this
            ->setRouter(['payment', 'user'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }

    /**
     * 
     * @return null|\stdClass
     */
    public function getGroupLocales()
    {
        return $this
            ->setRouter(['user', 'groupLocales', 'list'])
            ->request();
    }

    /**
     * @param Int $groupId
     * 
     * @return null|\stdClass
     */
    public function getLocalesByGroup($groupId)
    {
        $queryString = "?groupId={$groupId}";

        return $this
            ->setRouter(['user', 'groupLocales', 'locales', 'list'])
            ->addQueryString($queryString)
            ->request();
    }
}
