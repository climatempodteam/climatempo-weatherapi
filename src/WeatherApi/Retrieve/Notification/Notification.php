<?php

namespace WeatherApi\Retrieve\Notification;

use WeatherApi\Retrieve\AbstractRetrieve;

/**
 * Class Notification
 *
 * @package WeatherApi\Retrieve\Notification
 */
class Notification extends AbstractRetrieve
{
    /**
     * Busca notificações do usuário
     *
     * @return null|\stdClass
     */
    public function getNotifications($limit = null)
    {
        if ($limit) {
            $queryString = '?'.http_build_query([
                'limit' => $limit
            ]);

            return $this->setRouter(['notifications'])
                ->addQueryString($queryString)
                ->request();
        }
        return $this
            ->setRouter(['notifications'])
            ->request();
    }

    /**
     * Request para marcar notificação como lida baseado em seu ID
     *
     * @param  int $notificationId
     * @return \stdClass|null
     */
    public function markAsRead($notificationId)
    {
        $queryString = '?'.http_build_query([
            'notificationId' => $notificationId,
        ]);

        return $this
            ->setRouter(['notifications', 'read'])
            ->addQueryString($queryString)
            ->request();
    }

    /**
     * Request para marcar notificação como não lida baseado em seu ID
     *
     * @param  int $notificationId
     * @return \stdClass|null
     */
    public function markAsUnread($notificationId)
    {
        $queryString = '?'.http_build_query([
            'notificationId' => $notificationId,
        ]);

        return $this
            ->setRouter(['notifications', 'unread'])
            ->addQueryString($queryString)
            ->request();
    }
}
