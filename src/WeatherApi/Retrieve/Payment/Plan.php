<?php

namespace WeatherApi\Retrieve\Payment;

use WeatherApi\Retrieve\AbstractRetrieve;

/**
 * Class Plan
 *
 * @package WeatherApi\Retrieve\Payment
 * @author  Lucas de Oliveira <lucas.oliveira@climatempo.com.br>
 * @author Luiz Gonçalves <luiz.goncalves@climatempo.com.br>
 * @version 1.2.0
 */
class Plan extends AbstractRetrieve
{

    /**
     * Retorna todos planos da central e do gateway de pagamento
     *
     * @return array
     */
    public function getAll()
    {
        return $this
            ->setRouter(['payment', 'plans'])
            ->request();
    }

    /**
     * @param $productId
     * @param string $source
     * @return mixed
     * @throws \Exception
     */
    public function getAllByProductId($productId, $source = '')
    {
        $queryString = '?' . http_build_query([
            'productId' => $productId,
            'source' => $source
        ]);

        return $this
            ->setRouter(['payment', 'plans'])
            ->addQueryString($queryString)
            ->request();
    }

    /**
     * @param string $gatewayCode codigo do produto no gateaway de pagamento
     * @return array
     */
    public function getAllByGatewayCode($gatewayCode)
    {
        $queryString = "?" . http_build_query([
            "gatewayCode" => $gatewayCode
        ]);

        return $this
            ->setRouter(['payment', 'plan'])
            ->addQueryString($queryString)
            ->request();
    }

    /**
     * @return array
     */
    public function getAllFromGateway()
    {
        return $this
            ->setRouter(['payment', 'gateway', 'plans'])
            ->request();
    }

    /**
     *
     * Atualiza dados de limites/modulo do plano
     * @param int $id
     * @param int $planId
     * @param string $value
     * @return mixed
     * @throws \Exception
     */
    public function updateModulePlan($id, $planId, $value)
    {
        $queryString = [
            "id" => $id,
            "planId" => $planId,
            "value" => $value
        ];

        return $this
            ->setRouter(['payment', 'plan', 'module', 'save'])
            ->post($queryString)
            ->exec();
    }
    
    /**
     * Atualiza dados do plano
     * @param int $id
     * @param string $name
     * @param string $description
     * @param string $value
     * @param string $customization
     * @return mixed
     * @throws \Exception
     */
    public function updatePlan(
        $id,
        $name,
        $description,
        $value,
        $customization
    ) {

        $queryString = [
            'id' =>  $id,
            'name' => $name,
            'description' => $description,
            'value' => $value,
            'customization' => $customization
        ];

        return $this
            ->setRouter(['payment', 'plan', 'update'])
            ->post($queryString)
            ->exec();
    }

    /**
     * @param string $alias
     * @param string $source
     * @return mixed
     * @throws \Exception
     */
    public function getByAlias($alias, $source = '')
    {
        $queryString = '?' . http_build_query([
                'alias' => $alias,
                'source' => $source
            ]);

        return $this
            ->setRouter(['payment', 'plans'])
            ->addQueryString($queryString)
            ->request();
    }

    /**
     * Retorna a customização de um plano pelo id
     * @param string $planId
     * @return array
     * @throws \Exception
     */
    public function getOneCustomizationByPlanId($planId)
    {
        $queryString = "?" . http_build_query([
                "planId" => $planId
            ]);

        return $this
            ->setRouter(['payment', 'plan', 'customization'])
            ->addQueryString($queryString)
            ->request();
    }

}
