<?php

namespace WeatherApi\Retrieve\Payment;

use WeatherApi\Retrieve\AbstractRetrieve;

/**
 * Class Subscription
 *
 * Efetua operações para gerenciar assinaturas do usuário
 *
 * @package WeatherApi\Retrieve\Payment
 * @author  Gustavo Santos <gustavos.santos@climatempo.com.br>
 * @author  Lucas de Oliveira <lucas.oliveira@climatempo.com.br>
 * @author  Luiz Goncalves <luiz.goncalves@climatempo.com.br>
 * @version 1.4.0
 */
class Subscription extends AbstractRetrieve
{
    /**
     * Retorna lista de assinaturas de um usuario
     *
     * @param $email
     * @return $mixed
     */
    public function getListSubscriptions($email)
    {
        $queryString = '?email='.$email;

        return $this
            ->setRouter(['payment', 'subscription'])
            ->addQueryString($queryString)
            ->request();
    }

    /**
     * Atualiza dados da assinatura assinatura
     *
     * @param $id
     * @param $dateEnd
     * @param $price
     * @param $logo
     * @return mixed
     * @throws \Exception
     */
    public function updateSubscription($id, $dateEnd, $price = null, $logo = null)
    {
        $queryString = [
            "code" => $id,
            "price" => $price,
            "dateEnd" => $dateEnd,
            "logo" => $logo
        ];

        return $this
            ->setRouter(['payment', 'subscription', 'update'])
            ->post($queryString)
            ->exec();
    }

    /**
     * Renova data e preço da assinatura
     *
     * @param $id
     * @param $dateEnd
     * @param $price
     * @return mixed
     * @throws \Exception
     */
    public function renovateSubscription($id, $dateEnd, $price = null)
    {

        $queryString = '?'. http_build_query([
            "code" => $id,
            "price" => $price,
            "dateEnd" => $dateEnd
        ]);

        return $this
            ->setRouter(['payment', 'subscription', 'renovate'])
            ->addQueryString($queryString)
            ->request();
    }

    /**
     * Atualiza assinaturas do banco de dados com os dados do gateway
     *
     * @return string
     * @throws \Exception
     */
    public function syncDatabaseWithGateway()
    {
        return $this
            ->setRouter(['payment', 'subscription', 'update-database'])
            ->request();
    }

    /**
     *
     * Atualiza dados de limites/modulo de uma assinatura
     * @param $id
     * @param $subscriptionId
     * @param $value
     * @return mixed
     * @throws \Exception
     */
    public function updateModuleSubscription($id, $subscriptionId, $value)
    {
        $queryString = '?'. http_build_query([
                "id" => $id,
                "subscriptionId" => $subscriptionId,
                "value" => $value
            ]);

        return $this
            ->setRouter(['payment', 'subscription', 'module', 'save'])
            ->addQueryString($queryString)
            ->request();
    }

    /**
     *
     * Atualiza assinatura
     * @param int $userId
     * @param int $subscriptionId
     * @param int $registryCode
     * @param int $phone
     * @param int $zipCode
     * @param string $street
     * @param int $number
     * @param string $district
     * @param string  $city
     * @param string  $state
     * @param int $planId
     * @param int $gatewayPlanId
     * @param int $quantity
     * @param $paymentMethod
     * @param string  $holderName
     * @param int $cardNumber
     * @param int $cardCvv
     * @param int $cardExpiration
     * @return mixed
     * @throws \Exception
     */
    public function upgradeSubscription(
        $userId,
        $subscriptionId,
        $registryCode,
        $phone,
        $zipCode,
        $street,
        $number,
        $district,
        $city,
        $state,
        $planId,
        $gatewayPlanId,
        $quantity,
        $paymentMethod = null,
        $holderName = null,
        $cardNumber = null,
        $cardCvv  = null,
        $cardExpiration = null
    ){
        $queryString = '?'. http_build_query([
                'userId'=> $userId,
                'subscriptionId'=> $subscriptionId,
                'registryCode'=> $registryCode,
                'phone'=> $phone,
                'zipCode'=> $zipCode,
                'street'=> $street,
                'number'=> $number,
                'district'=> $district,
                'city'=> $city,
                'state'=> $state,
                'planId'=> $planId,
                'gatewayPlanId'=> $gatewayPlanId,
                'quantity'=> $quantity,
                'paymentMethod'=> $paymentMethod,
                'holderName'=> $holderName,
                'cardNumber'=> $cardNumber,
                'cardCvv'=> $cardCvv,
                'cardExpiration'=> $cardExpiration
            ]);

        return $this
            ->setRouter(['payment','subscription','upgrade-plan-subscription'])
            ->addQueryString($queryString)
            ->request();
    }
}
