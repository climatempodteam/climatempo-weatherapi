<?php

namespace WeatherApi\Retrieve\Payment;

use WeatherApi\Error\WeatherApiException;
use WeatherApi\Retrieve\AbstractRetrieve;

/**
 * Class Bill
 *
 * Realiza operações para gerarenciar as faturas do usuário
 *
 * @package WeatherApi\Retrieve\Payment
 * @author  Gustavo Santos <gustavos.santos@climatempo.com.br>
 * @author  Lucas de Oliveira <gustavos.santos@climatempo.com.br>
 * @version 1.1.0
 */
class Bill extends AbstractRetrieve
{

    /**
     * @param int $customerId
     * @param int $subscriptionId
     * @return mixed
     * @throws \Exception|WeatherApiException
     */
    public function getListBillSubscription($customerId, $subscriptionId)
    {
        $queryString = '?'.http_build_query([
                'customerId' => $customerId,
                'subscriptionId' => $subscriptionId
            ]);

        return $this
            ->setRouter(['payment', 'bill', 'customer', 'subscription'])
            ->addQueryString($queryString)
            ->request();
    }

    /**
     * @param int $chargeId
     * @return mixed
     * @throws \Exception|WeatherApiException
     */
    public function charge($chargeId)
    {
        $queryString = '?' . http_build_query([
            'chargeId' => $chargeId
        ]);

        return $this
            ->setRouter(['payment', 'charge'])
            ->addQueryString($queryString)
            ->request();
    }
}
