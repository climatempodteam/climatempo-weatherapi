<?php

namespace WeatherApi\Retrieve\Payment;

use WeatherApi\Retrieve\AbstractRetrieve;

/**
 * Classe responsável por lidar com as notificações
 * 
 * @author  Gustavo Santos Thiago <gustavo.santos@climatempo.com.br>
 * @author  Bruno Zepelini <bruno.zepelini@climatempo.com.br>
 * @version 1.0.0
 */
class Notification extends AbstractRetrieve
{

    /**
     * Busca por todas as notificações por produto
     * 
     * @param int $productId 
     * 
     * @return \stdClass
     */
    public function getByProduct($productId)
    {
        $queryString = '?' . http_build_query(
            [
                'productId' => $productId
            ]
        );
        return $this
            ->setRouter(['payment', 'notification'])
            ->addQueryString($queryString)
            ->request();
    }

    /**
     * Busca uma notificação pelo ID e pela assinatura
     * 
     * @param int $subscriptionId 
     * @param int $notificationId 
     * 
     * @return \stdClass
     */
    public function getNotificationSubscription($subscriptionId, $notificationId)
    {
        $queryString = '?' . http_build_query(
            [
                'notificationId' => $notificationId,
                'subscriptionId' => $subscriptionId
            ]
        );
        return $this
            ->setRouter(['payment', 'notification-subscription'])
            ->addQueryString($queryString)
            ->request();
    }

    /**
     * Salva um novo contato para ser notificado
     * 
     * @param int    $subscriptionId 
     * @param int    $notificationId 
     * @param string $email 
     * @param string $points 
     * @param int    $isIncrementPoint
     *
     * @return \stdClass
     */
    public function getSaveContactOfNotification($subscriptionId, $notificationId, $email, $points = null, $isIncrementPoint = 0)
    {
        $queryString = '?' . http_build_query(
            [
                'notificationId' => $notificationId,
                'subscriptionId' => $subscriptionId,
                'email' => $email,
                'points' => $points,
                'isIncrementPoint' => $isIncrementPoint
            ]
        );
        return $this
            ->setRouter(['payment', 'notification-subscription', 'save'])
            ->addQueryString($queryString)
            ->request();
    }

    /**
     * Apaga um contato das notificações
     * 
     * @param array $notificationParameters (ID ou E-mail do contato)
     * 
     * @return \stdClass
     */
    public function getRemoveContactOfNotification($notificationParameters)
    {
        $queryString = '?' . http_build_query($notificationParameters);
        return $this
            ->setRouter(['payment', 'notification-subscription', 'delete'])
            ->addQueryString($queryString)
            ->request();
    }
}
