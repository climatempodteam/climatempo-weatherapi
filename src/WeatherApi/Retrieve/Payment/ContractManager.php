<?php
namespace WeatherApi\Retrieve\Payment;

use WeatherApi\Error\WeatherApiException;
use WeatherApi\Retrieve\AbstractRetrieve;

/**
 * Class ContractManager
 *
 * @package WeatherApi\Retrieve\Payment
 * @author  Gustavo Santos <gustavos.santos@climatempo.com.br>
 * @author  Luiz Gonçalves <luiz.goncalves@climatempo.com.br>
 * @version 1.5.0
 */
class ContractManager extends AbstractRetrieve
{
    /**
     * @return mixed
     * @throws \Exception|WeatherApiException
     */
    public function getAllContracts()
    {
        return $this
            ->setRouter(['payment', 'contract', 'list'])
            ->request();
    }

    /**
     * @param $contractId
     * @param bool $forceUpdate
     * @return null|\stdClass
     */
    public function getContractById($contractId, $forceUpdate = false)
    {
        $queryString = "?contractId=" . $contractId;

        return $this
            ->setRouter(['payment', 'contract', 'list'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(__METHOD__, $queryString),
                $forceUpdate
            );
    }

    /**
     * @param  int $id
     * @param  bool $status
     * @return mixed
     * @throws \Exception|WeatherApiException
     */
    public function changeStatusContract($id, $status)
    {
        $queryString = '?' . http_build_query([
            'contractId' => $id,
            'status' => $status
        ]);

        return $this
            ->setRouter(['payment', 'contract', 'change-status'])
            ->addQueryString($queryString)
            ->request();

    }

    /**
     * @param  string $name
     * @param  string $contractCode
     * @param  int $interval
     * @param  string $intervalType
     * @param  bool $status
     * @param  int $planId
     * @param  int $subscriptionQuantity
     * @param  int $pointsQuantity
     * @param  int $contractId
     * @param  string $contractType
     * @return array
     * @throws \Exception|WeatherApiException
     */
    public function createOrUpdateContract(
        $name,
        $contractCode,
        $interval,
        $intervalType,
        $contractType,
        $status,
        $planId,
        $subscriptionQuantity,
        $pointsQuantity,
        $contractId = null
    )
    {
        $queryString = '?' . http_build_query([
                'name' => $name,
                'contractCode' => $contractCode,
                'interval' => $interval,
                'intervalType' => $intervalType,
                'status' => $status,
                'contractType' => $contractType,
                'pointsQuantity' => $pointsQuantity,
                'planId' => $planId,
                'subscriptionQuantity' => $subscriptionQuantity
            ]);

        if (! is_null($contractId)) {
            $queryString .= '&contractId=' . $contractId;
        }

        return $this
            ->setRouter(['payment', 'contract'])
            ->addQueryString($queryString)
            ->request();
    }

    /**
     * @param $filter
     * @param $forceUpdate
     * @return mixed
     * @throws \Exception
     */
    public function getContractByFilter($filter, $forceUpdate =  false)
    {
        $queryString = "?filter=" . $filter;

        return $this
            ->setRouter(['payment', 'contract', 'list'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(__METHOD__, $queryString),
                $forceUpdate
            );
    }

    /**
     * @param $codeContract
     * @param $userData
     * @param $limit
     * @return mixed
     * @throws \Exception
     */
    public function importUsers($codeContract, $userData, $limit)
    {
        $queryString = [
            'userData' => $userData,
            'contractId' => $codeContract,
            'limit' => $limit
        ];

        return $this
            ->setRouter(['payment', 'user', 'import'])
            ->post($queryString)
            ->exec();
    }

}
