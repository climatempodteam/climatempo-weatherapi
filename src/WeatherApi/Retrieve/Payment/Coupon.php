<?php

namespace WeatherApi\Retrieve\Payment;

use WeatherApi\Retrieve\AbstractRetrieve;

/**
 * @author Lucas de Oliveira <lucas.oliveira@climatempo.com.br>
 * @version 1.0.0
 */
class Coupon extends AbstractRetrieve
{

    /**
     * @param string $couponCode
     * @param int $productId
     * @return \stdClass
     */
    public function getCouponByCodeAndProductId($couponCode, $productId = null)
    {
        $params = [ 'code' => $couponCode ];

        if (! is_null($productId)) {
            $params['productId'] = $productId;
        }

        $queryString = '?' . http_build_query($params);

        return $this
            ->setRouter(['payment', 'coupon', 'use', 'check'])
            ->addQueryString($queryString)
            ->request();
    }
}
