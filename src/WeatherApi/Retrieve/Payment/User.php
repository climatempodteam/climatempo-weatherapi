<?php

namespace WeatherApi\Retrieve\Payment;

use WeatherApi\Retrieve\AbstractRetrieve;

/**
 * Class User
 *
 * Realiza operações para gerenciar as ações do usuário
 *
 * @package WeatherApi
 * @subpackage WeatherApi\Retrieve\Payment
 */
class User extends AbstractRetrieve
{
    /**
     * Pega informações do clente através do email
     *
     * @param $email
     * @return mixed
     */
    public function getProfileCustomer($email)
    {
        $queryString = "?".http_build_query([
                "email" => $email,
            ]);

        return $this
            ->setRouter(['payment', 'user', 'customer'])
            ->addQueryString($queryString)
            ->request();
    }

    /**
     * Autentica usuario e resgata as informações da central de pagamentos
     *
     * @param $email
     * @param $pwd
     * @return mixed
     */
    public function getProfileAuth($email, $pwd)
    {
        $queryString = "?".http_build_query([
                "email" => $email,
                "password" => $pwd
            ]);

        return $this
            ->setRouter(['payment', 'user', 'auth'])
            ->addQueryString($queryString)
            ->request();
    }

    /**
     * Registra o usuário na central de pagamentos
     *
     * @param $name
     * @param $email
     * @param $pwd
     * @param $confirmPwd
     * @param $phone
     * @param $cpf
     * @param $cnpj
     * @param $hasStation
     *
     * @return mixed
     */
    public function createRegister($name, $email, $pwd, $confirmPwd, $phone = null, $cpf = null, $cnpj = null, $hasStation = null)
    {
        $queryString = "?".http_build_query([
                "name" => $name,
                "email" => $email,
                "confirmPassword" => $confirmPwd,
                "password" => $pwd,
                "phone" => $phone,
                "cpf" => $cpf,
                "cnpj" => $cnpj,
                "hasStation" => $hasStation
            ]);

        return $this
            ->setRouter(['payment', 'register'])
            ->addQueryString($queryString)
            ->request();
    }

    /**
     * @param $id
     * @param $name
     * @param $cpf
     * @param $cnpj
     * @param $phone
     * @param $email
     * @return mixed
     * @throws \Exception
     */
    public function editRegister($id, $name, $cpf, $cnpj, $phone, $email = null)
    {
        $queries = [
            'id'           => $id,
            'name'         => $name,
            'registryCode' => trim($cpf) == '' ? $cnpj : $cpf,
            'phone'        => $phone
        ];

        if ($email) {
            $queries['email'] = $email;
        }

        $queryString = '?' . http_build_query($queries);

        return $this
            ->setRouter(['payment', 'user', 'edit'])
            ->addQueryString($queryString)
            ->request();
    }


    /**
     * @param int $gatewayPlanId
     * @param int $gatewayCustomerId
     * @param int $planId
     * @param int $userId
     * @param int $fee
     * @param int $coupon
     * @param string $paymentMethod
     * @param int $cycles
     * @param int $installments
     * @param array $limits
     * @param array $productsGateway
     * @return mixed
     */
    public function createSubscription(
        $gatewayPlanId,
        $gatewayCustomerId,
        $planId,
        $userId,
        $fee,
        $coupon,
        $paymentMethod,
        $cycles,
        $installments,
        array $limits,
        array $productsGateway
    ) {
        $queryString = [
            'gatewayPlanId'     => $gatewayPlanId,
            'gatewayCustomerId' => $gatewayCustomerId,
            'planId'            => $planId,
            'userId'            => $userId,
            'fee'               => $fee,
            'limits'            => $limits,
            'coupon'            => $coupon,
            'paymentMethod'     => $paymentMethod,
            'cycles'            => $cycles,
            'installments'      => $installments,
            'gatewayProducts'   => $productsGateway
        ];

        return $this
            ->setRouter(['payment', 'subscription', 'create'])
            ->post($queryString)
            ->exec();
    }

    /**
     * Retorna perfil de pagamento
     *
     * @param $email
     * @return mixed
     */
    public function getProfilePayment($email)
    {
        $queryString = '?email='.$email;

        return $this
            ->setRouter(['payment', 'payment-profile'])
            ->addQueryString($queryString)
            ->request();
    }


    /**
     * Cadastra ou atualiza informações do cartão do cliente
     *
     * @param $email
     * @param $holderName
     * @param $cardExpiration
     * @param $cardNumber
     * @param $cardCvv
     * @return mixed
     */
    public function createProfilePayment($email, $holderName, $cardExpiration, $cardNumber, $cardCvv)
    {
        $queryString = "?".http_build_query([
                "email" => $email,
                "holderName" => $holderName,
                "cardExpiration" => $cardExpiration,
                "cardNumber" => $cardNumber,
                "cardCvv" => $cardCvv
            ]);

        return $this
            ->setRouter(['payment', 'payment-profile', 'create'])
            ->addQueryString($queryString)
            ->request();
    }

    /**
     * Cadastra ou atualiza endereço do usuario
     *
     * @param $email
     * @param $zipcode
     * @param $state
     * @param $city
     * @param $street
     * @param $neighborhood
     * @param $number
     * @param $additionalDetails
     * @return mixed
     */
    public function createAddress($email, $zipcode, $state, $city, $street, $neighborhood, $number, $additionalDetails)
    {
        $queryString = "?".http_build_query([
                "email" => $email,
                "zipcode" => $zipcode,
                "state" => $state,
                "city" => $city,
                "street" => $street,
                "neighborhood" => $neighborhood,
                "number" => $number,
                "additionalDetails" => $additionalDetails
            ]);

        return $this
            ->setRouter(['payment', 'user', 'address', 'edit'])
            ->addQueryString($queryString)
            ->request();
    }


    /**
     *
     * Reseta senha de um usuario por email
     * @param $email
     * @return mixed
     */
    public function resetPasswordByEmail($email)
    {
        $queryString = "?".http_build_query([
                "email" => $email
            ]);

        return $this
            ->setRouter(['payment','user','password','send'])
            ->addQueryString($queryString)
            ->request();

    }

    /**
     * @param array $criteria
     * @param bool  $forceUpdate
     *
     * @return null|\stdClass
     */
    public function findBy(array $criteria, $forceUpdate = false)
    {
        $queryString = '?' . http_build_query($criteria);

        return $this->setRouter(['payment', 'user', 'filter'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(__METHOD__, $queryString),
                $forceUpdate
            );
    }

    /**
     * @param int $userId
     * @param int $planId
     *
     * @return mixed
     */
    public function tokenGenerate($userId, $planId)
    {
        $queryString = '?' . http_build_query([
                'userId' => $userId,
                'planId' => $planId
            ]);

        return $this->setRouter(['payment', 'user', 'token', 'generate'])
            ->addQueryString($queryString)
            ->request();
    }

    /**
     * Importação de usuários
     *
     * @param int   $contractId
     * @param array $userData
     * @param array $limit
     * @return mixed
     */
    public function import($contractId, $userData, $limit)
    {
        $queryString = '?' . http_build_query([
            'contractId' => $contractId,
            'userData' => $userData,
            'limit' => $limit
        ]);

        return $this
            ->setRouter(['payment', 'user', 'import'])
            ->addQueryString($queryString)
            ->request();
    }


    /**
     * @param $id
     * @param $actualPassword
     * @param $newPassword
     * @param $confirmPassword
     * @return mixed
     * @throws \Exception
     */
    public function changePassword($id, $actualPassword, $newPassword, $confirmPassword)
    {
        $queryString = '?' . http_build_query([
                'id' => $id,
                'actualPassword' => $actualPassword,
                'newPassword' => $newPassword,
                'confirmPassword' => $confirmPassword
            ]);

        return $this
            ->setRouter(['payment','user','password'])
            ->addQueryString($queryString)
            ->request();
    }

    /**
     * @param int $subscriptionId
     * @param string $email
     * @param string $password
     * @param string $name
     * @param string|null $cpf
     * @param string|null $cnpj
     * @param string|null $phone
     * @return mixed
     * @throws \Exception
     */
    public function createNewSubuser($subscriptionId, $email, $password, $name, $cpf = null, $cnpj = null, $phone = null)
    {
        $queryString = '?' . http_build_query([
                'subscriptionId' => $subscriptionId,
                'email' => $email,
                'password' => $password,
                'name' => $name,
                'cpf' => $cpf,
                'cnpj' => $cnpj,
                'phone' => $phone
            ]);

        return $this
            ->setRouter(['payment', 'register', 'user-in-subscription'])
            ->addQueryString($queryString)
            ->request();
    }

    /**
     * @param int $subscriptionId
     * @param int $userId
     * @param int $subuserId
     * @param array $value Array com formato de json com todos os limites para o subusuario
     * @return mixed
     * @throws \Exception
     */
    public function updateLimitsForSubuser($subscriptionId, $userId, $subuserId, $value)
    {
        $queryString = [
            'subscriptionId' => $subscriptionId,
            'userId' => $userId,
            'subuserId' => $subuserId,
            'value' => json_encode($value)
        ];

        return $this
            ->setRouter(['payment', 'subscription', 'shared', 'module', 'save'])
            ->post($queryString)
            ->exec();
    }

    /**
     * @param $subscriptionId
     * @param $userId
     * @return mixed
     * @throws \Exception
     */
    public function getSubusersBy($subscriptionId, $userId)
    {
        $queryString = '?' . http_build_query([
                'subscriptionId' => $subscriptionId,
                'userId' => $userId
            ]);

        return $this
            ->setRouter(['payment', 'subscription', 'subuser', 'limits'])
            ->addQueryString($queryString)
            ->request();
    }

    /**
     * Retorna todos os usuários que tem data final da assinatura entre
     * o intervalo passado pelos parametros dateBegin e dateEnd
     *
     * @param \DateTime $dateBegin
     * @param \DateTime $dateEnd
     * @return mixed
     * @throws \Exception
     */
    public function getBySubscriptionEndDateBetween(\DateTime $dateBegin, \DateTime $dateEnd)
    {
        $query = [
            'dateBegin' => $dateBegin->format('Y-m-d'),
            'dateEnd' => $dateEnd->format('Y-m-d')
        ];

        $queryString = '?' . http_build_query([
            'dateBegin' => $query['dateBegin'],
            'dateEnd' => $query['dateEnd']
        ]);

        return $this
            ->setRouter(['payment', 'user', 'subscription-between'])
            ->addQueryString($queryString)
            ->request();
    }
}
