<?php

namespace WeatherApi\Retrieve\History;

use WeatherApi\Retrieve\AbstractRetrieve;

/**
 * Class RainHistory
 *
 * @package WeatherApi\Retrieve\History
 * @author Guilherme Santos <guilhermedossantos91@gmail.com>
 * @author Gustavo Vitorino <gustavo.vittorino@climatempo.com>
 * @version 1.2.0
 */
class RainHistory extends AbstractRetrieve
{
    /**
     * @param int $idLocale
     * @param \DateTime $begin
     * @param \DateTime|null $end
     * @param bool $forceUpdate
     * @return null|\stdClass
     */
    public function getDaily(
        $idLocale,
        \DateTime $begin,
        \DateTime $end = null,
        $forceUpdate = false
    )
    {
        $queryString = "?idlocale={$idLocale}&dateBegin={$begin->format("Y-m-d")}";

        if (!is_null($end)) {
            $queryString .= "&dateEnd={$end->format("Y-m-d")}";
        }

        return $this
            ->setRouter(['history', 'rain'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }

    /**
     * @param $idLocale
     * @param \DateTime $begin
     * @param \DateTime|null $end
     * @param bool $forceUpdate
     * @return null|\stdClass
     */
    public function getHourly(
        $idLocale,
        \DateTime $begin,
        \DateTime $end = null,
        $forceUpdate = false
    )
    {
        $queryString = "?idlocale={$idLocale}&dateBegin={$begin->format("Y-m-d")}";

        if (!is_null($end)) {
            $queryString .= "&dateEnd={$end->format("Y-m-d")}";
        }

        return $this
            ->setRouter(['history', 'hourly-rain'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }

    /**
     * @param $idLocale
     * @param \DateTime $begin
     * @param \DateTime|null $end
     * @param bool $forceUpdate
     * @return null|\stdClass
     */
    public function getForecast(
        $idLocale,
        \DateTime $begin,
        \DateTime $end = null,
        $forceUpdate = false
    )
    {
        $queryString = "?idLocale={$idLocale}&dateBegin={$begin->format("Y-m-d")}";

        if (!is_null($end)) {
            $queryString .= "&dateEnd={$end->format("Y-m-d")}";
        }

        return $this
            ->setRouter(['history', 'forecast', 'rain'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }
}