<?php

namespace WeatherApi\Retrieve\History;
use WeatherApi\Retrieve\AbstractRetrieve;

/**
 * Class LightningHistory
 * @package WeatherApi\Retrieve\History
 * @author Guilherme Santos <guilhermedossantos91@gmail.com>
 * @author Luiz Gonçalves <luiz.goncalves@climatempo.com.br>
 * @author Guilherme Tinem <guilherme.tinem@climatempo.com.br>
 * @version 1.2.0
 */
class LightningHistory extends AbstractRetrieve
{

    /**
     * @param int $idLocale
     * @param \DateTime $begin
     * @param \DateTime|null $end
     * @param array $sources
     * @param bool|false $forceUpdate
     * @return \stdClass|null
     */
    public function getDaily(
        $idLocale,
        \DateTime $begin,
        \DateTime $end = null,
        array $sources = null,
        $forceUpdate = false
    )
    {
        $query = [
            'idlocale' => $idLocale,
            'dateBegin' => $begin->format('Y-m-d'),
            'source' => []
        ];

        if (!is_null($end)) {
            $query['dateEnd'] = $end->format('Y-m-d');
        }

        if(!is_null($sources)) {
            foreach ($sources as $source ) {
                $query['source'][] = $source;
            }
        }

        $queryString = '?'.http_build_query($query);

        return $this
            ->setRouter(['history', 'lightning'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }

    /**
     * @param $latitude
     * @param $longitude
     * @param $distance
     * @param \DateTime $begin
     * @param \DateTime|null $end
     * @param bool $forceUpdate
     * @param array $sources
     * @return null|\stdClass
     */
    public function getDailyByCoordinates(
        $latitude,
        $longitude,
        $distance,
        \DateTime $begin,
        \DateTime $end = null,
        $forceUpdate = false,
        $sources = null
    )
    {

        $query = [
            'latitude' => $latitude,
            'longitude' => $longitude,
            'distance' => $distance,
            'dateBegin' => $begin->format("Y-m-d H:i:00")
        ];

        if (!is_null($end)) {
            $query['dateEnd'] = $end->format("Y-m-d H:i:00");
        }

        if (is_array($sources) && !empty($sources)) {
            $query['sources'] = implode(',', $sources);
        }

        $queryString = '?' . http_build_query($query);

        return $this
            ->setRouter(['history', 'lightning', 'coverage'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }

    /**
     * @param array $localeIds
     * @param \Datetime $dateBegin
     * @param \Datetime $dateEnd
     * @param array $sources
     * @return null|stdClass[]
     */
    public function getHistoryByLocaleIds(
        array $localeIds,
        \Datetime $dateBegin,
        \Datetime $dateEnd = null, 
        array $sources = []
    ) {
        $postParms = [
            'dateBegin' => $dateBegin->format('Y-m-d H:i:s'),
            'localeIds' => $localeIds
        ];

        if (!is_null($dateEnd)) {
            $postParms['dateEnd'] = $dateEnd->format("Y-m-d H:i:s");
        }

        if (!empty($sources)) {
            $postParms['sources'] = $sources;
        }

        return $this
            ->setRouter(['history', 'lightning', 'locales'])
            ->post($postParms)
            ->exec();
    }
}