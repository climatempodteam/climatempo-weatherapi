<?php

namespace WeatherApi\Retrieve\History;

use WeatherApi\Retrieve\AbstractRetrieve;

/**
 * Class History
 * @package WeatherApi\Retrieve\History
 */
class History extends AbstractRetrieve
{

    /**
     * Undocumented function
     *
     * @param \DateTime $dateBegin
     * @param  $lat
     * @param  $lon
     * @param int $idLocale
     * @param \DateTime $dateEnd
     * @return \stdClass|null
     */
    function getPeriodDaily(\DateTime $dateBegin, $lat = null, $lon = null, $idLocale = null, \DateTime $dateEnd = null)
    {
        $query = [
            'dateBegin' => $dateBegin->format('Y-m-d')
        ];

        if (!is_null($lat)) {
            $query['latitude'] = $lat;
        }

        if (!is_null($lon)) {
            $query['longitude'] = $lon;
        }

        if (!is_null($idLocale)) {
            $query['idLocale'] = $idLocale;
        }

        if (!is_null($dateEnd)) {
            $query['dateEnd'] = $dateEnd->format('Y-m-d');
        }

        $queryString = '?' . http_build_query($query);

        return $this
            ->setRouter(['history', 'period', 'daily'])
            ->addQueryString($queryString)
            ->request();
    }
}

