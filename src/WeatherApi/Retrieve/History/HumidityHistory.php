<?php

namespace WeatherApi\Retrieve\History;

use WeatherApi\Retrieve\AbstractRetrieve;

/**
 * Class HumidityHistory
 * @package WeatherApi\Retrieve\History
 * @author Guilherme Santos <guilhermedossantos91@gmail.com>
 * @author Gustavo Vitorino <gustavo.vittorino@climatempo.com>
 * @version 1.1.0
 */
class HumidityHistory extends AbstractRetrieve
{

    /**
     * @param int $idLocale
     * @param \DateTime $begin
     * @param \DateTime|null $end
     * @param bool|false $forceUpdate
     * @return \stdClass|null
     */
    public function getDaily(
        $idLocale,
        \DateTime $begin,
        \DateTime $end = null,
        $forceUpdate = false
    )
    {
        $queryString = "?idlocale={$idLocale}&dateBegin={$begin->format("Y-m-d")}";

        if (!is_null($end)) {
            $queryString .= "&dateEnd={$end->format("Y-m-d")}";
        }

        return $this
            ->setRouter(['history', 'humidity'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }

    /**
     * @param $idLocale
     * @param \DateTime $begin
     * @param \DateTime|null $end
     * @param bool $forceUpdate
     * @return null|\stdClass
     */
    public function getHourly(
        $idLocale,
        \DateTime $begin,
        \DateTime $end = null,
        $forceUpdate = false
    )
    {
        $queryString = "?idlocale={$idLocale}&dateBegin={$begin->format("Y-m-d")}";

        if (!is_null($end)) {
            $queryString .= "&dateEnd={$end->format("Y-m-d")}";
        }

        return $this
            ->setRouter(['history', 'hourly-humidity'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }

}