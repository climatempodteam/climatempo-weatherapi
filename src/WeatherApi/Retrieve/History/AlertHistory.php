<?php

namespace WeatherApi\Retrieve\History;

use WeatherApi\Retrieve\AbstractRetrieve;

/**
 * Class AlertHistory
 * @package WeatherApi\Retrieve\History
 * @author Guilherme Santos <guilhermedossantos91@gmail.com>
 */
class AlertHistory extends AbstractRetrieve
{

    /**
     * @param int $idLocale
     * @param \DateTime $begin
     * @param \DateTime|null $end
     * @param bool|false $forceUpdate
     * @return \stdClass|null
     */
    public function getDaily(
        $idLocale,
        \DateTime $begin,
        \DateTime $end = null,
        $forceUpdate = false
    )
    {
        $queryString = "?idlocale={$idLocale}&dateBegin={$begin->format("Y-m-d")}";

        if (!is_null($end)) {
            $queryString .= "&dateEnd={$end->format("Y-m-d")}";
        }

        return $this
            ->setRouter(['history', 'alert'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }

    /**
     * @param \DateTime $begin
     * @param \DateTime $end
     * @return \stdClass|null
     */
    public function getByDate(
        \DateTime $begin,
        \DateTime $end
    ) {
        $queryString = "?&dateBegin={$begin->format("Y-m-d")}&dateEnd={$end->format("Y-m-d")}";

        return $this
            ->setRouter(['alert', 'log'])
            ->addQueryString($queryString)
            ->request();
    }
}