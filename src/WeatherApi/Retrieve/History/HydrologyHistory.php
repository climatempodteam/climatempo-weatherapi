<?php

namespace WeatherApi\Retrieve\History;

use WeatherApi\Retrieve\AbstractRetrieve;

class HydrologyHistory extends AbstractRetrieve
{

    /**
     * @param int $idLocale
     * @param \DateTime $begin
     * @param \DateTime|null $end
     * @param bool|false $forceUpdate
     * @return \stdClass|null
     */
    public function getDaily(
        $idlocale,
        \DateTime $begin,
        \DateTime $end = null,
        $forceUpdate = false
    )
    {
        $queries = [
            'idlocale' => $idlocale,
            'dateBegin' => $begin->format("Y-m-d")
        ];

        if(!is_null($end)) {
            $queries['dateEnd'] = $end->format("Y-m-d");
        }

        $queryString = "?".http_build_query($queries);

        return $this
            ->setRouter(['history', 'hydrology'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }
}