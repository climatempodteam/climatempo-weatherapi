<?php

namespace WeatherApi\Retrieve\History;

use WeatherApi\Retrieve\AbstractRetrieve;

/**
 * Class WindHistory
 * @package WeatherApi\Retrieve\History
 * @author Guilherme Santos <guilhermedossantos91@gmail.com>
 * @author Gustavo Vitorino <gustavo.vittorino@climatempo.com>
 * @version 1.1.0
 */
class WindHistory extends AbstractRetrieve
{

    /**
     * @param int $idLocale
     * @param \DateTime|null $begin
     * @param \DateTime|null $end
     * @param bool|false $forceUpdate
     * @return null|\stdClass
     */
    public function getDaily(
        $idLocale,
        \DateTime $begin = null,
        \DateTime $end = null,
        $forceUpdate = false
    )
    {
        $queryString = "?idlocale={$idLocale}&dateBegin={$begin->format("Y-m-d")}";

        if (!is_null($end)) {
            $queryString .= "&dateEnd={$end->format("Y-m-d")}";
        }

        return $this
            ->setRouter(['history', 'wind'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }

    /**
     * @param $idLocale
     * @param \DateTime|null $begin
     * @param \DateTime|null $end
     * @param bool $forceUpdate
     * @return null|\stdClass
     */
    public function getHourly(
        $idLocale,
        \DateTime $begin = null,
        \DateTime $end = null,
        $forceUpdate = false
    )
    {
        $queryString = "?idlocale={$idLocale}&dateBegin={$begin->format("Y-m-d")}";

        if (!is_null($end)) {
            $queryString .= "&dateEnd={$end->format("Y-m-d")}";
        }

        return $this
            ->setRouter(['history', 'hourly-wind'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }
}