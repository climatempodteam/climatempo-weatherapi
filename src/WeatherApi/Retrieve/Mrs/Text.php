<?php

namespace WeatherApi\Retrieve\Mrs;

use WeatherApi\Retrieve\AbstractRetrieve;

/**
 * Class Text
 *
 * @author Gustavo Vitorino <gustavo.vittorino@climatempo.com.br>
 * @version 1.1.0
 */
class Text extends AbstractRetrieve
{

    /**
     * @param $text
     * @param $dateBegin
     * @param $dateEnd
     * @param bool $forceUpdate
     * @return null|stdClass
     */
    public function save($text, $dateBegin, $dateEnd, $forceUpdate = false)
    {
        $queryString = '?' . http_build_query([
                'dateBegin' => $dateBegin->format('Y-m-d'),
                'dateEnd'   => $dateEnd->format('Y-m-d'),
                'text' => $text
            ]);

        return $this
            ->setRouter(['mrs', 'text', 'week', 'save'])
            ->addQueryString($queryString)
            ->request();
    }

    /**
     * @param $dateBegin
     * @param $dateEnd
     * @param bool $forceUpdate
     * @return null|stdClass
     */
    public function select($dateBegin, $dateEnd, $forceUpdate = false)
    {
        $queryString = '?' . http_build_query([
                'dateBegin' => $dateBegin->format('Y-m-d'),
                'dateEnd'   => $dateEnd->format('Y-m-d'),
            ]);

        return $this
            ->setRouter(['mrs', 'text', 'week'])
            ->addQueryString($queryString)
            ->request();
    }
}