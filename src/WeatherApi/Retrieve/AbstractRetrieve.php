<?php

namespace WeatherApi\Retrieve;

use WeatherApi\Cache\JsonCache;
use WeatherApi\Config;
use WeatherApi\Error\WeatherApiException;

/**
 * Class AbstractRetrieve
 *
 * @version 2.2.0
 * @author Guilherme Santos - guilhermedossantos91@gmail.com
 * @author Lucas de Oliveira - lucas.oliveira@climatempo.com.br
 * @author Gustavo Santos Thiago - gustavo.santos@climatempo.com.br
 * @author Luiz Gonçalves - luiz.goncalves@climatempo.com.br
 *
 * @package WeatherApi\Retrieve
 */
abstract class AbstractRetrieve
{

    /**
     * @var JsonCache
     */
    protected $cache;

    /**
     * @var string
     */
    protected $url;

    /**
     * @var Config $config
     */
    protected $config;

    /**
     * @var resource
     */
    protected $context = null;

    /**
     * @var string
     */
    protected $errorMode;

    /**
     * AbstractRetrieve constructor.
     *
     * @param Config $config
     * @param string $errorMode
     */
    public function __construct(Config $config, $errorMode = Config::ERROR_MODE_EXCEPTION_DEFAULT)
    {
        $this->config = $config;
        $this->config->validate();
        $this->url = $this->config->getServer();
        $this->cache = new JsonCache($this->config->getCacheDir());
        $this->errorMode = $errorMode;
    }

    /**
     * Request authentication config
     *
     * @throws \Exception
     */
    protected function authentication()
    {
        $post = array(
            'type'        => "json",
            'application' => $this->config->getApplication()
        );

        if (!is_null($this->config->getClient())) {
            $post['client'] = $this->config->getClient();
        }

        if (!is_null($this->config->getSecret())) {
            $post['secret'] = $this->config->getSecret();
        }

        if (!is_null($this->config->getKey())) {
            $post['key'] = $this->config->getKey();
        }

        if (!is_null($this->config->getProduct())) {
            $post['product'] = $this->config->getProduct();
        }

        if (!is_null($this->config->getAuthToken())) {
            $post['authToken'] = $this->config->getAuthToken();
        }

        if (!$this->config->isCacheApi()) {
            $post['clearcache'] = "true";
        }

        $http = array(
            'method'  => 'POST',
            'header'  => 'Content-type: application/x-www-form-urlencoded',
            'content' => http_build_query($post)
        );

        $this->context = stream_context_create(array('http' => $http));
    }

    /**
     * API request
     *
     * @param bool $jsonDecode
     * @return mixed
     * @throws \Exception
     */
    protected function request($jsonDecode = true)
    {
        $this->authentication();

        $content = file_get_contents($this->url, false, $this->context);

        if ($jsonDecode) {
            if ($content === false) {
                throw new \Exception("API Error: Invalid server");
            }

            $api = @json_decode($content);

            if (!is_object($api)) {
                throw new \Exception("API Error: Invalid return");
            }

            if (!isset($api->response)) {
                throw new \Exception("API Error: Invalid response");
            }

            if (!isset($api->response->success)) {
                throw new \Exception("API Error: Invalid response.success");
            }

            if (!isset($api->data)) {
                throw new \Exception("API Error: Invalid data");
            }

            if (!isset($api->response->message)) {
                throw new \Exception("API Error: Invalid response.message");
            }

            return $api;
        }

        return $content;
    }

    /**
     * Adiciona rota
     *
     * $routers = array(
     *     0 => 'alert',
     *     1 => 'lightning',
     *     2 => 'cep'
     * );
     * http://api.climatempo.com.br/api/v1/alert/lightning/cep
     *
     * @param array $router
     * @return $this
     */
    protected function setRouter(array $router)
    {
        $this->url = sprintf('%s/%s', $this->config->getServer(), implode('/', $router));

        return $this;
    }

    /**
     * Atribui querystring na rota
     *
     * $queryString = array(
     *     'id' => 1,
     *     'country' => 'brazil'
     * );
     *
     * http://api.climatempo.com.br/api/v1?id=1&country=brazil
     *
     * @param array $queryStrings
     * @return $this
     */
    protected function setQueryString(array $queryStrings)
    {
        $this->url .= http_build_query($queryStrings);
        return $this;
    }

    /**
     * Transforma uma querystring em um array
     *
     * @param string $queryString
     * @param array $params
     */
    protected function queryStringToArray($queryString = null, array &$params)
    {
        $queryString = str_replace("?", "", $queryString);
        parse_str($queryString, $params);
    }

    /**
     * Add querystring na rota
     *
     * @param string $queryString
     * @return $this
     */
    protected function addQueryString($queryString)
    {
        $this->url .= $queryString;
        return $this;
    }

    /**
     * @return string
     */
    protected function getURL()
    {
        return $this->url;
    }

    /**
     * Formata nome do arquivo de cache
     *
     * @param string $name
     * @param string $queryString
     * @return string
     */
    protected function formatCacheName($name, $queryString = null)
    {
        if ($this->config->isCacheEncode()) {
            $cacheName = hash("adler32", $name);
        } else {
            $cacheName = $name;
        }

        $params = array();

        if (is_string($queryString)) {
            $this->queryStringToArray($queryString, $params);
        }

        $params["client"] = $this->config->getClient();

        foreach ($params as $key => $value) {

            if (is_array($value)) {
                $value = implode(".", $value);
            }

            $cacheName .= "_{$key}{$value}";
        }

        $cacheName = str_replace(["/", "\\"], ".", $cacheName);
        $cacheName = str_replace([":", "::"], "-", $cacheName);
        $cacheName = preg_replace('/[^A-Za-z0-9-_.]/u', '', $cacheName);
        $cacheName = trim($cacheName);
        $cacheName = strtolower($cacheName);

        return $cacheName;
    }

   /**
     * Gerencia retorno do cache
     *
     * @param string $cacheName
     * @param bool|false $forceUpdate
     * @return null|\stdClass
     */
    protected function manageCache($cacheName, $forceUpdate = false)
    {
        $api = null;

        try {
            $api = $this->cache->fetch($cacheName);

            if ($forceUpdate || ($this->cache->isExpired($cacheName, $this->config->getCacheTime())) || !$api) {
                $api = $this->request();

                if ($api->response->success) {
                    $this->cache->save($cacheName, $api);
                }
            }
        } catch (\Exception $e) {
            $response = new \stdClass();
            $response->success = false;
            $response->message = $e->getMessage();
            $response->time = 0;
            $api = new \stdClass();
            $api->response = $response;
            $api->data = [];
        }

        return $api;
    }

    /**
     * seta metodo e recebe os parametros
     *
     * @param array $params
     * @return $this
     */
    protected function post($params = null)
    {

        $this->method = 'POST';
        $this->arguments = $params;
        return $this;
    }

    /**
     *  API exec - metodo em curl para conexão com a API
     *
     * @param bool $jsonDecode
     * @return mixed
     * @throws \Exception
     */
    protected function exec($jsonDecode = true)
    {

        if (! function_exists('curl_version')) {
            throw new \Exception("API Error: CURL Not Install");
        }

        $headers = array("Content-type: application/x-www-form-urlencoded");

        $this->arguments = array_merge($this->arguments, array(
            'type'        => "json",
            'application' => $this->config->getApplication()
        ));

        if (!is_null($this->config->getClient())) {
            $this->arguments['client'] = $this->config->getClient();
        }

        if (!is_null($this->config->getSecret())) {
            $this->arguments['secret'] = $this->config->getSecret();
        }

        if (!is_null($this->config->getKey())) {
            $this->arguments['key'] = $this->config->getKey();
        }

        if (!is_null($this->config->getProduct())) {
            $this->arguments['product'] = $this->config->getProduct();
        }

        if (!is_null($this->config->getAuthToken())) {
            $this->arguments['authToken'] = $this->config->getAuthToken();
        }

        if (!$this->config->isCacheApi()) {
            $this->arguments['clearcache'] = "true";
        }

        $this->arguments=  http_build_query($this->arguments, "", "&");

        $content = null;
        $connect = curl_init();
        curl_setopt($connect, CURLOPT_USERAGENT, "Climatempo API PHP SDK v" . Config::VERSION);
        curl_setopt($connect, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($connect, CURLOPT_SSL_VERIFYPEER, true);
        curl_setopt($connect, CURLOPT_CUSTOMREQUEST, $this->method);
        curl_setopt($connect, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($connect, CURLOPT_URL, $this->url);
        curl_setopt($connect, CURLOPT_POSTFIELDS, $this->arguments);

        $api_result = curl_exec($connect);
        $api_http_code = curl_getinfo($connect, CURLINFO_HTTP_CODE);

        if ($api_result === FALSE) {
            throw new \Exception(curl_error($connect));
        }

        curl_close($connect);

        if ($jsonDecode) {
            if ($content === false) {
                throw new \Exception("API Error: Invalid server :: {$api_http_code}");
            }
            $api = @json_decode($api_result);

            if (!is_object($api)) {
                throw new \Exception("API Error: Invalid return :: {$api_http_code}");
            }

            if (!isset($api->response)) {
                throw new \Exception("API Error: Invalid response :: {$api_http_code}");
            }

            if (!isset($api->response->success)) {
                throw new \Exception("API Error: Invalid response.success :: {$api_http_code}");
            }

            if (!isset($api->data)) {
                throw new \Exception("API Error: Invalid data :: {$api_http_code}");
            }

            if (!isset($api->response->message)) {
                throw new \Exception("API Error: Invalid response.message :: {$api_http_code}");
            }

            if ($api->response->success == false && $this->errorMode == Config::ERROR_MODE_EXCEPTION_WEATHER_API) {
                $apiData = (array) $api->data;
                throw new WeatherApiException($api->response->message, $apiData);
            }

            if ($api->response->success == false) {
                throw new \Exception($api->response->message);
            }

            return $api;
        }
        return $content;
    }

}
