<?php

namespace WeatherApi\Retrieve\Observed;

use WeatherApi\Param\AgroclimaModuleParam;
use WeatherApi\Retrieve\AbstractRetrieve;

/**
 * Class Station
 *
 *
 * @package WeatherApi\Retrieve\Observed
 * @author  Luiz Goncalves <luiz.goncalves@climatempo.com.br>
 * @author  Lucas Porto de Deus <lucas.deus@climatempo.com.br>
 * @version 1.5.0
 */
class Station extends AbstractRetrieve
{
    /**
     * Busca estacoes do usuario
     * @param int $userId
     * @param bool|false $forceUpdate
     * @return null|\stdClass
     */
    public function getStation($userId, $forceUpdate = false)
    {
        $queryString = '?' . http_build_query([
                'userId' => $userId
            ]);

        return $this
            ->setRouter(['observed','station', 'byuser'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }

    /**
     * Busca dados horários das estações
     * @param  integer $stationId
     * @param  string  $dateBegin
     * @param  string  $dateEnd
     * @param  boolean $forceUpdate Default false
     * @return mixed   NULL | \stdClass
     */
    public function getDataStationHourly($stationId, $dateBegin, $dateEnd, $forceUpdate = false)
    {
        $queryString = '?' . http_build_query([
            'dateBegin' => $dateBegin,
            'dateEnd'   => $dateEnd,
            'stationId' => $stationId
        ]);

        return $this
            ->setRouter(['observed','history'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }

    /**
     * Busca dados horários das estações
     * @param  integer $stationId
     * @param  string  $dateBegin
     * @param  string  $dateEnd
     * @param  boolean $forceUpdate Default false
     * @return mixed   NULL | \stdClass
     */
    public function getDataStationHourlyWithoutCache($stationId, $dateBegin, $dateEnd, $forceUpdate = false)
    {
        $queryString = '?' . http_build_query([
            'dateBegin' => $dateBegin,
            'dateEnd'   => $dateEnd,
            'stationId' => $stationId
        ]);

        return $this
            ->setRouter(['observed','history', 'nocache'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }

    /**
     * Busca dados diários das estações
     * @param  integer $stationId
     * @param  string  $dateBegin
     * @param  string  $dateEnd
     * @param  boolean $forceUpdate Default false
     * @return mixed   NULL | \stdClass
     */
    public function getDataStationDaily($stationId, $dateBegin, $dateEnd, $forceUpdate = false)
    {
        $queryString = '?' . http_build_query([
            'dateBegin' => $dateBegin,
            'dateEnd'   => $dateEnd,
            'stationId' => $stationId
        ]);

        return $this
            ->setRouter(['observed','history', 'daily'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }

    /**
     * Busca dados da estacao do usuario
     * @param int $stationId
     * @return null|\stdClass
     */
    public function getRealTimeStation($stationId)
    {
        $queryString = '?' . http_build_query([
                'stationId' => $stationId
            ]);

        return $this
            ->setRouter(['observed','latest'])
            ->addQueryString($queryString)
            ->request();
    }

    /**
     * Busca dados da estacao mais proxima do ponto do usuario
     * @param string $latitude
     * @param string $longitude
     * @param array $sources
     * @param bool $isLimitedByCity
     * @return null|\stdClass
     * @throws \Exception
     */
    public function getRealTimeNearestStation($latitude, $longitude, $sources = [], $isLimitedByCity = false)
    {
        $queryString = '?' . http_build_query([
                'latitude' => $latitude,
                'longitude' => $longitude,
                'sources' => $sources,
                AgroclimaModuleParam::IS_LIMITED_BY_CITY_OPTION => $isLimitedByCity
            ]);

        return $this
            ->setRouter(['observed', 'latest','nearest-station'])
            ->addQueryString($queryString)
            ->request();
    }


    /**
     * Busca dados de uma variavel da estações
     * @param $stationId
     * @param $operation
     * @param $variable
     * @param $values
     * @param bool $forceUpdate
     * @param bool $isHour
     * @return \stdClass|null
     */
    public function getVariableDataStation($stationId, $operation, $variable, $values, $forceUpdate = false, $isHour = false)
    {
        $queryString = '?' . http_build_query([
                'operation' => $operation,
                'variable'   => $variable,
                'stationId' => $stationId,
                'values' => $values,
                'isHour' => $isHour
            ]);

        return $this
            ->setRouter(['observed','latest','by-variable-condition'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }

    /**
     * Retorna informações da estação buscando por coordenada
     * @param $latitude
     * @param $longitude
     * @param bool $forceUpdate
     * @return \stdClass|null
     */
    public function getInfoSatationByCoordinates($latitude, $longitude, $forceUpdate = false)
    {
        $queryString = '?' . http_build_query([
                'latitude' => $latitude,
                'longitude' => $longitude
            ]);

        return $this
            ->setRouter(['observed','station','id'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }

    /**
     * Retorna dados de uma variavel de uma estação no formato horario
     * @param $stationId
     * @param $variable
     * @param $dateBegin
     * @param $dateEnd
     * @param bool $forceUpdate
     * @return mixed
     * @throws \Exception
     */
    public function getDataHourlyStationVarible($stationId,$variable, $dateBegin, $dateEnd, $forceUpdate = false)
    {
        $queryString = '?' . http_build_query([
                'dateBegin' => $dateBegin,
                'dateEnd'   => $dateEnd,
                'stationId' => $stationId,
                'variable' => $variable
            ]);

        return $this
            ->setRouter(['observed','history','hourly', 'variable'])
            ->addQueryString($queryString)
            ->request(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }

    /**
     * Retorna dados de uma variavel de uma estação no formato diario
     * @param $stationId
     * @param $variable
     * @param $dateBegin
     * @param $dateEnd
     * @param bool $forceUpdate
     * @return mixed
     * @throws \Exception
     */
    public function getDataDailyStationVarible($stationId, $variable, $dateBegin, $dateEnd, $forceUpdate = false)
    {
        $queryString = '?' . http_build_query([
                'dateBegin' => $dateBegin,
                'dateEnd'   => $dateEnd,
                'stationId' => $stationId,
                'variable' => $variable
            ]);

        return $this
            ->setRouter(['observed','history','daily', 'variable'])
            ->addQueryString($queryString)
            ->request(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }
}
