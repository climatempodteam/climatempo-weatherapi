<?php

namespace WeatherApi\Retrieve\Observed;

use WeatherApi\Retrieve\AbstractRetrieve;

/**
 * Class ObservedHourly
 *
 * @package WeatherApi\Retrieve\Observed
 *
 * @author  Danilo Luz <danilo.luz@climatempo.com.br>
 * @version 1.0.0
 */
class ObservedHourly extends AbstractRetrieve
{

    /**
     * Busca dados historico observado horário
     *
     * @param  integer $stationId
     * @param  string  $dateBegin
     * @param  string  $dateEnd
     * @param  boolean $forceUpdate Default false
     * @return mixed   NULL | \stdClass
     */
    public function getHistoricObservedHourly($stationId, $dateBegin, $dateEnd, $forceUpdate = false)
    {
        $queryString = '?' . http_build_query([
            'dateBegin' => $dateBegin,
            'dateEnd'   => $dateEnd,
            'stationId' => $stationId
        ]);

        return $this
            ->setRouter(['observed', 'history', 'hourly'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }
}
