<?php

namespace WeatherApi\Retrieve\Observed;

use WeatherApi\Retrieve\AbstractRetrieve;
use DateTime;

/**
 * Class Metar
 * @package WeatherApi\Retrieve\Observed
 * @author  Gustavo Santos Thiago <gustavo.santos@climatempo.com.br>
 * @version 1.0.0
 */
class Metar extends AbstractRetrieve
{
    /**
     * @param array $localesIds
     * @param DateTime $dateBegin
     * @return DateTime $dateEnd
     */
    public function getHistoryByLocalesAndPeriod(
        $localesIds,
        DateTime $dateBegin,
        DateTime $dateEnd
    )
    {
        $queryString = '?' . http_build_query([
            'localesIds' => implode(',', $localesIds),
            'dateBegin' => $dateBegin->format('Y-m-d H:i:s'),
            'dateEnd' => $dateEnd->format('Y-m-d H:i:s')
        ]);

        return $this
            ->setRouter(['observed','history', 'metar'])
            ->addQueryString($queryString)
            ->request();
    }
}

