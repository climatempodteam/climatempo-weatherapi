<?php

namespace WeatherApi\Retrieve\Observed;

use WeatherApi\Retrieve\AbstractRetrieve;
use DateTime;

/**
 * Class Fire
 *
 * @package WeatherApi\Retrieve\Observed
 */
class Fire extends AbstractRetrieve
{

	/**
	 * @param DateTime $dateBegin
	 * @param DateTime $dateEnd
	 * @param integer $idLocale
	 * @param bool $forceUpdate
	 * @return null|\stdClass
	 */
	public function getHistoricObservedFire(
		DateTime $dateBegin,
		DateTime $dateEnd,
		$idLocale,
		$forceUpdate = false
	) {
		$queryString = '?' . http_build_query([
			'dateBegin' => $dateBegin->format('Y-m-d'),
			'dateEnd'   => $dateEnd->format('Y-m-d'),
			'idLocale' => $idLocale
		]);

		return $this
			->setRouter(['observed', 'fire'])
			->addQueryString($queryString)
			->manageCache(
				$this->formatCacheName(
					__METHOD__,
					$queryString
				),
				$forceUpdate
			);
	}
}
