<?php

namespace WeatherApi\Retrieve\Product;

use WeatherApi\Retrieve\AbstractRetrieve;

/**
 * Class File
 * Classe para obter arquivos de clientes
 *
 * @version 1.0.0
 * @author Guilherme Santos - guilhermedossantos91@gmail.com
 * @package WeatherApi\Retrieve\Product
 */
class File extends AbstractRetrieve
{
    /**
     * Busca todos os arquivos por periodo
     *
     * @author Guilherme Santos - guilhermedossantos91@gmail.com
     * @param \DateTime $begin
     * @param \DateTime|null $end
     * @param bool $forceUpdate
     * @return null|\stdClass
     */
    public function getByPeriod(
        \DateTime $begin,
        \DateTime $end = null,
        $forceUpdate = false
    )
    {
        $queryString = "?dateBegin={$begin->format("Y-m-d")}";

        if (!is_null($end)) {
            $queryString .= "&dateEnd={$end->format("Y-m-d")}";
        }

        return $this
            ->setRouter(['files'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }


    /**
     * Retorna conteudo do arquivo do cliente (PDF, TXT ou EXCEL)
     *
     * @author Guilherme Santos - guilhermedossantos91@gmail.com
     * @param string $fileName
     * @return mixed
     */
    public function download($fileName)
    {
        $queryString = "?file={$fileName}";
        return $this
            ->setRouter(['files', 'download'])
            ->addQueryString($queryString)
            ->request(false);
    }
}