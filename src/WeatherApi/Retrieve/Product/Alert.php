<?php

namespace WeatherApi\Retrieve\Product;

use WeatherApi\Retrieve\AbstractRetrieve;

/**
 * Class Alert
 *
 * Essa classe é responsável por obter dados dos alertas
 *
 * @package WeatherApi\Retrieve\Product
 * @author  Gustavo Vitorino -  <gustavo.vittorino@climatempo.com.br>
 * @version 1.0.0
 */
class Alert extends AbstractRetrieve
{

    /**
     * Obtem os contatos cadastrados para alerta para uma localidade do cliente
     *
     * @param int $idLocale
     * @param bool $forceUpdate parâmetro para definir se deve atualizar sempre
     * @return null|\stdClass retorno da API
     */
    public function getContacts($idLocale = null, $forceUpdate = true)
    {
        $query = [];
        
        if (!is_null($idLocale)) {
            $query["idLocale"] = $idLocale;
        }

        $queryString = '?' . http_build_query($query);

        return $this
            ->setRouter(['alert','contacts'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }
}