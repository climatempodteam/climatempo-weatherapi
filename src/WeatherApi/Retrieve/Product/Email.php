<?php

namespace WeatherApi\Retrieve\Product;

use WeatherApi\Retrieve\AbstractRetrieve;

/**
 * Class Email
 *
 * Essa classe é responsável por obter dados de grupos e e-mails
 * do sistema clientes.
 *
 * @package WeatherApi\Retrieve\Product
 * @author  Guilherme Santos <guilhermedossantos91@gmail.com>
 */
class Email extends AbstractRetrieve
{

    /**
     * Obtem todos os e-mails pelo id de um grupo
     *
     * @author Guilherme Santos <guilhermedossantos91@gmail.com>
     * @param int $groupId      id do grupo
     * @param bool $forceUpdate parâmetro para definir se deve atualizar sempre
     * @return null|\stdClass retorno da API
     */
    public function getByGroup($groupId, $forceUpdate = true)
    {
        $queryString = "?groupid={$groupId}";

        return $this
            ->setRouter(['emails'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }
    
    /**
     * Retorna os e-mails cadastrados no subproduto do sistema clientes de acordo com o
     * produto configurado na autenticação.
     *
     * @param int $idSubProducts Id do subproduto do tipo boletim
     * @param bool $forceUpdate parâmetro para definir se deve atualizar sempre
     * @author Michel Araujo
     * @return null|\stdClass
     */
    public function getEmailsSubProducts($idSubProducts, $forceUpdate = true)
    {
        $queryString = "?id={$idSubProducts}";

        return $this
            ->setRouter(['emails', 'module'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }
}