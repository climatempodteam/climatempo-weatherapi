<?php

namespace WeatherApi\Retrieve\Product;

use WeatherApi\Retrieve\AbstractRetrieve;

/**
 * LogisticBulletin
 * 
 * @version 1.0.0
 */
class LogisticBulletin extends AbstractRetrieve
{
    /**
     * @return StdClass
     */
    public function getConfigByClient() {
        return $this
            ->setRouter(['logistic-bulletin', 'show', 'config'])
            ->request();
    }
}
