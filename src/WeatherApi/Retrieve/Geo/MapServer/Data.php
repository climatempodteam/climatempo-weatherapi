<?php

namespace WeatherApi\Retrieve\Geo\MapServer;

use WeatherApi\Retrieve\AbstractRetrieve;
use DateTime;

/**
 * Class Data Responsável por pegar os dados de previsão georreferenciados
 * @package WeatherApi\Retrieve\Geo\MapServer
 *
 * @author Gustavo Santos <gustavo.santos@climatempo.com.br>
 *
 * @version 1.0.0
 */
class Data extends AbstractRetrieve
{
    /**
     * Pega previsão horária georreferenciada
     *
     * @param String   $latitude
     * @param String   $longitude
     * @param DateTime $dateBegin
     * @param DateTime $dateEnd
     * @param array $variables
     * @param int $rounding
     *
     * @return \stdClass
     */
    public function getDataHourly(
        $latitude, 
        $longitude, 
        DateTime $dateBegin, 
        DateTime $dateEnd,
        array $variables,
        $rounding
    )
    {
        $queryString = '?'.http_build_query([
            'latitude' => $latitude,
            'longitude' => $longitude,
            'dateBegin' => $dateBegin->format('Y-m-d'),
            'dateEnd' => $dateEnd->format('Y-m-d'),
            'variables' => implode(',', $variables),
            'rounding' => $rounding
        ]);
        
        return $this
            ->setRouter(['geo', 'data', 'hourly'])
            ->addQueryString($queryString)
            ->request();
    }
}
