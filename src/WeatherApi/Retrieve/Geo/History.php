<?php

namespace WeatherApi\Retrieve\Geo;

use WeatherApi\Retrieve\AbstractRetrieve;

/**
 * Class History
 * @package WeatherApi\Retrieve\Geo
 * @author Luiz Goncalves <luiz.goncalves@climatempo.com.br>
 * @version 1.0.0
 */
class History extends AbstractRetrieve
{

    /**
     * @param array $coordinates
     * @param \DateTime $begin
     * @param \DateTime|null $end
     * @param bool|false $forceUpdate
     * @return null|\stdClass
     */
    public function getHourlyHistoryStation(
        $coordinates,
        \DateTime $begin,
        \DateTime $end = null,
        $forceUpdate = false
    )
    {

        $queryString = "?stationId={$coordinates['id']}";
        $queryString .= "&dateBegin={$begin->format("Y-m-d")}";

        if (!is_null($end)) {
            $queryString .= "&dateEnd={$end->format("Y-m-d")}";
        }

        return $this
            ->setRouter(['observed','history'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }

    /**
     * @param array $coordinates
     * @param \DateTime $begin
     * @param \DateTime|null $end
     * @param bool|false $forceUpdate
     * @return null|\stdClass
     */
    public function getDailyHistoryPoint(
        $coordinates,
        \DateTime $begin,
        \DateTime $end = null,
        $forceUpdate = false
    )
    {
        $queryString = "?longitude={$coordinates['longitude']}&latitude={$coordinates['latitude']}";
        $queryString .= "&dateBegin={$begin->format("Y-m-d")}";

        if (!is_null($end)) {
            $queryString .= "&dateEnd={$end->format("Y-m-d")}";
        }

        return $this
            ->setRouter(['history','period','daily'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }
}