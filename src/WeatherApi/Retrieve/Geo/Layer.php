<?php

namespace WeatherApi\Retrieve\Geo;

use WeatherApi\Retrieve\AbstractRetrieve;

/**
 * Class Layer
 *
 * @package WeatherApi\Retrieve\Geo
 * @author  Guilherme Santos <guilhermedossantos91@gmail.com>
 */
class Layer extends AbstractRetrieve
{
    /**
     * @param bool|false $forceUpdate
     * @return mixed
     */
    public function getAll($forceUpdate = false)
    {
        return $this
            ->setRouter(['geo', 'map'])
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__
                ),
                $forceUpdate
            );
    }
}