<?php

namespace WeatherApi\Retrieve\Geo;

use WeatherApi\Retrieve\AbstractRetrieve;

/**
 * Class Climate Responsável por pegar os dados da previsão climática
 * @package WeatherApi\Retrieve\Geo
 *
 * @author Gustavo Santos <gustavo.santos@climatempo.com.br>
 * @version 1.0.0
 */
class Climate extends AbstractRetrieve
{
    /**
     * Pegar dados climáticos de chuva
     *
     * @param $latitude latitude do local
     * @param $longitude longitude do local
     * @param \DateTime $dateBegin Data de inicio da busca
     * @param \DateTime $dateEnd Data de fim da busca
     * @param bool $forceUpdate verifica se é pra forçar uma atualização ou não
     *
     * @author Gustavo Santos <gustavo.santos@climatempo.com.br>
     * @return null|\stdClass
     */
    public function getClimateRain($latitude, $longitude, \DateTime $dateBegin, \DateTime $dateEnd, $forceUpdate = false)
    {
        $queryString = "?".http_build_query([
                "latitude" => $latitude,
                "longitude" => $longitude,
                "dateBegin" => $dateBegin->format("Y-m"),
                "dateEnd" => $dateEnd->format("Y-m")
            ]);

        return $this
            ->setRouter(["climate", "rain"])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }

    /**
     * Pegar dados climáticos de Temperatura
     *
     * @param $latitude latitude do local
     * @param $longitude longitude do local
     * @param \DateTime $dateBegin Data de inicio da busca
     * @param \DateTime $dateEnd Data de fim da busca
     * @param bool $forceUpdate verifica se é pra forçar uma atualização ou não
     *
     * @author Gustavo Santos <gustavo.santos@climatempo.com.br>
     * @return null|\stdClass
     */
    public function getClimateTemperature($latitude, $longitude, \DateTime $dateBegin, \DateTime $dateEnd, $forceUpdate = false)
    {
        $queryString = "?".http_build_query([
                "latitude" => $latitude,
                "longitude" => $longitude,
                "dateBegin" => $dateBegin->format("Y-m"),
                "dateEnd" => $dateEnd->format("Y-m")
            ]);

        return $this
            ->setRouter(["climate", "temperature"])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }

}