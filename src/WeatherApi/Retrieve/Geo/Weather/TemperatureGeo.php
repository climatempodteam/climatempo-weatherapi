<?php

namespace WeatherApi\Retrieve\Geo\Weather;

use WeatherApi\Retrieve\AbstractRetrieve;

/**
 * Class TemperatureGeo
 *
 * Responsável por pegar os dados de previsão georreferenciados de temperatura
 *
 * @package WeatherApi\Retrieve\Geo\Weather
 *
 * @author Danilo Luz <danilo.luz@climatempo.com.br>
 * @version 1.0.0
 */
class TemperatureGeo extends AbstractRetrieve
{
    /**
     * Pega previsão diaria georreferenciada de temperatura
     *
     * @param String $latitude latitude do local
     * @param String $longitude longitude do local
     * @param \DateTime $dateBegin Data de inicio da busca
     * @param \DateTime $dateEnd Data de fim da busca
     * @param null $font
     * @param bool $forceUpdate verifica se é pra forçar uma atualização ou não
     * @return null|\stdClass
     * @author Danilo Luz <danilo.luz@climatempo.com.br>
     */
    public function getDaily(
        $latitude,
        $longitude,
        \DateTime $dateBegin,
        \DateTime $dateEnd,
        $font = null,
        $forceUpdate = false
    )
    {

        $params = [
            "latitude" => $latitude,
            "longitude" => $longitude,
            "dateBegin" => $dateBegin->format("Y-m-d"),
            "dateEnd" => $dateEnd->format("Y-m-d")
        ];

        if ($font) {
            $params["font"] = $font;
        }

        $queryString = "?". http_build_query($params);

        return $this
            ->setRouter(['geo', 'temperature', 'daily'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }
}