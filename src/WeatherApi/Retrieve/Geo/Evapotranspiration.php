<?php

namespace WeatherApi\Retrieve\Geo;

use WeatherApi\Retrieve\AbstractRetrieve;

/**
 * Class Evapotranspiration - Responsável por pegar os dados para a evapotranspiração
 * @package WeatherApi\Retrieve\Geo
 *
 * @author Giovanni Cruz <giovanni.cruz97@hotmail.com>
 * @version 1.0.0
 */
class Evapotranspiration extends AbstractRetrieve
{
    /**
     * Pegar a evapotranspiração potencial diária
     *
     * @param $latitude latitude do local
     * @param $longitude longitude do local
     * @param \DateTime $dateBegin Data de inicio da busca
     * @param \DateTime $dateEnd Data de fim da busca
     * @param bool $forceUpdate verifica se é pra forçar uma atualização ou não
     *
     * @return null|\stdClass
     */
    public function getDailyPotentialEvapotranspiration(
        $latitude,
        $longitude,
        \DateTime $dateBegin = null,
        \DateTime $dateEnd = null,
        $forceUpdate = false
    ) {
        $query = [
            "latitude" => $latitude,
            "longitude" => $longitude
        ];

        $query['dateBegin'] = ($dateBegin) ? $dateBegin->format("Y-m-d") : null;
        $query['dateEnd'] = ($dateEnd) ? $dateEnd->format("Y-m-d") : null;
        $queryString = "?".http_build_query($query);

        return $this
            ->setRouter(['geo', 'evapotranspiration', 'daily'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }
}
