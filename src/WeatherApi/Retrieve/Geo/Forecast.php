<?php

namespace WeatherApi\Retrieve\Geo;

use WeatherApi\Retrieve\AbstractRetrieve;

/**
 * Class Forecast Responsável por pegar os dados de previsão georreferenciados
 * @package WeatherApi\Retrieve\Geo
 *
 * @author Gustavo Santos <gustavo.santos@climatempo.com.br>
 * @author Lucas de Oliveira <lucas.oliveira@climatempo.com.br>
 * @version 1.1.0
 */
class Forecast extends AbstractRetrieve
{
    /**
     * Pega previsão diaria georreferenciada
     *
     * @param String $latitude latitude do local
     * @param String $longitude longitude do local
     * @param \DateTime $dateBegin Data de inicio da busca
     * @param \DateTime $dateEnd Data de fim da busca
     * @param bool $forceUpdate verifica se é pra forçar uma atualização ou não
     *
     * @author Gustavo Santos <gustavo.santos@climatempo.com.br>
     * @return null|\stdClass
     */
    public function getDaily($latitude, $longitude, \DateTime $dateBegin, \DateTime $dateEnd, $forceUpdate = false)
    {
        $queryString = "?".http_build_query([
            "latitude" => $latitude,
            "longitude" => $longitude,
            "dateBegin" => $dateBegin->format("Y-m-d"),
            "dateEnd" => $dateEnd->format("Y-m-d")
        ]);

        return $this
            ->setRouter(['geo', 'forecast', 'daily'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }

    /**
     * Pega previsão horária georreferenciada
     *
     * @param String $latitude latitude do local
     * @param String $longitude longitude do local
     * @param \DateTime $dateBegin Data de inicio da busca
     * @param \DateTime $dateEnd Data de fim da busca
     * @param string $timezone fuso horario
     * @param bool $forceUpdate verifica se é pra forçar uma atualização ou não
     *
     * @author Gustavo Santos <gustavo.santos@climatempo.com.br>
     * @return null|\stdClass
     */
    public function getHourly(
        $latitude, 
        $longitude, 
        \DateTime $dateBegin, 
        \DateTime $dateEnd, 
        $timezone = null,
        $forceUpdate = false
    ) {
        $queryString = "?".http_build_query([
            "latitude" => $latitude,
            "longitude" => $longitude,
            "dateBegin" => $dateBegin->format("Y-m-d"),
            "dateEnd" => $dateEnd->format("Y-m-d"),
            "timezone" => $timezone
        ]);

        return $this
            ->setRouter(['geo', 'forecast', 'hour'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }

    /**
     * Pega previsão agrícola diária georreferenciada
     *
     * @param String $latitude latitude do local
     * @param String $longitude longitude do local
     * @param \DateTime $dateBegin Data de inicio da busca
     * @param \DateTime $dateEnd Data de fim da busca
     * @param bool $forceUpdate verifica se é pra forçar uma atualização ou não
     *
     * @author Gustavo Santos <gustavo.santos@climatempo.com.br>
     * @return null|\stdClass
     */
    public function getDailyAgriculture($latitude, $longitude, \DateTime $dateBegin, \DateTime $dateEnd, $forceUpdate = false)
    {
        $queryString = "?".http_build_query([
            "latitude" => $latitude,
            "longitude" => $longitude,
            "dateBegin" => $dateBegin->format("Y-m-d"),
            "dateEnd" => $dateEnd->format("Y-m-d")
        ]);

        return $this
            ->setRouter(['geo', 'forecast', 'daily-agriculture'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }


}