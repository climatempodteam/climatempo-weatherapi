<?php

namespace WeatherApi\Retrieve\Geo;

use WeatherApi\Retrieve\AbstractRetrieve;

/**
 * Class HydricBalancer Responsável por pegar os dados para o balanço hídrico
 * @package WeatherApi\Retrieve\Geo
 *
 * @author Gustavo Santos <gustavo.santos@climatempo.com.br>
 * @version 1.2.0
 */
class HydricBalancer extends AbstractRetrieve
{
    /**
     * Pegar excesso de água
     *
     * @param $latitude latitude do local
     * @param $longitude longitude do local
     * @param \DateTime $dateBegin Data de inicio da busca
     * @param \DateTime $dateEnd Data de fim da busca
     * @param bool $forceUpdate verifica se é pra forçar uma atualização ou não
     *
     * @author Gustavo Santos <gustavo.santos@climatempo.com.br>
     * @return null|\stdClass
     */
    public function getExcessiveBalance($latitude, $longitude, \DateTime $dateBegin, \DateTime $dateEnd, $forceUpdate = false)
    {
        $queryString = "?".http_build_query(array("latitude" => $latitude,
                "longitude" => $longitude,
                "dateBegin" => $dateBegin->format("Y-m-d"),
                "dateEnd" => $dateEnd->format("Y-m-d")));

        return $this
            ->setRouter(['geo', 'hb', 'exc'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }

    /**
     * Pegar deficiencia de água
     *
     * @param $latitude latitude do local
     * @param $longitude longitude do local
     * @param \DateTime $dateBegin Data de inicio da busca
     * @param \DateTime $dateEnd Data de fim da busca
     * @param bool $forceUpdate verifica se é pra forçar uma atualização ou não
     *
     * @author Gustavo Santos <gustavo.santos@climatempo.com.br>
     * @return null|\stdClass
     */
    public function getDefitBalance($latitude, $longitude,\DateTime $dateBegin, \DateTime $dateEnd, $forceUpdate = false)
    {
        $queryString = "?".http_build_query(array("latitude" => $latitude,
                "longitude" => $longitude,
                "dateBegin" => $dateBegin->format("Y-m-d"),
                "dateEnd" => $dateEnd->format("Y-m-d")));

        return $this
            ->setRouter(['geo', 'hb', 'def'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }

    /**
     * Retorna os dados de balanço hidrico mesclados
     *
     * @param $latitude
     * @param $longitude
     * @param \DateTime $dateBegin
     * @param \DateTime $dateEnd
     * @param bool $forceUpdate
     * @return null|\stdClass
     */
    public function getHydricForecast($latitude, $longitude, \DateTime $dateBegin, \DateTime $dateEnd, $forceUpdate = false)
    {
        $queryString = "?".http_build_query([
                "latitude" => $latitude,
                "longitude" => $longitude,
                "dateBegin" => $dateBegin->format("Y-m-d"),
                "dateEnd" => $dateEnd->format("Y-m-d")
            ]);

        return $this
            ->setRouter(['geo', 'hb', 'forecast'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );


    }

    /**
     * Retorna os dados de extrato do balanço hídrico
     *
     * @param $latitude
     * @param $longitude
     * @param \DateTime $dateBegin
     * @param \DateTime $dateEnd
     * @param bool $forceUpdate
     * @return null|\stdClass
     */
    public function getExtractBalance($latitude, $longitude, \DateTime $dateBegin, \DateTime $dateEnd, $forceUpdate = false)
    {
        $queryString = "?".http_build_query([
                "latitude" => $latitude,
                "longitude" => $longitude,
                "dateBegin" => $dateBegin->format("Y-m-d"),
                "dateEnd" => $dateEnd->format("Y-m-d")
            ]);

        return $this
            ->setRouter(['geo', 'hb', 'extract'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );


    }

}