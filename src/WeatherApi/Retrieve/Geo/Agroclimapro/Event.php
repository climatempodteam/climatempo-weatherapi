<?php

namespace WeatherApi\Retrieve\Geo\Agroclimapro;

use WeatherApi\Retrieve\AbstractRetrieve;

/**
 * Class Event
 *
 * @package WeatherApi\Retrieve\Agroclimapro\Event
 * @author Luiz Goncalves <luiz.goncalves@climatempo.com.br>
 * @author Gustavo Santos Thiago <gustavo.santos@climatempo.com.br>
 * @version 1.2.0
 */
class Event extends AbstractRetrieve
{

    /**
     * @param bool $forceUpdate verifica se é pra forçar uma atualização ou não
     * @return object
     */
    public function getAllTypeEvent($forceUpdate = false)
    {

        return $this
            ->setRouter(['geo', 'agroclimapro','event','list-date-option'])
            ->manageCache(
                $this->formatCacheName(__METHOD__),
                $forceUpdate
            );
    }

    /**
     * @param  int    $cycleId
     * @param  int    $typeEventId
     * @param  array  $thresholds
     * @param  array  $fields
     * @param  array  $dateEvents
     * @param  int $eventCycleId
     * @return array
     */
    public function createOrUpdate(
        $cycleId,
        $typeEventId,
        array $thresholds,
        array $fields,
        array $dateEvents,
        $eventCycleId = null   
    )
    {
        $data = [
            'cycleId' => $cycleId,
            'typeEventId' => $typeEventId,
            'thresholds' => $thresholds,
            'fields' => $fields,
            'dateEvents' => $dateEvents
        ];
        
        if (! is_null($eventCycleId)) {
            $data['eventCycleId'] = $eventCycleId;
        }

        return $this
            ->setRouter(['geo', 'agroclimapro','event','add'])
            ->post($data)
            ->exec();
    }

    /**
     * @param $eventId
     * @param $dateEvent
     * @param $subscriptionId
     * @return mixed
     * @throws \Exception
     */
    public function remove($eventId, $dateEvent, $subscriptionId)
    {
        $queryString = "?" . http_build_query([
                'subscriptionId' => $subscriptionId,
                'dateEvent' => $dateEvent,
                'id' => $eventId
            ]);

        return $this
            ->setRouter(['geo', 'agroclimapro','event','remove'])
            ->addQueryString($queryString)
            ->request();
    }
}
