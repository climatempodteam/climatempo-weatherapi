<?php

namespace WeatherApi\Retrieve\Geo\Agroclimapro;

use WeatherApi\Retrieve\AbstractRetrieve;

/**
 * Class Pluviometer
 *
 * @package WeatherApi\Retrieve\Agroclimapro
 */
class Pluviometer extends AbstractRetrieve
{
    /**
     * @param $subscriptionId
     * @param $farmId
     * @param $name
     * @param $latitude
     * @param $longitude
     * @return mixed
     * @throws \Exception
     */
    public function insert($subscriptionId, $farmId, $name, $latitude, $longitude)
    {
        $queryString = '?' . http_build_query([
                'subscriptionId' => $subscriptionId,
                'farmId' => $farmId,
                'name' => $name,
                'latitude' => $latitude,
                'longitude' => $longitude
            ]);

        return $this
            ->setRouter(['geo', 'agroclimapro', 'pluviometer', 'save'])
            ->addQueryString($queryString)
            ->request();
    }

    /**
     * @param $subscriptionId
     * @param null $farmId
     * @param bool $forceUpdate
     * @return null|\stdClass
     * @throws \Exception
     */
    public function findBy($subscriptionId, $farmId = null, $forceUpdate = false)
    {
        $queryString = '?' . http_build_query([
                'subscriptionId' => $subscriptionId,
                'farmId' => $farmId
            ]);

        return $this
            ->setRouter(['geo', 'agroclimapro', 'pluviometer', 'list'])
            ->addQueryString($queryString)
            ->request();
    }

    /**
     * @param int $subscriptionId
     * @param \DateTime $date
     * @param array $data
     * @return mixed
     * @throws \Exception
     */
    public function saveData($subscriptionId, \DateTime $date, array $data)
    {
        $queryString = [
            'subscriptionId' => $subscriptionId,
            'date' => $date->format('Y-m-d'),
            'pluviometerData' => $data
        ];

        return $this
            ->setRouter(['geo', 'agroclimapro', 'pluviometer', 'data', 'save'])
            ->post($queryString)
            ->exec();
    }

    /**
     * @param int $subscriptionId
     * @param array $pluviometerIds
     * @param \DateTime $dateBegin
     * @param \DateTime $dateEnd
     * @param bool $forceUpdate
     * @return mixed
     * @throws \Exception
     */
    public function dataFindBy(
        $subscriptionId,
        array $pluviometerIds,
        \DateTime $dateBegin,
        \DateTime $dateEnd
    ) {
        $queryString = '?' . http_build_query([
                'subscriptionId' => $subscriptionId,
                'pluviometerIds' => implode($pluviometerIds, ','),
                'dateBegin' => $dateBegin->format('Y-m-d'),
                'dateEnd' => $dateEnd->format('Y-m-d')
            ]);

        return $this
            ->setRouter(['geo', 'agroclimapro', 'pluviometer', 'data', 'observed'])
            ->addQueryString($queryString)
            ->request();
    }
}
