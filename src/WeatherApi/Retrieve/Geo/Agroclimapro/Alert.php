<?php

namespace WeatherApi\Retrieve\Geo\Agroclimapro;

use WeatherApi\Retrieve\AbstractRetrieve;

/**
 * Class Alert
 *
 * @package WeatherApi\Retrieve\Agroclimapro
 * @author Lucas Porto de Deus <lucas.deus@climatempo.com.br>
 * @version 1.0.1
 */
class Alert extends AbstractRetrieve
{
    /**
     * @param int  $subscriptionId Id da assinatura
     * @param int  $farmId         Id da fazenda
     * @param bool $forceUpdate
     *
     * @return null|\stdClass
     */
    public function findBy($subscriptionId, $farmId, $forceUpdate = false)
    {
        $queryString = '?' . http_build_query([
                'subscriptionId' => $subscriptionId,
                'farmId' => $farmId
            ]);
        return $this
            ->setRouter(['geo', 'agroclimapro', 'monitoring-field', 'by-subscription'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(__METHOD__, $queryString),
                $forceUpdate
            );
    }

    /**
     * @param int  $subscriptionId Id da assinatura
     * @param bool $monitoringIds   Id do monitoramento
     * @param bool $status         Status do talhao
     *
     * @return null|\stdClass
     */
    public function changeStatus($subscriptionId, $monitoringIds, $status)
    {
        $queryString = '?' . http_build_query([
                'subscriptionId' => $subscriptionId,
                'monitoringIds' => $monitoringIds,
                'status' => (string) $status
            ]);

        return $this
            ->setRouter(['geo', 'agroclimapro', 'monitoring-field', 'edit', 'status'])
            ->addQueryString($queryString)
            ->request();
    }

    /**
     * @param int  $subscriptionId Id da assinatura
     * @param array $monitoringIds   Id do monitoramento
     *
     * @return null|\stdClass
     */
    public function remove($subscriptionId, array $monitoringIds)
    {
        $queryString = '?' . http_build_query([
                'subscriptionId' => $subscriptionId,
                'monitoringIds' => $monitoringIds
            ]);

        return $this
            ->setRouter(['geo', 'agroclimapro', 'monitoring-field', 'remove'])
            ->addQueryString($queryString)
            ->request();
    }

    /**
     * @param array $data
     * @return array
     */
    public function insert(array $data)
    {
        $queryString = '?' . http_build_query($data);

        return $this
            ->setRouter(['geo', 'agroclimapro', 'monitoring-field', 'create'])
            ->addQueryString($queryString)
            ->request();
    }

    /**
     * @param array $data
     * @return array
     */
    public function edit(array $data)
    {
        $queryString = '?' . http_build_query($data);

        return $this
            ->setRouter(['geo', 'agroclimapro', 'monitoring-field', 'edit'])
            ->addQueryString($queryString)
            ->request();
    }

    /**
     * @param array $filters
     * @param bool  $forceUpdate
     *
     * @return null|\stdClass
     */
    public function historyFindBy(array $filters, $forceUpdate = false)
    {
        $queryString = '?' . http_build_query($filters);

        return $this
            ->setRouter(['geo', 'agroclimapro', 'history-field', 'list'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(__METHOD__, $queryString),
                $forceUpdate
            );
    }

    /**
     * @param array $filters
     * @param bool  $forceUpdate
     *
     * @return null|\stdClass
     */
    public function listFieldBySubscription(array $filters, $forceUpdate = false)
    {
        $queryString = '?' . http_build_query($filters);

        return $this
            ->setRouter(['geo', 'agroclimapro', 'monitoring-field', 'list-field', 'by-subscription'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(__METHOD__, $queryString),
                $forceUpdate
            );
    }
}
