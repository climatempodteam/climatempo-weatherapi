<?php

namespace WeatherApi\Retrieve\Geo\Agroclimapro;

use WeatherApi\Retrieve\AbstractRetrieve;

/**
 * Class Phytosanitary
 *
 * @package WeatherApi\Retrieve\Agroclimapro\Phytosanitary
 * @author Adilson Ferreira <adilson@climatempo.com.br>
 * @author Lucas de Oliveira <lucas.oliveira@climatempo.com.br>
 * @author Lucas Porto de Deus <lucas.deus@climatempo.com.br>
 * @version 1.0.2
 */
class Phytosanitary extends AbstractRetrieve
{

    /**
     * @todo foi removido o cache dessa requisicao pois esta ocorrendo
     *       o erro conforme reportado na issue #1
     *
     * @param number    $latitude      Latitude do local
     * @param number    $longitude     Longitude do local
     * @param array     $phytosanitary Variavel de pragas do endpoint do mapserver
     * @param bool      $forceUpdate   Verifica se é pra forçar uma atualização ou não
     * @param \DateTime $dateBegin     Data de inicio
     * @param \DateTime $dateEnd       Data de fim
     *
     * @author Adilson Ferreira <adilson@climatempo.com.br>
     * @return object
     */
    public function getPhytosanitary15D(
        $latitude,
        $longitude,
        array $phytosanitary,
        $forceUpdate = false,
        \DateTime $dateBegin = null,
        \DateTime $dateEnd = null
    ) {
        $queryString = '?' . http_build_query([
            'latitude'      => $latitude,
            'longitude'     => $longitude,
            'phytosanitary' => implode(',', $phytosanitary)
        ]);

        if ($dateBegin && $dateEnd) {
            $queryString .= '&' . http_build_query([
                'dateBegin' => $dateBegin->format('Y-m-d'),
                'dateEnd'   => $dateEnd->format('Y-m-d')
            ]);
        }

        return $this
            ->setRouter(['geo', 'phytosanitary'])
            ->addQueryString($queryString)
            ->request();
    }
}
