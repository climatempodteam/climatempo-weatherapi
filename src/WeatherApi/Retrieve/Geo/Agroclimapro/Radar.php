<?php

namespace WeatherApi\Retrieve\Geo\Agroclimapro;

use WeatherApi\Retrieve\AbstractRetrieve;

/**
 * Class Radar
 * @package WeatherApi\Retrieve
 * @subpackage WeatherApi\Retrieve\Geo
 * @author Lucas Porto de Deus <lucas.deus@climatempo.com.br>
 * @version 1.0.1
 */
class Radar extends AbstractRetrieve
{
    /**
     * @param          $subscriptionId
     * @param int      $farmId
     * @param int/null $fieldId
     * @param bool     $forceUpdate
     *
     * @return null|\stdClass
     */
    public function getStatistics($subscriptionId, $farmId, $fieldId = null, $radarName)
    {
        $queryString = '?' . http_build_query([
                'subscriptionId' => $subscriptionId,
                'farmId' => $farmId,
                'fieldId' => $fieldId
            ]);

        if (!empty($radarName)) {
            $queryString .= "&radarName={$radarName}";
        }

        return $this
            ->setRouter(['geo', 'radar', 'statistics'])
            ->addQueryString($queryString)
            ->request();
    }

    /**
     * @param int       $subscriptionId
     * @param int       $farmId
     * @param int       $fieldId
     * @param \DateTime $dateBegin
     * @param \DateTime $dateEnd
     * @param bool      $forceUpdate
     *
     * @return null|\stdClass
     */
    public function getHistoric(
        $subscriptionId,
        $farmId,
        $fieldId,
        \DateTime $dateBegin,
        \DateTime $dateEnd,
        $forceUpdate = false
    ) {
        $queryString = '?' . http_build_query([
                'subscriptionId' => $subscriptionId,
                'farmId' => $farmId,
                'fieldId' => $fieldId,
                'dateBegin' => $dateBegin->format('Y-m-d H:i:s'),
                'dateEnd' => $dateEnd->format('Y-m-d H:i:s')
            ]);

        return $this
            ->setRouter(['geo', 'radar', 'statistics', 'history'])
            ->addQueryString($queryString)
            ->request();
    }
}
