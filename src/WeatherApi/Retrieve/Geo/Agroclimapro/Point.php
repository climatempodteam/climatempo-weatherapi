<?php

namespace WeatherApi\Retrieve\Geo\Agroclimapro;

use WeatherApi\Retrieve\AbstractRetrieve;

/**
 * Class Point
 *
 * @package WeatherApi\Retrieve\Agroclimapro
 * @author Lucas de Oliveira <lucas.oliveira@climatempo.com.br>
 * @author Luiz Goncalves <luiz.goncalves@climatempo.com.br>
 * @author Lucas Porto de Deus <lucas.deus@climatempo.com.br>
 * @version 1.5.0
 */
class Point extends AbstractRetrieve
{

    /**
     * @param $userId
     * @param $productId
     * @param $subscriptionId
     * @return mixed
     * @throws \Exception
     */
    public function getAllByUserId($userId, $productId, $subscriptionId)
    {
        $queryString = '?' . http_build_query([
            'userId' => $userId,
            'productId' => $productId,
            'subscriptionId' => $subscriptionId
        ]);

        return $this
            ->setRouter(['geo', 'agroclimapro', 'point', 'by-user'])
            ->addQueryString($queryString)
            ->request();
    }

    /**
     * @param string $name nome do ponto
     * @param string $latitude latitude do ponto
     * @param string $longitude longitude do ponto
     * @param string $cultures culturas do ponto
     * @param string $farm nome da fazenda
     * @param int $idFarm id da fazenda
     * @param string $typeLocale tipo do local do ponto
     * @param int $userId id do usuario
     * @param int $id id da assinatura
     * @return object
     */
    public function insert(
        $name,
        $latitude,
        $longitude,
        $cultures,
        $farm,
        $idFarm,
        $typeLocale,
        $userId,
        $id
    ) {
        $queryString = "?" . http_build_query([
            'userId'    => $userId,
            'name'      => $name,
            'latitude'  => $latitude,
            'longitude' => $longitude,
            'farm'      => $farm,
            'typeLocale'=> $typeLocale,
            'farmId'    => $idFarm,
            'cultures'  => $cultures,
            'id'        => $id
        ]);

        return $this
            ->setRouter(['geo', 'agroclimapro','point','create'])
            ->addQueryString($queryString)
            ->request();
    }

    /**
     * @param int    $pointId    Id do ponto
     * @param string $name       Nome do ponto
     * @param string $latitude   Latitude
     * @param string $longitude  Longitude
     * @param array  $cultures   Culturas para o ponto
     * @param string $farm       Nome da Fazenda
     * @param int    $farmId     Id da fazenda
     * @param int    $typeLocale Tipo do local
     * @param int    $userId     Id do Usuario
     * @param int    $id         Id da inscrição
     * @param int    $localeId   Id da localixacao
     * @param int    $stationId Id da estação no mongo
     *
     * @return object
     * @throws \Exception
     */
    public function edit(
        $pointId,
        $name,
        $latitude,
        $longitude,
        $cultures,
        $farm,
        $farmId,
        $typeLocale,
        $userId,
        $id,
        $localeId,
        $stationId
    ) {
        $queryString = '?' . http_build_query([
                'pointId'    => $pointId,
                'name'       => $name,
                'latitude'   => $latitude,
                'longitude'  => $longitude,
                'cultures'   => $cultures,
                'farm'       => $farm,
                'farmId'     => $farmId,
                'typeLocale' => $typeLocale,
                'userId'     => $userId,
                'id'         => $id,
                'localeId'   => $localeId,
                'stationId'   => $stationId
            ]);

        return $this
            ->setRouter(['geo', 'agroclimapro','point','edit'])
            ->addQueryString($queryString)
            ->request();
    }

    /**
     * @param int $pointId
     * @param int $userId
     * @return array
     */
    public function remove($pointId, $userId)
    {
        $queryString = "?" . http_build_query([
            'userId' => $userId,
            'id'     => $pointId
        ]);

        return $this
            ->setRouter(['geo', 'agroclimapro','point','remove'])
            ->addQueryString($queryString)
            ->request();
    }
}
