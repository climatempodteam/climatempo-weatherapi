<?php

namespace WeatherApi\Retrieve\Geo\Agroclimapro;

use WeatherApi\Retrieve\AbstractRetrieve;

/**
 * Class Farm
 *
 * @package WeatherApi\Retrieve\Agroclimapro\Point
 * @author Luiz Goncalves <luiz.goncalves@climatempo.com.br>
 * @author Lucas Porto de Deus <lucas.deus@climatempo.com.br>
 * @version 1.3.0
 */
class Farm extends AbstractRetrieve
{

    /**
     * @param $userId
     * @return mixed
     * @throws \Exception
     */
    public function getAllByUserId($userId)
    {
        $queryString = "?" . http_build_query([
            'userId' => $userId
        ]);

        return $this
            ->setRouter(['geo', 'agroclimapro','farm','by-user'])
            ->addQueryString($queryString)
            ->request();
    }

    /**
     * @param $name
     * @param $userId
     * @param $fields
     * @param $subscriptionId
     * @param $cultures
     * @return mixed
     * @throws \Exception
     */
    public function insert($name, $userId, $fields = [], $subscriptionId = null, $cultures = [])
    {
        $queryString = [
                'userId'         => $userId,
                'subscriptionId' => $subscriptionId,
                'cultures'       => $cultures,
                'name'           => $name,
                'fields'         => $fields
            ];


        return $this
            ->setRouter(['geo', 'agroclimapro','farm','create'])
            ->post($queryString)
            ->exec();
    }

    /**
     * @param $userId
     * @param $farmId
     * @param $name
     * @return mixed
     * @throws \Exception
     */
    public function edit($userId, $farmId, $name)
    {
        $queryString = "?" . http_build_query([
            'userId' => $userId,
            'id'     => $farmId,
            'name'   => $name
        ]);

        return $this
            ->setRouter(['geo', 'agroclimapro','farm','edit'])
            ->addQueryString($queryString)
            ->request();
    }

    /**
     * @param $farmId
     * @param $userId
     * @return mixed
     * @throws \Exception
     */
    public function remove($farmId, $userId)
    {
        $queryString = "?" . http_build_query([
            'userId' => $userId,
            'id'     => $farmId
        ]);

        return $this
            ->setRouter(['geo', 'agroclimapro','farm','remove'])
            ->addQueryString($queryString)
            ->request();
    }
}
