<?php

namespace WeatherApi\Retrieve\Geo\Agroclimapro;

use WeatherApi\Retrieve\AbstractRetrieve;

/**
 * Class Cycle
 *
 * @package WeatherApi\Retrieve\Agroclimapro
 * @author Lucas de Oliveira <lucas.oliveira@climatempo.com.br>
 * @author Lucas Porto de Deus <lucas.deus@climatempo.com.br>
 * @author Luiz gonçalves <luiz.goncalves@climatempo.com.br>
 * @version 1.3.1
 */
class Cycle extends AbstractRetrieve
{

    /**
     * @param array $data
     * @return array
     */
    public function insert(array $data)
    {

        return $this
            ->setRouter(['geo', 'agroclimapro', 'cycle', 'create'])
            ->post($data)
            ->exec();
    }

    /**
     * @param array $data
     * @return array
     */
    public function edit(array $data)
    {
        $queryString = '?' . http_build_query([
                'id' => $data['id'],
                'cycleParent' => $data['cycleParent'],
                'idHarvest' => $data['idHarvest'],
                'idPoint' => $data['idPoint'],
                'idSubscription' => $data['idSubscription'],
                'idCulture' => $data['idCulture'],
                'plantationDate' => $data['plantationDate'],
                'qtyDays' => $data['qtyDays'],
                'geneticMaterial' => $data['geneticMaterial'],
                'email' => $data['email'],
                'artificialIrrigation' => $data['artificialIrrigation']
            ]);

        return $this
            ->setRouter(['geo', 'agroclimapro', 'cycle', 'edit'])
            ->addQueryString($queryString)
            ->request();
    }

    /**
     * @param array $data
     * @return array
     */
    public function remove(array $data)
    {
        $queryString = '?' . http_build_query($data);

        return $this
            ->setRouter(['geo', 'agroclimapro', 'cycle', 'remove'])
            ->addQueryString($queryString)
            ->request();
    }

    /**
     * @param int $userId
     * @param int $subscriptionId
     * @param int $harvestId
     * @param int $cycleId
     * @return array
     */
    public function getAllByUserId(
        $userId,
        $subscriptionId = null,
        $harvestId = null,
        $cycleId = null
    ) {
        $queryString = '?' . http_build_query([
            'userId' => $userId,
            'subscriptionId' => $subscriptionId,
            'harvestId' => $harvestId,
            'cycleId' => $cycleId
        ]);

        return $this
            ->setRouter(['geo', 'agroclimapro', 'cycle', 'by-user'])
            ->addQueryString($queryString)
            ->request();
    }

   /**
     * @param array $data
     * @return array
     */
    public function getByPeriod(array $data)
    {
        $queryString = '?' . http_build_query($data);

        return $this
            ->setRouter(['geo', 'agroclimapro', 'cycle', 'by-event'])
            ->addQueryString($queryString)
            ->request();
    }
}
