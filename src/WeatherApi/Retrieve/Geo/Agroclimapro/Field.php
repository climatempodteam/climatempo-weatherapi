<?php

namespace WeatherApi\Retrieve\Geo\Agroclimapro;

use WeatherApi\Retrieve\AbstractRetrieve;

/**
 * Class Field
 *
 * @package WeatherApi\Retrieve\Agroclimapro\Field
 * @author Luiz Goncalves <luiz.goncalves@climatempo.com.br>
 * @version 1.0.0
 */
class Field extends AbstractRetrieve
{

     /**
     * @param $farmId
     * @param $subscriptionId
     * @return mixed
     * @throws \Exception
     */
    public function getAllByFarmId($farmId, $subscriptionId)
    {
        $queryString = "?" . http_build_query([
            'farmId' => $farmId,
            'subscriptionId' => $subscriptionId
        ]);

        return $this
            ->setRouter(['geo', 'agroclimapro','field','by-farm'])
            ->addQueryString($queryString)
            ->request();
    }

    /**
     * @param $name
     * @param $farmId
     * @param $subscriptionId
     * @param $coordinates
     * @return mixed
     * @throws \Exception
     */
   public function insert($name, $farmId, $subscriptionId, $coordinates)
    {
        $queryString = [
            'subscriptionId' => $subscriptionId,
            'name'           => $name,
            'farmId'         => $farmId,
            'coordinates'    => $coordinates
        ];

        return $this
            ->setRouter(['geo', 'agroclimapro','field','create'])
            ->post($queryString)
            ->exec();
    }

    /**
     * @param $id
     * @param $subscriptionId
     * @param $name
     * @param $coordinates
     * @return mixed
     * @throws \Exception
     */
    public function edit($id, $subscriptionId, $name, $coordinates)
    {
        $queryString = [
            'id'             => $id,
            'subscriptionId' => $subscriptionId,
            'name'           => $name,
            'coordinates'    => $coordinates
        ];

        return $this
            ->setRouter(['geo', 'agroclimapro','field','edit'])
            ->post($queryString)
            ->exec();
    }


    /**
     * @param $fieldId
     * @param $subscriptionId
     * @return mixed
     * @throws \Exception
     */
    public function remove($fieldId, $subscriptionId)
    {
        $queryString = "?" . http_build_query([
                'id' => $fieldId,
                'subscriptionId' => $subscriptionId
            ]);

        return $this
            ->setRouter(['geo', 'agroclimapro','field','remove'])
            ->addQueryString($queryString)
            ->request();
    }


}
