<?php

namespace WeatherApi\Retrieve\Geo\Agroclimapro;

use WeatherApi\Retrieve\AbstractRetrieve;

/**
 * Class NDVI
 * @package WeatherApi\Retrieve
 * @subpackage WeatherApi\Retrieve\Geo
 * @author Lucas Porto de Deus <lucas.deus@climatempo.com.br>
 * @version 1.0.1
 */
class NDVI extends AbstractRetrieve
{
    /**
     * @param      $subscriptionId
     * @param      $farmId
     * @param bool $forceUpdate
     *
     * @return null|\stdClass
     */
    public function getStatisticsNDVI($subscriptionId, $farmId, $forceUpdate = false)
    {
        $queryString = '?' . http_build_query([
                'subscriptionId' => $subscriptionId,
                'farmId' => $farmId
            ]);

        return $this
            ->setRouter(['satellite', 'ndvi', 'statistics'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }

    /**
     * @param      $subscriptionId
     * @param      $fieldId
     * @param bool $forceUpdate
     *
     * @return null|\stdClass
     */
    public function getHistogramNDVI($subscriptionId, $fieldId, $forceUpdate = false)
    {
        $queryString = '?' . http_build_query([
                'subscriptionId' =>$subscriptionId,
                'fieldId' => $fieldId
            ]);

        return $this
            ->setRouter(['satellite', 'ndvi', 'histogram'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }

    /**
     * @param           $subscriptionId
     * @param           $fieldId
     * @param \DateTime $dateBegin
     * @param \DateTime $dateEnd
     * @param bool      $forceUpdate
     *
     * @return null|\stdClass
     */
    public function getTemporalEvolution(
        $subscriptionId,
        $fieldId,
        \DateTime $dateBegin,
        \DateTime $dateEnd,
        $forceUpdate = false
    ) {
        $queryString = '?' . http_build_query([
                'subscriptionId' => $subscriptionId,
                'fieldId' => $fieldId,
                'dateBegin' => $dateBegin->format('Y-m-d'),
                'dateEnd' => $dateEnd->format('Y-m-d')
            ]);

        return $this
            ->setRouter(['satellite', 'ndvi', 'statistics', 'search'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }
}
