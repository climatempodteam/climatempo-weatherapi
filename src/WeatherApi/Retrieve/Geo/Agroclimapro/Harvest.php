<?php

namespace WeatherApi\Retrieve\Geo\Agroclimapro;

use WeatherApi\Retrieve\AbstractRetrieve;

/**
 * Class Harvest
 *
 * @package WeatherApi\Retrieve\Geo\Agroclimapro
 * @author Lucas Porto de Deus <lucas.deus@climatempo.com.br>
 * @version 1.0.0
 */
class Harvest extends AbstractRetrieve
{
    /**
     * Cria uma nova safra
     *
     * @param int            $subscriptionId Id da assinatura
     * @param string         $name           Nome da safra
     * @param \DateTime      $dateBegin      Data de início da safra
     * @param \DateTime|null $dateEnd        Data de fim da safra - opcional
     *
     * @return mixed
     */
    public function createHarvest(
        $subscriptionId,
        $name,
        \DateTime $dateBegin,
        \DateTime $dateEnd = null
    ) {
        $parameters = [
            'subscriptionId' => $subscriptionId,
            'name' => $name,
            'dateBegin' => $dateBegin->format('Y-m-d')
        ];

        $dateEnd instanceof \DateTime
            ? $parameters['dateEnd'] = $dateEnd->format('Y-m-d')
            : $parameters['dateEnd'] = '';

        $queryString = '?' . http_build_query($parameters);

        return $this
            ->setRouter(['geo', 'agroclimapro','harvest','create'])
            ->addQueryString($queryString)
            ->request();
    }

    /**
     * Realiza modificações em uma safra existente
     *
     * @param int            id              Id da safra
     * @param int            $subscriptionId Id da assinatura
     * @param string         $name           Nome da safra
     * @param \DateTime      $dateBegin      Data de início da safra
     * @param \DateTime|null $dateEnd        Data de fim da safra - opcional
     *
     * @return mixed
     */
    public function edit(
        $id,
        $subscriptionId,
        $name,
        \DateTime $dateBegin,
        \DateTime $dateEnd = null
    ) {
        $parameters = [
            'id' => $id,
            'subscriptionId' => $subscriptionId,
            'name' => $name,
            'dateBegin' => $dateBegin->format('Y-m-d')
        ];

        $dateEnd instanceof \DateTime
            ? $parameters['dateEnd'] = $dateEnd->format('Y-m-d')
            : $parameters['dateEnd'] = '';

        $queryString = '?' . http_build_query($parameters);

        return $this
            ->setRouter(['geo', 'agroclimapro','harvest','edit'])
            ->addQueryString($queryString)
            ->request();
    }

    /**
     * Deleta uma safra
     *
     * @param int $id              Id da safra
     * @param int $subscriptionId  Id da assinatura
     *
     * @return mixed
     */
    public function remove($id, $subscriptionId)
    {
        $queryString = '?' . http_build_query([
                'id' => $id,
                'subscriptionId' => $subscriptionId
            ]);

        return $this
            ->setRouter(['geo', 'agroclimapro','harvest','remove'])
            ->addQueryString($queryString)
            ->request();
    }

    /**
     * Lista todos os registros de uma determinada assinatura ou uma safra específica
     *
     * @param int      $subscriptionId Id da assinatura
     * @param int|null $id             Id da safra
     *
     * @return mixed
     */
    public function findBySubscription($subscriptionId, $id = null)
    {
        $queryString = '?' . http_build_query([
                'subscriptionId' => $subscriptionId,
                'id' => $id
            ]);

        return $this
            ->setRouter(['geo', 'agroclimapro','harvest','by-subscription'])
            ->addQueryString($queryString)
            ->request();
    }
}
