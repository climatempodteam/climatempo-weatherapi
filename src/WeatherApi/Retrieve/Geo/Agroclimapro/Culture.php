<?php

namespace WeatherApi\Retrieve\Geo\Agroclimapro;

use WeatherApi\Retrieve\AbstractRetrieve;

/**
 * Class Culture
 *
 * @package WeatherApi\Retrieve\Agroclimapro
 * @author Lucas de Oliveira <lucas.oliveira@climatempo.com.br>
 * @version 1.0.0
 */
class Culture extends AbstractRetrieve
{

    /**
     * @param bool $forceUpdate
     * @return null|\stdClass
     */
    public function getAll($forceUpdate = false)
    {
        return $this
            ->setRouter(['geo', 'agroclimapro', 'cultures'])
            ->manageCache(
                $this->formatCacheName(__METHOD__),
                $forceUpdate
            );
    }
}
