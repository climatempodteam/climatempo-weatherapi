<?php

namespace WeatherApi\Retrieve\Geo\History\Observed;

use WeatherApi\Retrieve\AbstractRetrieve;

/**
 * Class Rain
 * 
 * @package WeatherApi\Retrieve\Geo\History\Observed
 * @author Danilo Luz <danilo.luz@climatempo.com.br>
 */
class Rain extends AbstractRetrieve
{
    /**
     * Method used to search rains' history data by lat lon
     * 
     * @param \DateTime $dateBegin
     * @param \DateTime $dateEnd
     * @param string $latitude
     * @param string $longitude
     * @param string $source
     * @param bool $forceUpdate
     * 
     * @return mixed
     */
    public function getDaily(
        \DateTime $dateBegin,
        \DateTime $dateEnd,
        $latitude = null,
        $longitude = null,
        $source = 'merge',
        $idLocale = null,
        $forceUpdate = false
    ) {
        $params = [
            'dateBegin' => $dateBegin->format('Y-m-d'),
            'dateEnd' => $dateEnd->format('Y-m-d'),
            'source' => $source,
        ];

        if (!is_null($idLocale)) {
            $params['idLocale'] = $idLocale;
        } else {
            $params['latitude'] = $latitude;
            $params['longitude'] = $longitude;
        }
        $queryString = '?' . http_build_query($params);

        return $this
            ->setRouter(['geo', 'observed', 'history', 'daily', 'rain'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(__METHOD__, $queryString),
                $forceUpdate
            );
    }

    /**
     * Method used to search rains' history hourly data 
     * 
     * @param \DateTime $dateBegin
     * @param \DateTime $dateEnd
     * @param string $latitude
     * @param string $longitude
     * @param string $idLocale
     * @param string $source
     * @param bool $forceUpdate
     * 
     * @return mixed
     */
    public function getHourly(
        \DateTime $dateBegin,
        \DateTime $dateEnd,
        $latitude = null,
        $longitude = null,
        $source = 'interpolated',
        $idLocale = null,
        $forceUpdate = false
    ) {
        $params = [
            'dateBegin' => $dateBegin->format('Y-m-d H:i'),
            'dateEnd' => $dateEnd->format('Y-m-d H:i'),
            'source' => $source,
        ];

        if (!is_null($idLocale)) {
            $params['idLocale'] = $idLocale;
        } else {
            $params['latitude'] = $latitude;
            $params['longitude'] = $longitude;
        }
        $queryString = '?' . http_build_query($params);

        return $this
            ->setRouter(['geo', 'observed', 'history', 'hourly', 'rain'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(__METHOD__, $queryString),
                $forceUpdate
            );
    }
}
