<?php

namespace WeatherApi\Retrieve\Geo\History\Forecast;

use WeatherApi\Retrieve\AbstractRetrieve;

/**
 * Class Temperature
 * @package WeatherApi\Retrieve\Geo\History\Forecast
 * @author Lucas Porto de Deus <lucas.deus@climatempo.com.br>
 * @version 1.0.0
 */
class Temperature extends AbstractRetrieve
{
    /**
     * Busca dados de histórico de previsão de temperatura diário
     * @param \DateTime $dateBegin
     * @param \DateTime $dateEnd
     * @param string $dataType     Parametros possiveis (modelo, metereologista)
     * @param string $forecastTime Parametros possiveis (1d,2d,3d,4d,5d,6d,7d)
     * @param string $latitude
     * @param string $longitude
     * @param bool $forceUpdate
     * @return mixed
     * @throws \Exception
     */
    public function getDaily(
        \DateTime $dateBegin,
        \DateTime $dateEnd,
        $dataType,
        $forecastTime,
        $latitude,
        $longitude,
        $forceUpdate = false
    ) {
        $queryString = '?' . http_build_query([
                'dateBegin' => $dateBegin->format('Y-m-d'),
                'dateEnd' => $dateEnd->format('Y-m-d'),
                'dataType' => $dataType,
                'forecastTime' => $forecastTime,
                'latitude' => $latitude,
                'longitude' => $longitude,
            ]);

        return $this
            ->setRouter(['geo', 'forecast', 'history', 'temperature', 'daily'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }
}
