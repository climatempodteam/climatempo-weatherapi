<?php

namespace WeatherApi\Retrieve\Geo;

use WeatherApi\Retrieve\AbstractRetrieve;

/**
 * Class Satellite
 *
 * 337;"Sul"
 * 334;"Nordeste"
 * 332;"Centro-Oeste"
 * 336;"Sudeste"
 * 335;"Norte"
 * 327;"ND"
 *
 * @package WeatherApi\Retrieve\Satellite
 */
class Satellite extends AbstractRetrieve
{

    /**
     * @param number $idLocale
     * @param \DateTime $begin
     * @param \DateTime|null $end
     * @param bool $forceUpdate
     * @return mixed
     */
    public function getClimateRegion($idLocale, \DateTime $begin, \DateTime $end = null, $forceUpdate = false)
    {
        $queryString = "?idlocale={$idLocale}&dateBegin={$begin->format("Y-m-d")}";

        if (!is_null($end)) {
            $queryString .= "&dateEnd={$end->format("Y-m-d")}";
        }

        return $this
            ->setRouter(['climate', 'region'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }


    /**
     * @param \DateTime $begin
     * @param \DateTime|null $end
     * @param bool $forceUpdate
     * @return mixed
     */
    public function getTSM(\DateTime $begin, \DateTime $end = null, $forceUpdate = false)
    {
        $queryString = "?dateBegin={$begin->format("Y-m")}";

        if (!is_null($end)) {
            $queryString .= "&dateEnd={$end->format("Y-m")}";
        }

        return $this
            ->setRouter(['climate', 'tsm'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }

    /**
     * @param bool $forceUpdate
     * @return mixed
     */
    public function getAgricultural($forceUpdate = false)
    {
        return $this
            ->setRouter(['satellite', 'agricultural'])
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__
                ),
                $forceUpdate
            );
    }


    /**
     * Obtem imagens de satelite
     *
     * @param bool|false $forceUpdate
     * @return \stdClass|null
     */
    public function getForecast($forceUpdate = false)
    {
        return $this
            ->setRouter(['satellite', 'forecast'])
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__
                ),
                $forceUpdate
            );
    }


    /**
     * @param string $locale
     * @param bool $forceUpdate
     * @return mixed
     */
    public function getByLocale($locale, $forceUpdate = false)
    {
        $queryString = "?locale={$locale}";

        return $this
            ->setRouter(['satellite', 'images'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }


    /**
     * @param string $style
     * @param bool|false $forceUpdate
     * @return \stdClass|null
     */
    public function getByStyle($style, $forceUpdate = false)
    {
        $queryString = "?style={$style}";

        return $this
            ->setRouter(['satellite', 'images'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }


    /**
     * @param string $locale
     * @param string $style
     * @param bool $forceUpdate
     * @return array|\stdClass
     */
    public function getByLocaleAndStyle($locale, $style, $forceUpdate = false)
    {
        $queryString = "?locale={$locale}&style={$style}";

        return $this
            ->setRouter(['satellite', 'images'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }


    /**
     * @param bool|false $forceUpdate
     * @return \stdClass|null
     */
    public function getText($forceUpdate = false)
    {
        return $this
            ->setRouter(['satellite', 'text'])
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__
                ),
                $forceUpdate
            );
    }


    /**
     * @param bool|false $forceUpdate
     * @return mixed
     */
    public function getRegionForecastText($forceUpdate = false)
    {
        return $this
            ->setRouter(['forecast', 'region'])
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__
                ),
                $forceUpdate
            );
    }

}