<?php

namespace WeatherApi\Retrieve\Geo;

use WeatherApi\Retrieve\AbstractRetrieve;

/**
 * Class Forecast Responsável por pegar os dados de previsão georreferenciados
 * @package WeatherApi\Retrieve\Geo
 *
 * @version 1.1.0
 */
class SprayWindow extends AbstractRetrieve
{

    /**
     * Pega previsão horária georreferenciada
     *
     * @param String $latitude latitude do local
     * @param String $longitude longitude do local
     * @param \DateTime $dateBegin Data de inicio da busca
     * @param \DateTime $dateEnd Data de fim da busca
     * @param string $timezone fuso horario
     * @param bool $forceUpdate verifica se é pra forçar uma atualização ou não
     * @return null|\stdClass
     */
    public function getHourly(
        $latitude, 
        $longitude, 
        \DateTime $dateBegin, 
        \DateTime $dateEnd, 
        $timezone = null,
        $forceUpdate = false
    ) {
        $queryString = "?".http_build_query([
            "latitude" => $latitude,
            "longitude" => $longitude,
            "dateBegin" => $dateBegin->format("Y-m-d"),
            "dateEnd" => $dateEnd->format("Y-m-d"),
            "timezone" => $timezone
        ]);

        return $this
            ->setRouter(['geo', 'spray-window-hourly-by-period'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }
}