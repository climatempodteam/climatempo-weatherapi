<?php

namespace WeatherApi\Retrieve\Geo;

use WeatherApi\Retrieve\AbstractRetrieve;

/**
 * Class City
 *
 * @author Gustavo Santos <gustavo.santos@climatempo.com.br>
 * @author Lucas de Oliveira <lucas.oliveira@climatempo.com.br>
 * @author Danilo Luz <danilo.luz@climatempo.com.br>
 * @version 1.3.0
 *
 * @package WeatherApi\Retrieve\Geo
 */
class City extends AbstractRetrieve
{

    /**
     * Pega a localidade pela latitude e longitude
     *
     * @param $latitude
     * @param $longitude
     * @param bool $forceUpdate
     * @return null|\stdClass
     */
    public function getCityByLatLon($latitude, $longitude, $forceUpdate = false)
    {
        $arrayLatLon = [
            'latitude' => $latitude,
            'longitude' => $longitude
        ];
        $queryString = '?' . http_build_query($arrayLatLon);

        return $this
            ->setRouter(['geo', 'city'])
            ->addQueryString($queryString)
            ->request();
    }

    /**
     * Obtém cidade passando por estado
     *
     * @param string $uf      Sigla do estado
     * @return null|\stdClass
     */
    public function getCityByUf($uf = '', $forceUpdate = false)
    {
        $arrayUF = [
            'uf' => $uf
        ];
        $queryString = '?' . http_build_query($arrayUF);

        return $this
            ->setRouter(['locales', 'cities'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $arrayUF
                ),
                $forceUpdate
            );
    }

    /**
     * Pega a localidade pelo Id Cidade
     *
     * @param $idcity
     * @param bool $forceUpdate
     * @return null|\stdClass
     */
    public function getCityByIdCity($idcity, $forceUpdate = false)
    {
        $arrayParam = [
            'idcity' => $idcity
        ];
        $queryString = '?' . http_build_query($arrayParam);

        return $this
            ->setRouter(['locale', 'city'])
            ->addQueryString($queryString)
            ->request();
    }
}
