<?php

namespace WeatherApi\Retrieve\Geo;

use WeatherApi\Retrieve\AbstractRetrieve;

/**
 * Class WeatherStation Responsável por pegar os dados das estações meteorólogicas
 * @package WeatherApi\Retrieve\Geo
 *
 */
class WeatherStation extends AbstractRetrieve
{
    /**
     * Pegar dados das estações meteorológicas
     *
     * @param $typeStationData Tipo de dado que irá retornar da estação
     * @param bool $forceUpdate verifica se é pra forçar uma atualização ou não
     *
     * @return null|\stdClass
     */
    public function getAll($typeStationData, $sources = null, $forceUpdate = false)
    {
        $queryString = '?'.http_build_query(array(
            'typeStationData' => $typeStationData
        ));

        if (! is_null($sources)) {
            $queryString .= '&source=' . implode(',', $sources);
        }

        return $this
            ->setRouter(['geo', 'weather-station'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }

    /**
     * Pegar dados das estações meteorológicas por filtro
     *
     * @param string $typeStationData tipo da variavel
     * @param array $sources tipo de fontes
     * @param array $coordinates coordenadas longitude, latitude
     * @param array $distance raio de busca de cada coordenada passado em metros
     * @param array $stationsIds ids de estações
     * @param int $lastMinutesLimit flag para pegar o ultimo dado que entrou na estacao  
     * 
     * @return mixed
     * @throws \Exception
     */
    public function getByFilter(
        $typeStationData,
        $sources = null,
        $coordinates = null,
        $distance = null,
        $stationsIds = null,
        $lastMinutesLimit = null
    ) {

        $query = [
            'typeStationData' => $typeStationData,
            'distance' => $distance,
            'lastMinutesLimit' => $lastMinutesLimit
        ];

        if (! is_null($sources)) {
            $query['source'] = implode(',', $sources);
        }

        if (! is_null($stationsIds)) {
            $query['stationsIds'] = implode(',', $stationsIds);
        }

        if (! is_null($coordinates)) {
            $coordinates = array_map(function($value){
                return implode(',', $value);
            }, $coordinates);
            $query['coordinates'] = implode(';', $coordinates);
        }

        $queryString = '?'.http_build_query($query);

        return $this
            ->setRouter(['geo', 'weather-station'])
            ->addQueryString($queryString)
            ->request();
    }
}