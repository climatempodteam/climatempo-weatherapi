<?php

namespace WeatherApi\Retrieve\Portal;

use WeatherApi\Retrieve\AbstractRetrieve;

/**
 * Class Pedagua
 *
 * @package WeatherApi\Retrieve\Portal
 * @author  Adilson Ferreira <adilson@climatempo.com.br>
 */
class Pedagua extends AbstractRetrieve
{
    /**
     * Make a call to request number of posts from Pedagua
     *
     * @param string $word Word that will be searched
     * @param int $platformId ID from social network
     * @param int $pastHour Period in hours to search posts
     * @param int $idLocale Locale ID from post location
     * @return \stdClass
     */
    public function getTotal($word, $platformId, $pastHour, $idLocale)
    {

        $postData = [
            'word' => $word,
            'platformId' => $platformId,
            'pastHour'   => $pastHour,
            'idLocale'   => $idLocale
        ];

        $queryString = '?' . http_build_query($postData);

        return $this
            ->setRouter(['portal', 'pedagua', 'social'])
            ->addQueryString($queryString)
            ->request();
    }

}
