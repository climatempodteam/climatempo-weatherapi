<?php

namespace WeatherApi\Retrieve\Portal;

use WeatherApi\Retrieve\AbstractRetrieve;

/**
 * Class JoinUs
 *
 * @package WeatherApi\Retrieve\Portal
 * @author  Gustavo Vitorino <gustavo.vittorino@climatempo.com.br>
 * @version 1.0.0
 */
class JoinUs extends AbstractRetrieve
{
    /**
     * Realiza a chamada para trazer os posts passando um limite
     *
     * @param Integer $limit
     * @param boolean $forceUpdate
     * @return \stdClass
     */
    public function getWithLimit($limit=null, $forceUpdate = false)
    {

        $queryString = "?limit={$limit}";

        return $this
            ->setRouter(['portal','joinus'])
            ->addQueryString($queryString)
            ->request();
    }

    /**
     * Realiza a chamada para trazer um post pelo id
     *
     * @param Integer $id
     * @param boolean $forceUpdate
     * @return \stdClass
     */
    public function getById($id = null, $forceUpdate = false)
    {

        $queryString = "?id={$id}";

        return $this
            ->setRouter(['portal', 'joinus'])
            ->addQueryString($queryString)
            ->request();
    }
}
