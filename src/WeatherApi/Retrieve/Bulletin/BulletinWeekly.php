<?php

namespace WeatherApi\Retrieve\Bulletin;

use WeatherApi\Retrieve\AbstractRetrieve;

/**
 * @author Lucas Porto de Deus <lucas.deus@climatempo.com.br>
 * @author Gustavo Santos Thiago <gustavo.santos@climatempo.com.br>
 * @version 1.0.1
 **/
class BulletinWeekly extends AbstractRetrieve
{
    /**
     * @param int       $localeId
     * @param string    $legend
     * @param \DateTime $date
     *
     * @return \stdClass
     */
    public function save(
        $localeId,
        \DateTime $date,
        $legend
    ) {
        $postData = [
            'localeId' => (int) $localeId,
            'legend'   => (string) $legend,
            'datetime' => $date->format('Y-m-d')
        ];

        $queryString = '?' . http_build_query($postData);

        return $this
            ->setRouter(['event', 'daily', 'save'])
            ->addQueryString($queryString)
            ->request();
    }

    /**
     * @param array     $localeIds
     * @param \DateTime $startDate
     * @param \DateTime $endDate
     *
     * @return null|\stdClass
     */
    public function findByPeriod(
        array $localeIds,
        \DateTime $dateBegin,
        \DateTime $dateEnd,
        $forceUpdate = false
    ) {
        $postData = [
            'localeIds' => implode(',', $localeIds),
            'dateBegin' => $dateBegin->format('Y-m-d'),
            'dateEnd'   => $dateEnd->format('Y-m-d')
        ];

        $queryString = '?' . http_build_query($postData);

        return $this
            ->setRouter(['event', 'daily'])
            ->addQueryString($queryString)
            ->request();
    }
}
