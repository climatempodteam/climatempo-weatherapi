<?php

namespace WeatherApi\Retrieve\Monitoring;

use WeatherApi\Retrieve\AbstractRetrieve;

/**
 * Class LocaleMonitoring
 * @package WeatherApi\Retrieve\Monitoring
 * @author Guilherme Santos <guilhermedossantos91@gmail.com>
 */
class LocaleMonitoring extends AbstractRetrieve
{
    /**
     * @param bool|false $forceUpdate
     * @return array|\stdClass
     */
    public function getAll($forceUpdate = false)
    {
        return $this
            ->setRouter(['monitoring', 'locale'])
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__
                ),
                $forceUpdate
            );
    }
}