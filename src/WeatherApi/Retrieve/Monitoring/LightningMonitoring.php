<?php

namespace WeatherApi\Retrieve\Monitoring;

use WeatherApi\Retrieve\AbstractRetrieve;

/**
 * Class LightningMonitoring
 * @package WeatherApi\Retrieve\Monitoring
 * @author Guilherme Santos <guilhermedossantos91@gmail.com>
 * @author Luiz Gonçalves <luiz.goncalves@climatempo.com.br>
 * @version 1.1.0
 */
class LightningMonitoring extends AbstractRetrieve
{
    /**
     * @param array $numbers
     * @param bool|false $forceUpdate
     * @return array|\stdClass
     */
    public function getByCeps(array $numbers = [], $forceUpdate = false)
    {
        $cepNumber = 0;
        $queryString = '?num=';

        foreach ($numbers as $num) {
            $queryString .= $num . ',';
            $cepNumber .= $num;
        }

        return $this
            ->setRouter(['monitoring', 'lightning', 'cep'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }


    /**
     * @param bool $forceUpdate
     * @return array|\stdClass
     */
    public function getAll($forceUpdate = false)
    {
        return $this
            ->setRouter(['monitoring', 'lightning'])
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__
                ),
                $forceUpdate
            );
    }

    /**
     * Método responsável por acessar a rota de monitoramento de raios e retornar
     * os mesmos de acordo com o periodo definido
     *
     * @param string $lightningFilter
     * @param \DateTime $dateBegin
     * @param \DateTime $dateEnd
     * @param bool $forceUpdate
     * @return array|\stdClass
     */
    public function getByPeriod($lightningFilter, \DateTime $dateBegin, \Datetime $dateEnd, $forceUpdate = false, $sources = '')
    {
        $queryString = '?' . http_build_query([
            'lightingType' => $lightningFilter,
            'dateBegin' => $dateBegin->format('Y-m-d H:i:00'),
            'dateEnd' => $dateEnd->format('Y-m-d H:i:00'),
            'sources' => $sources
        ]);

        return $this
            ->setRouter(['monitoring', 'lightning'])
            ->addQueryString($queryString)
            ->request();
    }

    /**
     * @param bool $forceUpdate
     * @return array|\stdClass
     */
    public function getByTypeLightning($lightningFilter, $forceUpdate = false)
    {
        $queryString ="?lightingType={$lightningFilter}";

        return $this
            ->setRouter(['monitoring', 'lightning'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__
                ),
                $forceUpdate
            );
    }

    /**
     * @param null $lightningFilter
     * @param null $dateBegin
     * @param null $dateEnd
     * @param false $forceUpdate
     * @return mixed
     * @throws \Exception
     */
    public function getAlertLightning(
        $lightningFilter = null,
        $dateBegin = null,
        $dateEnd = null,
        $forceUpdate = false
    ) {
        $request = $this->setRouter(['monitoring', 'lightning', 'alert']);
        $queryString = [];

        if (!is_null($lightningFilter)) {
            $queryString['lightingType'] = $lightningFilter;
        }

        if (!is_null($dateBegin) && !is_null($dateEnd)) {
            $queryString['dateBegin'] = $dateBegin->format('Y-m-d H:i:00');
            $queryString['dateEnd'] = $dateEnd->format('Y-m-d H:i:00');
        }

        if (count($queryString)) {
            $queryString = '?' . http_build_query($queryString);
            $request->addQueryString($queryString);
        }

        return $request->request();
    }
}
