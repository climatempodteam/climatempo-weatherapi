<?php

namespace WeatherApi\Retrieve\Monitoring;

use WeatherApi\Retrieve\AbstractRetrieve;

/**
 * Class WeatherMonitoring
 * @package WeatherApi\Retrieve\Monitoring
 * @author Guilherme Santos <guilhermedossantos91@gmail.com>
 * @author Gustavo Santos Thiago <gustavo.santos@climatempo.com.br>
 * 
 * @version 1.1.0
 */
class WeatherMonitoring extends AbstractRetrieve
{

    /**
     * @param int $idLocale
     * @param bool|false $forceUpdate
     * @return null|\stdClass
     */
    public function get($idLocale, $forceUpdate = false)
    {
        $queryString ="?idlocale={$idLocale}";

        return $this
            ->setRouter(['monitoring', 'weather'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }

    /**
     * @param int $idLocale
     * @return null|\stdClass
     */
    public function getWeatherMetar($idLocale)
    {
        $queryString ="?idlocale={$idLocale}";

        return $this
            ->setRouter(['monitoring', 'weather', 'metar'])
            ->addQueryString($queryString)
            ->request();
    }
}