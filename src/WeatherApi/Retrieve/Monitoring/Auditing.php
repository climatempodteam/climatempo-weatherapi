<?php

namespace WeatherApi\Retrieve\Monitoring;

use WeatherApi\Retrieve\AbstractRetrieve;

/**
 * Class Auditing
 *
 * @package WeatherApi\Retrieve\Monitoring
 * @author  Guilherme Santos <guilhermedossantos91@gmail.com>
 */
class Auditing extends AbstractRetrieve
{

    /**
     * @param \DateTime $begin
     * @param \DateTime|null $end
     * @param bool|false $forceUpdate
     * @return null|\stdClass
     */
    public function getDaily(\DateTime $begin, \DateTime $end = null, $forceUpdate = false)
    {
        $queryString = "?dateBegin={$begin->format("Y-m-d")}";

        if (!is_null($end)) {
            $queryString .= "&dateEnd={$end->format("Y-m-d")}";
        }

        return $this
            ->setRouter(['auditing'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }
}