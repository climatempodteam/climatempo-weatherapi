<?php

namespace WeatherApi\Retrieve\Monitoring;

use WeatherApi\Retrieve\AbstractRetrieve;

/**
 * Class FireMonitoring
 * @package WeatherApi\Retrieve\Monitoring
 * @author Guilherme Santos <guilhermedossantos91@gmail.com>
 */
class FireMonitoring extends AbstractRetrieve
{

    /**
     * @param $source
     * @param false $forceUpdate
     * @return \stdClass|null
     */
    public function getAll($sources = null, $forceUpdate = false)
    {

        $queryString = [];
        $queryString['sources'] = $sources;
        $queryString = "?" . http_build_query($queryString);

        return $this
            ->setRouter(['geo', 'fire'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );

    }

    /**
     * Busca pelo ID da localidade no sistema clientes
     *
     * @param int        $localeId
     * @param bool|false $forceUpdate
     *
     * @return null|\stdClass
     */
    public function getByLocaleId($localeId, $sources = null, $forceUpdate = false)
    {
        $queryString = [];
        $queryString['localeId'] = $localeId;

        if (!is_null($sources)) {
            $queryString['sources'] = $sources;
        }

        $queryString = "?" . http_build_query($queryString);

        return $this
            ->setRouter(['geo', 'fire'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }

    /**
     * Busca pelo ID da localidade na base de produtos
     *
     * @param int        $pointId
     * @param bool|false $forceUpdate
     *
     * @return null|\stdClass
     */
    public function getByPointId($pointId, $sources = null, $forceUpdate = false)
    {
        $queryString = [];
        $queryString['pointId'] = $pointId;

        if (!is_null($sources)) {
            $queryString['sources'] = $sources;
        }

        $queryString = "?" . http_build_query($queryString);

        return $this
            ->setRouter(['geo', 'fire'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }

    /**
     *
     * @param $interval (hour)
     * @param $radius (meter)
     * @return mixed
     * @throws \Exception
     */
    public function get(
        $interval = null,
        $radius = null,
        $dateBegin = null,
        $dateEnd = null,
        $sources = null
    ) {
        $queryString = [];

        if (!is_null($sources)) {
            $queryString['sources'] = $sources;
        }

        if (!is_null($interval)) {
            $queryString['interval'] = $interval;
        }

        if (!is_null($radius)) {
            $queryString['radius'] = $radius;
        }

        if (!is_null($dateBegin)) {
            $queryString['dateBegin'] = $dateBegin;
        }

        if (!is_null($dateEnd)) {
            $queryString['dateEnd'] = $dateEnd;
        }

        $queryString = "?" . http_build_query($queryString);

        return $this
            ->setRouter(['monitoring', 'fire'])
            ->addQueryString($queryString)
            ->request();
    }
}
