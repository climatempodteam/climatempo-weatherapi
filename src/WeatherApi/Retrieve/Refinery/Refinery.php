<?php

namespace WeatherApi\Retrieve\Refinery;

use WeatherApi\Retrieve\AbstractRetrieve;

/**
 * Class Refinery
 * @package WeatherApi\Retrieve\Refinery
 */
class Refinery extends AbstractRetrieve
{
    /**
     * Undocumented function
     *
     * @param string $date
     * @param string $localeIds
     * @param bool|false $forceUpdate
     *
     * @return \stdClass|null
     */
    function getForecastTexts(string $date, string $localeIds, $forceUpdate = false)
    {
        $query = ['date' => $date, 'localeIds' => $localeIds];

        $queryString = '?' . http_build_query($query);

        return $this
            ->setRouter(['forecast-texts'])
            ->addQueryString($queryString)
            ->manageCache(
                $this->formatCacheName(
                    __METHOD__,
                    $queryString
                ),
                $forceUpdate
            );
    }
}
