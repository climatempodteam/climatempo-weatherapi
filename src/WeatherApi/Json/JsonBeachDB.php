<?php

namespace WeatherApi\Json;
use WeatherApi\Type\StringType;

/**
 * Class JsonBeachDB
 *
 * @package WeatherApi\Json
 * @author  Guilherme Santos <guilhermedossantos91@gmail.com>
 */
class JsonBeachDB extends JsonDB
{

    /**
     * @var self
     */
    private static $instance;

    /**
     * @return self
     */
    public static function getInstance()
    {
        if (self::$instance instanceof self) {
            return self::$instance;
        }

        return self::$instance = new self();
    }

    /**
     * find one beach by id
     *
     * @param int $id
     * @return null|\stdClass
     */
    public function findBeachById($id)
    {
        $result = null;
        $this->loopData(self::BASE_BEACH, function ($beach) use ($id, &$result) {

            $idBeach = isset($beach->idbeach) ?
                $beach->idbeach : null;

            if ($idBeach === $id) {
                $beach->base = self::BASE_BEACH;
                $result = $beach;
                return true;
            }

            return false;
        });

        return $result;
    }

    /**
     * find beach by id city
     *
     * @param int $id
     * @return null|\stdClass
     */
    public function findBeachByIdCity($id)
    {
        $result = null;
        $this->loopData(self::BASE_BEACH, function ($beach) use ($id, &$result) {

            $idBeach = isset($beach->idbeach) ?
                $beach->idbeach : null;

            if ($idBeach === $id) {
                $beach->base = self::BASE_BEACH;
                $result = $beach;
                return true;
            }

            return false;
        });

        return $result;
    }

    /**
     * find all beaches by name
     *
     * @param string $nameSearch
     * @param int $percentMin
     * @param int $maxResult
     * @return \stdClass[]
     */
    public function findAllBeachesByName($nameSearch, $percentMin = 0, $maxResult = 1000)
    {
        $result = [];

        $this->loopData(self::BASE_BEACH, function ($beach) use ($nameSearch, $percentMin, &$result, $maxResult) {

            $nameBeach = isset($beach->beach) ?
                $beach->beach : null;

            $percent = StringType::similar($nameSearch, $nameBeach);

            // add to result list
            if ($percent >= $percentMin) {
                $beach->base = self::BASE_BEACH;
                $beach->searchPoints = (int)$percent;
                $result[] = $beach;
            }

            return count($result) >= $maxResult;
        });

        // reorder by search points
        usort($result, function ($a, $b) {
            return $a->searchPoints < $b->searchPoints;
        });

        return $result;
    }

    /**
     * find all beaches by UF
     *
     * @param string $uf
     * @return \stdClass[]
     */
    public function findAllBeachesByUf($uf)
    {
        $result = [];
        $cityDb = JsonCityDB::getInstance();

        $this->loopData(self::BASE_BEACH, function ($beach) use ($uf, &$result, $cityDb) {

            $ufState = isset($beach->uf) ?
                $beach->uf : null;

            if (strtolower($uf) === strtolower($ufState)) {
                $beach->base = self::BASE_BEACH;
                $beach->city = $cityDb->findCityById($beach->idcity);
                $result[] = $beach;
            }
        });
        return $result;
    }

    /**
     * find all beaches
     *
     * @return \stdClass[]
     */
    public function findAllBeaches()
    {
        return $this->fetchData(self::BASE_BEACH);
    }
}