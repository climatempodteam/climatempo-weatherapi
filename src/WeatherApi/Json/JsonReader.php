<?php

namespace WeatherApi\Json;

/**
 * Class JsonReader
 * @package WeatherApi\Json
 */
class JsonReader
{

    /**
     * @var mixed
     */
    protected $data;

    /**
     * JsonReader constructor.
     * @param string $file
     */
    public function __construct($file)
    {
        $this->data = $this->validate($file);
    }


    /**
     * @param string $file
     * @return mixed
     * @throws \Exception
     */
    private function validate($file)
    {
        if(!file_exists($file)) {
            throw new \Exception("File not found:{$file}");
        }

        if(($data = json_decode(@file_get_contents($file))) === false) {
            throw new \Exception("Invalid JSON: {$file}");
        }

        return $data;
    }


    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

}