<?php

namespace WeatherApi\Json;

use WeatherApi\Type\StringType;

/**
 * Class JsonCityDB
 *
 * @package WeatherApi\Json
 * @author  Guilherme Santos <guilhermedossantos91@gmail.com>
 */
class JsonCityDB extends JsonDB
{

    /**
     * @var self
     */
    private static $instance;

    /**
     * @return self
     */
    public static function getInstance()
    {
        if (self::$instance instanceof self) {
            return self::$instance;
        }

        return self::$instance = new self();
    }

    /**
     * find city by id
     *
     * @param int $id
     * @return null|\stdClass
     */
    public function findCityById($id)
    {
        $result = null;
        $this->loopData(self::BASE_CITY, function ($city) use ($id, &$result) {

            $idcity = isset($city->idcity) ?
                $city->idcity : null;

            if ($idcity === $id) {
                $city->base = self::BASE_CITY;
                $result = $city;
                return true;
            }

            return false;
        });

        return $result;
    }

    /**
     * find city by id locale
     *
     * @param int $id
     * @return null|\stdClass
     */
    public function findCityByIdLocale($id)
    {
        $result = null;
        $this->loopData(self::BASE_CITY, function ($city) use ($id, &$result) {

            $idLocale = isset($city->idlocale) ?
                $city->idlocale : null;

            if ($idLocale === $id) {
                $city->base = self::BASE_CITY;
                $result = $city;
                return true;
            }

            return false;
        });

        return $result;
    }

    /**
     * find all cities by name
     *
     * @param string $nameSearch
     * @param int $percentMin
     * @param int $maxResult
     * @return \stdClass[]
     */
    public function findAllCitiesByName($nameSearch, $percentMin = 0, $maxResult = 1000)
    {
        $result = [];

        $this->loopData(self::BASE_CITY, function ($city) use ($nameSearch, $percentMin, &$result, $maxResult) {

            $nameCity = isset($city->city) ?
                $city->city : null;

            $percent = StringType::similar($nameSearch, $nameCity);

            // add to result list
            if ($percent >= $percentMin) {
                $city->base = self::BASE_CITY;
                $city->searchPoints = (int)$percent;
                $result[] = $city;
            }

            return count($result) >= $maxResult;
        });

        // reorder by search points
        usort($result, function ($a, $b) {
            return $a->searchPoints < $b->searchPoints;
        });

        return $result;
    }


    /**
     * find all cities by UF
     *
     * @param string $uf
     * @return \stdClass[]
     */
    public function findAllCitiesByUf($uf)
    {
        $result = [];

        $this->loopData(self::BASE_CITY, function ($city) use ($uf, &$result) {

            $ufState = isset($city->uf) ?
                $city->uf : null;

            if (strtolower($uf) === strtolower($ufState)) {
                $city->base = self::BASE_CITY;
                $result[] = $city;
            }
        });

        return $result;
    }

    /**
     * find all cities by id country
     *
     * @param int $id
     * @return \stdClass[]
     */
    public function findAllCitiesByIdCountry($id)
    {
        $result = [];

        $this->loopData(self::BASE_CITY, function ($city) use ($id, &$result) {

            $idCountry = isset($city->idcountry) ?
                $city->idcountry : null;

            if ($idCountry === $id) {
                $city->base = self::BASE_CITY;
                $result[] = $city;
            }
        });

        return $result;
    }

    /**
     * Retorna uma cidade pelo seu slug
     *
     * @param $slug
     * @return null
     */
    public function findCitiesBySlug($slug)
    {
        $conversion = array(
            'á' => 'a', 'à' => 'a', 'ã' => 'a', 'â' => 'a', 'é' => 'e',
            'ê' => 'e', 'í' => 'i', 'ï' => 'i', 'ó' => 'o', 'ô' => 'o', 'õ' => 'o', "ö" => "o",
            'ú' => 'u', 'ü' => 'u', 'ç' => 'c', 'ñ' => 'n', 'Á' => 'A', 'À' => 'A', 'Ã' => 'A',
            'Â' => 'A', 'É' => 'E', 'Ê' => 'E', 'Í' => 'I', 'Ï' => 'I', "Ö" => "O", 'Ó' => 'O',
            'Ô' => 'O', 'Õ' => 'O', 'Ú' => 'U', 'Ü' => 'U', 'Ç' => 'C', 'N' => 'Ñ'
        );

        $slug = explode('-', $slug);
        $city = $slug[0];
        $uf = $slug[1];

        $result = null;
        $this->loopData(self::BASE_CITY, function ($cities) use ($slug, &$result, $city, $uf, $conversion) {
            if (strtolower($cities->uf) === strtolower($uf)) {
                $nameCity = strtr($cities->city, $conversion);
                $nameCity = str_replace(' ', '', $nameCity);
                if (strtolower($nameCity) === strtolower($city)) {
                    $result = $cities;
                }
            }
        });

        return $result;
    }

    /**
     * find all cities
     *
     * @return \stdClass[]
     */
    public function findAllCities()
    {
        return $this->fetchData(self::BASE_CITY);
    }

    /**
     * find all countries
     *
     * @return \stdClass[]
     */
    public function findAllCountries()
    {
        $result = [];

        $this->loopData(self::BASE_CITY, function ($city) use (&$result) {

            $indexResult = 0;
            $totalResult = count($result);
            $countryResult = null;
            $found = 0;

            // country
            $acCountry = isset($city->ac) ? $city->ac : null;
            $idCountry = isset($city->idcountry) ? $city->idcountry : null;
            $nameCountry = isset($city->country) ? $city->country : null;

            for (; $indexResult < $totalResult; $indexResult += 1) {
                $countryResult = $result[$indexResult];

                if ($countryResult->idcountry === $idCountry) {
                    $found++;
                }
            }

            if ($found === 0 && !is_null($idCountry)) {
                $country = new \stdClass();
                $country->acCountry = $acCountry;
                $country->idcountry = $idCountry;
                $country->country = $nameCountry;
                $result[] = $country;
            }
        });

        return $result;
    }


    /**
     * Retorna todas cidades turisticas
     *
     * @return array|mixed
     */
    public function findAllTouristCities()
    {
        $result = [];

        $this->loopData(self::BASE_CITY, function ($cities) use (&$result) {
            $tourist = isset($cities->tourist) ? $cities->tourist : null;

            if ($tourist == true) {
                $cities->base = self::BASE_CITY;
                $result[] = $cities;
            }
        });

        return $result;
    }

    /**
     * Retorna cidades turisticas por região
     *
     * @param $region
     * @return null
     */
    public function findTouristCitiesByRegions($region)
    {
        $result = [];

        $this->loopData(self::BASE_CITY, function ($cities) use ($region, &$result) {
            $reg = isset($cities->region) ? $cities->region : null;
            $tourist = isset($cities->tourist) ? $cities->tourist : null;

            if ($region == $reg && $tourist == true) {
                $cities->base = self::BASE_CITY;
                $result[] = $cities;
            }

            return false;
        });

        return $result;
    }
}