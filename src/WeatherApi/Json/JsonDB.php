<?php

namespace WeatherApi\Json;

/**
 * Class JsonDB
 *
 * @package WeatherApi\Json
 */
class JsonDB
{

    const BASE_CITY = "cities";
    const BASE_STATE = "states";
    const BASE_BEACH = "beaches";
    const BASE_AIRPORT = "airports";
    const BASE_TIDE = "tides";

    /**
     * @return string
     */
    protected function getDBDir()
    {
        return __DIR__ . "/../../../db/";
    }

    /**
     * Load json data from file
     *
     * @param string $table
     * @return array|mixed
     */
    public function fetchData($table)
    {
        $list = [];
        $file = $this->getDBDir() . "/{$table}.json";

        if (file_exists($file)) {
            $content = @file_get_contents($file);
            if ($content !== false) {
                if (($list = json_decode($content)) === false) {
                    return [];
                }
            }
        }
        return $list;
    }

    /**
     * @param array $list
     * @return \Generator
     */
    public function fetchGen(array $list = [])
    {
        $index = 0;
        $total = count($list);
        for (; $index < $total; $index += 1) {
            if ((yield $list[$index]) === true) {
                return;
            }
        }
    }

    /**
     * Loop data
     *
     * @param string $base
     * @param callable $callback
     */
    protected function loopData($base, $callback)
    {
        $list = $this->fetchData($base);
        $index = 0;
        $total = count($list);
        for (; $index < $total; $index += 1) {
            if (!is_callable($callback)) {
                break;
            }
            if ($callback($list[$index])) {
                break;
            }
        }
    }
}