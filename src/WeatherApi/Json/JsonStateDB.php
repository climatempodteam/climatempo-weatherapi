<?php


namespace WeatherApi\Json;

/**
 * Class JsonStatesDB
 *
 * @package WeatherApi\Json
 * @author  Guilherme Santos <guilhermedossantos91@gmail.com>
 */
class JsonStateDB extends JsonDB
{

    /**
     * @var self
     */
    private static $instance;

    /**
     * @return self
     */
    public static function getInstance()
    {
        if (self::$instance instanceof self) {
            return self::$instance;
        }

        return self::$instance = new self();
    }
    
    /**
     * find one state by id
     *
     * @param int $id
     * @return null|\stdClass
     */
    public function findStateById($id)
    {
        $result = null;
        $this->loopData(self::BASE_STATE, function ($state) use ($id, &$result) {

            $idState = isset($state->idstate) ?
                $state->idstate : null;

            if ($idState === $id) {
                $state->base = self::BASE_STATE;
                $result = $state;
                return true;
            }

            return false;
        });

        return $result;
    }

    /**
     * find one state by uf
     *
     * @param string $uf
     * @return null|\stdClass
     */
    public function findStateByUf($uf)
    {
        $result = null;
        $this->loopData(self::BASE_STATE, function ($state) use ($uf, &$result) {

            $ufState = isset($state->uf) ?
                $state->uf : null;

            if ($ufState === $uf) {
                $state->base = self::BASE_STATE;
                $result = $state;
                return true;
            }

            return false;
        });

        return $result;
    }

    /**
     * find all states by region
     *
     * @param string $region
     * @return \stdClass[]
     */
    public function findAllStatesByRegion($region)
    {
        $result = [];
        $this->loopData(self::BASE_STATE, function ($state) use ($region, &$result) {

            $regionState = isset($state->region) ?
                $state->region : null;

            if (strtolower($region) === strtolower($regionState)) {
                $state->base = self::BASE_STATE;
                $result[] = $state;
            }
        });

        return $result;
    }


    /**
     * find state by id locale
     *
     * @param int $id
     * @return null|\stdClass
     */
    public function findStateByIdLocale($id)
    {
        $result = null;
        $this->loopData(self::BASE_STATE, function ($state) use ($id, &$result) {

            $idLocale = isset($state->idlocale) ?
                $state->idlocale : null;

            if ($idLocale === $id) {
                $state->base = self::BASE_STATE;
                $result = $state;
                return true;
            }

            return false;
        });

        return $result;
    }


    /**
     * find all states
     *
     * @return \stdClass[]
     */
    public function findAllStates()
    {
        return $this->fetchData(self::BASE_STATE);
    }
}