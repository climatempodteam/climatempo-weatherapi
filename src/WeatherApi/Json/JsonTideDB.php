<?php

namespace WeatherApi\Json;
use WeatherApi\Type\StringType;

/**
 * Class JsonTideDB
 *
 * @package WeatherApi\Json
 * @author  Guilherme Santos <guilhermedossantos91@gmail.com>
 */
class JsonTideDB extends JsonDB
{
    /**
     * @var self
     */
    private static $instance;

    /**
     * @return self
     */
    public static function getInstance()
    {
        if (self::$instance instanceof self) {
            return self::$instance;
        }

        return self::$instance = new self();
    }

    /**
     * find tide by id
     *
     * @param int $id
     * @return null|\stdClass
     */
    public function findTideById($id)
    {
        $result = null;

        $this->loopData(self::BASE_TIDE, function ($tide) use ($id, &$result) {

            $idTide = isset($tide->idtide) ?
                $tide->idtide : null;

            if ($idTide === $id) {
                $tide->base = self::BASE_TIDE;
                $result = $tide;
                return true;
            }

            return false;
        });

        return $result;
    }

    /**
     * find all tides by UF
     *
     * @param string $uf
     * @return \stdClass[]
     */
    public function findAllTidesByUf($uf)
    {
        $result = [];

        $this->loopData(self::BASE_TIDE, function ($tide) use ($uf, &$result) {

            $ufState = isset($tide->uf) ?
                $tide->uf : null;

            if (strtolower($uf) === strtolower($ufState)) {
                $tide->base = self::BASE_TIDE;
                $result[] = $tide;
            }
        });

        return $result;
    }

    /**
     * find all tides by name
     *
     * @param string $nameSearch
     * @param int $percentMin
     * @param int $maxResult
     * @return \stdClass[]
     */
    public function findAllTidesByName($nameSearch, $percentMin = 0, $maxResult = 1000)
    {
        $result = [];

        $this->loopData(self::BASE_TIDE, function ($tide) use ($nameSearch, $percentMin, &$result, $maxResult) {

            $nameTide = isset($tide->tide) ?
                $tide->tide : null;

            $percent = StringType::similar($nameSearch, $nameTide);

            // add to result list
            if ($percent >= $percentMin) {
                $tide->base = self::BASE_TIDE;
                $tide->searchPoints = (int)$percent;
                $result[] = $tide;
            }

            return count($result) >= $maxResult;
        });

        // reorder by search points
        usort($result, function ($a, $b) {
            return $a->searchPoints < $b->searchPoints;
        });

        return $result;
    }

    /**
     * find all states
     *
     * @return \stdClass[]
     */
    public function findAllTides()
    {
        return $this->fetchData(self::BASE_TIDE);
    }

}