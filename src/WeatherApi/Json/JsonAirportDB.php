<?php

namespace WeatherApi\Json;
use WeatherApi\Type\StringType;

/**
 * Class JsonAirportDB
 *
 * @package WeatherApi\Json
 * @author  Guilherme Santos <guilhermedossantos91@gmail.com>
 */
class JsonAirportDB extends JsonDB
{

    /**
     * @var self
     */
    private static $instance;

    /**
     * @return self
     */
    public static function getInstance()
    {
        if (self::$instance instanceof self) {
            return self::$instance;
        }

        return self::$instance = new self();
    }

    /**
     * find all Airports
     *
     * @return \stdClass[]
     */
    public function findAllAirports()
    {
        return $this->fetchData(self::BASE_AIRPORT);
    }

    /**
     * find airport by id
     *
     * @param int $id
     * @return null|\stdClass
     */
    public function findAirportById($id)
    {
        $result = null;
        $this->loopData(self::BASE_AIRPORT, function ($airport) use ($id, &$result) {

            $idAirport = isset($airport->idairport) ?
                $airport->idairport : null;

            if ($idAirport === $id) {
                $airport->base = self::BASE_AIRPORT;
                $result = $airport;
                return true;
            }

            return false;
        });

        return $result;
    }

    /**
     * find all airports by UF
     *
     * @param string $uf
     * @return \stdClass[]
     */
    public function findAllAirportsByUf($uf)
    {
        $result = [];

        $this->loopData(self::BASE_AIRPORT, function ($airport) use ($uf, &$result) {

            $ufState = isset($airport->uf) ?
                $airport->uf : null;

            if (strtolower($uf) === strtolower($ufState)) {
                $airport->base = self::BASE_AIRPORT;
                $result[] = $airport;
            }
        });

        return $result;
    }

    /**
     * find all airports by name
     *
     * @param string $nameSearch
     * @param int $percentMin
     * @param int $maxResult
     * @return \stdClass[]
     */
    public function findAllAirportsByName($nameSearch, $percentMin = 0, $maxResult = 1000)
    {
        $result = [];

        $this->loopData(self::BASE_AIRPORT, function ($airport) use ($nameSearch, $percentMin, &$result, $maxResult) {

            $nameAirport = isset($airport->airport) ?
                $airport->airport : null;

            $percent = StringType::similar($nameSearch, $nameAirport);

            // add to result list
            if ($percent >= $percentMin) {
                $airport->base = self::BASE_AIRPORT;
                $airport->searchPoints = (int)$percent;
                $result[] = $airport;
            }

            return count($result) >= $maxResult;
        });

        // reorder by search points
        usort($result, function ($a, $b) {
            return $a->searchPoints < $b->searchPoints;
        });

        return $result;
    }

}