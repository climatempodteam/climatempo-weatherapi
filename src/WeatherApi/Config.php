<?php

namespace WeatherApi;

/**
 * Class Config
 *
 * @version 1.3.0
 * @author  Guilherme Santos <guilhermedossantos91@gmail.com>
 * @author  Lucas de Oliveira - lucas.oliveira@climatempo.com.br
 * @author  Luiz Gonçalves - luiz.goncalves@climatempo.com.br
 * @package WeatherApi
 */
class Config
{

    /**
     * Server
     */
    const SERVER_PROD = "http://api.climatempo.com.br/api/v1";
    const SERVER_DEV = "http://api.climatempo.teste.com/api/v1";
    const SERVER_BETA = "http://betaapi.climatempo.com.br/api/v1";

    const VERSION = "1.0.0";

    /**
     * Timezone
     */
    const TIMEZONE_UTC = "UTC";
    const TIMEZONE_LOCAL = "LOCAL";

    /**
     * Cache
     *
     * Atencao:
     *
     *  - O tempo de cache pode nao funcionar caso exista
     *    alguma rota dinamica.
     */
    const CACHE_INFINITE = 9999999999999;
    const CACHE_OFF = 1;
    const CACHE_1MIN = 60;
    const CACHE_5MIN = 300;
    const CACHE_12MIN = 720;
    const CACHE_1H = 3600;
    const CACHE_5H = 18000;
    const CACHE_12H = 43200;
    const CACHE_1D = 86400;

    /**
     * Error
     *
     * Modo default lanca uma excecoa padrao do PHP;
     * Modo Weather API lanca uma excecao personalizada com dados da resposta
     */
    const ERROR_MODE_EXCEPTION_WEATHER_API = 'WeatherApiException';
    const ERROR_MODE_EXCEPTION_DEFAULT = 'DefaultException';

    /**
     * Product
     */
    const PRODUCT_WEBSERVICE = 3;

    /**
     * @var string
     */
    private $client;

    /**
     * @var string
     */
    private $secret;

    /**
     * @var number
     */
    private $key;

    /**
     * @var string
     */
    private $application;

    /**
     * @var string
     */
    private $cacheDir;

    /**
     * @var number
     */
    private $cacheTime;

    /**
     * @var bool
     */
    private $cacheApi;

    /**
     * @var number
     */
    private $product;

    /**
     * @var string
     */
    private $server;

    /**
     * @var string
     */
    private $timezone;

    /**
     * @var bool
     */
    private $encode;

    /**
     * @var string
     */
    private $authToken;

    /**
     * Config constructor.
     */
    public function __construct()
    {
        $this->server = self::SERVER_PROD;
        $this->timezone = self::TIMEZONE_UTC;
        $this->cacheTime = self::CACHE_OFF;
        $this->product = self::PRODUCT_WEBSERVICE;
        $this->enableCacheEncode();
        $this->enableCacheApi();
    }

    /**
     * @return $this
     */
    public function enableCacheEncode()
    {
        $this->encode = true;
        return $this;
    }

    /**
     * @return $this
     */
    public function disableCacheEncode()
    {
        $this->encode = false;
        return $this;
    }

    /**
     * @return bool
     */
    public function isCacheEncode()
    {
        return $this->encode;
    }

    /**
     * @return boolean
     */
    public function isCacheApi()
    {
        return $this->cacheApi;
    }

    /**
     * @return $this
     */
    public function enableCacheApi()
    {
        $this->cacheApi = true;
        return $this;
    }

    /**
     * @return $this
     */
    public function disableCacheApi()
    {
        $this->cacheApi = false;
        return $this;
    }

    /**
     * @return number
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @param number $product
     */
    public function setProduct($product)
    {
        $this->product = $product;
    }

    /**
     * @return string
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * @param string $client
     * @return Config
     */
    public function setClient($client)
    {
        $this->client = $client;
        return $this;
    }

    /**
     * @return string
     */
    public function getSecret()
    {
        return $this->secret;
    }

    /**
     * @param string $secret
     * @return Config
     */
    public function setSecret($secret)
    {
        $this->secret = $secret;
        return $this;
    }

    /**
     * @return number
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * @param number $key
     * @return Config
     */
    public function setKey($key)
    {
        $this->key = $key;
        return $this;
    }

    /**
     * @return string
     */
    public function getApplication()
    {
        return $this->application;
    }

    /**
     * @param string $application
     * @return Config
     */
    public function setApplication($application)
    {
        $this->application = $application;
        return $this;
    }

    /**
     * @return string
     */
    public function getCacheDir()
    {
        return $this->cacheDir;
    }

    /**
     * @param string $cacheDir
     * @return Config
     */
    public function setCacheDir($cacheDir)
    {
        $this->cacheDir = $cacheDir;
        return $this;
    }

    /**
     * @return number
     */
    public function getCacheTime()
    {
        return $this->cacheTime;
    }

    /**
     * @param number $cacheTime
     * @return Config
     */
    public function setCacheTime($cacheTime)
    {
        $this->cacheTime = $cacheTime;
        return $this;
    }

    /**
     * @return string
     */
    public function getServer()
    {
        return $this->server;
    }

    /**
     * @param string $server
     * @return Config
     */
    public function setServer($server)
    {
        $this->server = $server;
        return $this;
    }

    /**
     * @return string
     */
    public function getTimezone()
    {
        return $this->timezone;
    }

    /**
     * @param string $timezone
     * @return $this
     */
    public function setTimezone($timezone)
    {
        $this->timezone = $timezone;
        return $this;
    }

    /**
     * @return string
     */
    public function getAuthToken()
    {
        return $this->authToken;
    }

    /**
     * @param string $authToken
     * @return $this
     */
    public function setAuthToken($authToken)
    {
        $this->authToken = $authToken;
        return $this;
    }

    /**
     * @throws \Exception
     */
    public function validate()
    {
        if (is_null($this->client) && is_null($this->authToken)) {
            throw new \Exception("WeatherApi Config Error: client and/or authToken undefined");
        }

        if (is_null($this->authToken) && is_null($this->key) && is_null($this->secret)) {
            throw new \Exception("WeatherAPI Config error: key and/or secret undefined");
        }

        if (is_null($this->application)) {
            throw new \Exception("WeatherAPI Config error: application undefined");
        }

        if (is_null($this->cacheDir) || !is_dir($this->cacheDir)) {
            throw new \Exception("WeatherAPI Config error: Cache dir not found");
        }
    }
}
