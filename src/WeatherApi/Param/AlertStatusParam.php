<?php


namespace WeatherApi\Param;

/**
 * Class AlertStatusParam
 *
 * @package WeatherApi\Param
 * @author  Guilherme Santos <guilhermedossantos91@gmail.com>
 */
class AlertStatusParam
{
    /**
     * @var string
     */
    const RED = "vermelho";

    /**
     * @var string
     */
    const YELLOW = "amarelo";

    /**
     * @var string
     */
    const GREEN = "verde";


    /**
     * @var int[]
     */
    public static $STATUS = [
        self::RED    => "R",
        self::YELLOW => "Y",
        self::GREEN  => "G"
    ];

    /**
     * @param mixed $key
     *
     * @return int|null
     */
    public static function getStatus($key = null)
    {

        if (array_key_exists($key, self::$STATUS)) {
            return self::$STATUS[$key];
        }

        return null;
    }
}