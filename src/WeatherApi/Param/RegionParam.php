<?php


namespace WeatherApi\Param;

/**
 * Class RegionParam
 *
 * @package WeatherApi\Param
 * @author  Guilherme Santos <guilhermedossantos91@gmail.com>
 */
class RegionParam
{
    const URL_IMAGE = "http://usuarios.climatempo.com.br/diadianovo/aplicacao/diadia_regioes/";

    /**
     * Regiao Brasil (Resumo)
     *
     * @var int
     */
    const BRASIL = 17;

    /**
     * Regiao Sul
     *
     * @var int
     */
    const SUL = 337;

    /**
     * Regiao Sudeste
     *
     * @var int
     */
    const SUDESTE = 336;

    /**
     * Regiao Centro-oeste
     *
     * @var int
     */
    const CENTROOESTE = 332;

    /**
     * Regiao Nordeste
     *
     * @var int
     */
    const NORDESTE = 334;

    /**
     * Regiao Norte
     *
     * @var int
     */
    const NORTE = 335;


    /**
     * Mapa de regioes
     *
     * @var string[]
     */
    public static $regions = [
        self::BRASIL      => "Brasil",
        self::SUL         => "Sul",
        self::SUDESTE     => "Sudeste",
        self::CENTROOESTE => "Centro-Oeste",
        self::NORDESTE    => "Nordeste",
        self::NORTE       => "Norte"
    ];


    /**
     * @var string[]
     */
    public static $regionsAbbreviation = [
        self::BRASIL      => "BR",
        self::SUL         => "S",
        self::SUDESTE     => "SE",
        self::CENTROOESTE => "CO",
        self::NORDESTE    => "NE",
        self::NORTE       => "N"
    ];


    /**
     * Obtem nome da regiao
     *
     * @param int $index
     *
     * @return string
     */
    public static function getRegion($index = null)
    {
        if (!array_key_exists($index, self::$regions)) {
            return null;
        }

        return self::$regions[$index];
    }


    /**
     * Obtem abreviação da regiao
     *
     * @param int $index
     * @return null|string
     */
    public static function getRegionAbbreviation($index = null)
    {
        if (!array_key_exists($index, self::$regionsAbbreviation)) {
            return null;
        }

        return self::$regionsAbbreviation[$index];
    }
}