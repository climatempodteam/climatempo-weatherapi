<?php


namespace WeatherApi\Param;

/**
 * Class ManagerModuleParam
 *
 * @package WeatherApi\Param
 * @author  Luiz Gonçalves <luiz.goncalves@climatempo.com.br>
 * @version 1.1.0
 */
class ManagerModuleParam
{

    /**
     * Pagina de contrato
     *
     * @var int
     */
    const LOGIN_IN_USER_PRODUCT = 1;

    /**
     * Pagina de contrato
     *
     * @var int
     */
    const PAGE_CONTRACT = 2;

    /**
     * Pagina de importação de Contrato
     *
     * @var int
     */
    const PAGE_CONTRACT_IMPORT_USER = 3;

    /**
     * Pagina de relatorio cliente
     *
     * @var int
     */
    const PAGE_REPORT_USER = 4;

    /**
     * Permissão para editar limites da assinatura
     */
    const PAGE_SUBSCRIPTION_LIMIT = 5;

    /**
     * permissão para editar data da assinatura
     */
     const PAGE_SUBSCRIPTION_EDIT_DATE = 6;

    /**
     *  Pagina de cupom
     */
     const PAGE_CUPON = 7;

    /**
     * Graficos no dashboard
     */
    const PAGE_DASHBOARD = 8;

    /**
     * Pagina monitoramento radar
     */
    const PAGE_MONITORING_RADAR = 9;

    /**
     * Pagina Gerenciar Plano
     */
    const PAGE_PLAN_MANAGER = 10;

    /**
     * Permissão para editar logo da assinatura
     */
    const PAGE_SUBSCRIPTION_EDIT_LOGO = 11;
}