<?php

namespace WeatherApi\Param;

/**
 * Class ImageParam
 *
 * @package WeatherApi\Param
 * @author  Guilherme Santos <guilhermedossantos91@gmail.com>
 */
class ImageParam
{

    const STYLE_BM = "bm";
    const STYLE_IR = "ir";
    const STYLE_IRR = "irr";
    const STYLE_VI = "vi";
    const STYLE_RGB = "rgb";

    /**
     * @return \stdClass[]
     */
    public static function getStyles()
    {
        return [
            (object)[
                "style" => self::STYLE_BM,
                "name"  => "Colorida (4Km)"
            ],
            (object)[
                "style" => self::STYLE_IR,
                "name"  => "Infravermelho (4km)"
            ],
            (object)[
                "acronym" => self::STYLE_IRR,
                "name"    => "Infra Colorida (4km)"
            ],
            (object)[
                "style" => self::STYLE_VI,
                "name"  => "Visível (1km)"
            ],
            (object)[
                "style" => self::STYLE_RGB,
                "name"  => "Composta (1km)"
            ]
        ];
    }
}