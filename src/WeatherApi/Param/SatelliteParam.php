<?php


namespace WeatherApi\Param;

/**
 * Class SatelliteParam
 *
 * @package WeatherApi\Param
 * @author  Guilherme Santos <guilhermedossantos91@gmail.com>
 */
class SatelliteParam
{

    /**
     * URL das imagens
     */
    const URL = 'http://imagens.climatempo.com.br/mapas/satelite/g12/';

    /**
     * Inicio e fim das imagens
     */
    const IMAGE_MIN = 1;
    const IMAGE_MAX = 10;

    /**
     * Estilos de imagem
     */
    const STYLE_BM = "bm";
    const STYLE_IR = "ir";
    const STYLE_IRR = "irr";
    const STYLE_VI = "vi";
    const STYLE_RGB = "rgb";

    /**
     * Localidades que tem imagens de satelite
     */
    const LOCALE_AS = "as";
    const LOCALE_BR = "br";
    const LOCALE_ME = "me";
    const LOCALE_CN = "cn";
    const LOCALE_CS = "cs";
    const LOCALE_COE = "coe";
    const LOCALE_NOR = "nor";
    const LOCALE_NDE = "nde";
    const LOCALE_SDE = "sde";
    const LOCALE_SUL = "sul";
    const LOCALE_GSP = "gsp";
    const LOCALE_GRJ = "grj";
    const LOCALE_AC = "ac";
    const LOCALE_AL = "al";
    const LOCALE_AP = "ap";
    const LOCALE_AM = "am";
    const LOCALE_BA = "ba";
    const LOCALE_DF = "df";
    const LOCALE_CE = "ce";
    const LOCALE_ES = "es";
    const LOCALE_GO = "go";
    const LOCALE_MA = "ma";
    const LOCALE_MT = "mt";
    const LOCALE_MS = "ms";
    const LOCALE_MG = "mg";
    const LOCALE_PA = "pa";
    const LOCALE_PB = "pb";
    const LOCALE_PR = "pr";
    const LOCALE_PE = "pe";
    const LOCALE_PI = "pi";
    const LOCALE_RJ = "rj";
    const LOCALE_RS = "rs";
    const LOCALE_RN = "rn";
    const LOCALE_RO = "ro";
    const LOCALE_RR = "rr";
    const LOCALE_SC = "sc";
    const LOCALE_SP = "sp";
    const LOCALE_SE = "se";
    const LOCALE_TO = "to";


    /**
     * @var array
     */
    private static $LOCALES = [
        [
            "acronym" => self::LOCALE_AS,
            "name"    => "América do Sul"
        ],
        [
            "acronym" => self::LOCALE_BR,
            "name"    => "Brasil"
        ],
        [
            "acronym" => self::LOCALE_ME,
            "name"    => "Mercosul"
        ],
        [
            "acronym" => self::LOCALE_CN,
            "name"    => "Centro-Norte do Brasil"
        ],
        [
            "acronym" => self::LOCALE_CS,
            "name"    => "Centro-Sul do Brasil"
        ],
        [
            "acronym" => self::LOCALE_COE,
            "name"    => "Centro-Oeste"
        ],
        [
            "acronym" => self::LOCALE_NOR,
            "name"    => "Norte"
        ],
        [
            "acronym" => self::LOCALE_NDE,
            "name"    => "Nordeste"
        ],
        [
            "acronym" => self::LOCALE_SDE,
            "name"    => "Sudeste"
        ],
        [
            "acronym" => self::LOCALE_SUL,
            "name"    => "Sul"
        ],
        [
            "acronym" => self::LOCALE_GSP,
            "name"    => "Grande São Paulo"
        ],
        [
            "acronym" => self::LOCALE_GRJ,
            "name"    => "Grande Rio"
        ],
        [
            "acronym" => self::LOCALE_AC,
            "name"    => "Acre"
        ],
        [
            "acronym" => self::LOCALE_AL,
            "name"    => "Alagoas"
        ],
        [
            "acronym" => self::LOCALE_AP,
            "name"    => "Amapá"
        ],
        [
            "acronym" => self::LOCALE_AM,
            "name"    => "Amazonas"
        ],
        [
            "acronym" => self::LOCALE_BA,
            "name"    => "Bahia"
        ],
        [
            "acronym" => self::LOCALE_DF,
            "name"    => "Distrito Federal"
        ],
        [
            "acronym" => self::LOCALE_CE,
            "name"    => "Ceará"
        ],
        [
            "acronym" => self::LOCALE_ES,
            "name"    => "Espírito Santo"
        ],
        [
            "acronym" => self::LOCALE_GO,
            "name"    => "Goiás"
        ],
        [
            "acronym" => self::LOCALE_MA,
            "name"    => "Maranhão"
        ],
        [
            "acronym" => self::LOCALE_MT,
            "name"    => "Mato Grosso"
        ],
        [
            "acronym" => self::LOCALE_MS,
            "name"    => "Mato Grosso do Sul"
        ],
        [
            "acronym" => self::LOCALE_MG,
            "name"    => "Minas Gerais"
        ],
        [
            "acronym" => self::LOCALE_PA,
            "name"    => "Pará"
        ],
        [
            "acronym" => self::LOCALE_PB,
            "name"    => "Paraíba"
        ],
        [
            "acronym" => self::LOCALE_PR,
            "name"    => "Paraná"
        ],
        [
            "acronym" => self::LOCALE_PE,
            "name"    => "Pernambuco"
        ],
        [
            "acronym" => self::LOCALE_PI,
            "name"    => "Piauí"
        ],
        [
            "acronym" => self::LOCALE_RJ,
            "name"    => "Rio de Janeiro"
        ],
        [
            "acronym" => self::LOCALE_RS,
            "name"    => "Rio Grande do Sul"
        ],
        [
            "acronym" => self::LOCALE_RN,
            "name"    => "Rio Grande do Norte"
        ],
        [
            "acronym" => self::LOCALE_RO,
            "name"    => "Rondônia"
        ],
        [
            "acronym" => self::LOCALE_RR,
            "name"    => "Roraima"
        ],
        [
            "acronym" => self::LOCALE_SC,
            "name"    => "Santa Catarina"
        ],
        [
            "acronym" => self::LOCALE_SP,
            "name"    => "São Paulo"
        ],
        [
            "acronym" => self::LOCALE_SE,
            "name"    => "Sergipe"
        ],
        [
            "acronym" => self::LOCALE_TO,
            "name"    => "Tocantins"
        ],
    ];


    /**
     * @var array
     */
    private static $STYLES = [
        [
            "acronym" => "bm",
            "name"    => "Colorida (4Km)"
        ],
        [
            "acronym" => "ir",
            "name"    => "Infravermelho (4km)"
        ],
        [
            "acronym" => "irr",
            "name"    => "Infra Colorida (4km)"
        ],
        [
            "acronym" => "vi",
            "name"    => "Visível (1km)"
        ],
        [
            "acronym" => "rgb",
            "name"    => "Composta (1km)"
        ],
    ];

    /**
     * @return array
     */
    public static function getLocales()
    {
        return self::$LOCALES;
    }

    /**
     * @return array
     */
    public static function getStyles()
    {
        return self::$STYLES;
    }
}