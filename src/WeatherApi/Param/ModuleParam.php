<?php


namespace WeatherApi\Param;

/**
 * Class ModuleParam
 *
 * @package WeatherApi\Param
 * @author  Guilherme Santos <guilhermedossantos91@gmail.com>
 * @author  Gustavo Santos <gusanthiagodv@gmail.com>
 * @version 1.3.0
 */
class ModuleParam
{

    /**
     * Alerta para localidades
     *
     * @var int
     */
    const ALERT_LOCALES = 1;

    /**
     * Alerta por CEP (raios)
     *
     * @var int
     */
    const ALERT_CEP = 23;

    /**
     * Queimada
     *
     * @var int
     */
    const BURNED = 11;

    /**
     * Historico para cidades
     *
     * @var int
     */
    const HISTORY_CITIES = 3;

    /**
     * Historico para estações
    */
    const HISTORY_STATIONS = 113;

    /**
     * Historico para estações
    */
    const HISTORY_HYDRO = 115;

    /**
     * Previsao TSM (Temperatura da Superfície do Mar)
     *
     * @var int
     */
    const FORECAST_TSM = 5;

    /**
     * Previsao 72 horas para cidades (id:25)
     *
     * @vat int
     */
    const FORECAST_72HS_CITIES = 2;
    const FORECAST_72HS_CITIES_REAL = 25;

    /**
     * Previsao 15 dias para cidades
     *
     * @var int
     */
    const FORECAST_15DAYS_CITIES = 2;

    /**
     * Previsao 180 dias para cidades
     *
     * @var int
     */
    const FORECAST_180DAYS_CITIES = 79;

    /**
     * Previsao 15 dias para estados
     *
     * @var int
     */
    const FORECAST_15DAYS_STATES = 7;

    /**
     * Previsao para regioes
     *
     * @var int
     */
    const FORECAST_REGIONS = 4;

    /**
     * Previsao para cidades
     *
     * @vat int
     */
    const FORECAST_CITIES = 6;

    /**
     * Previsao Pontual com DOC
     *
     * @var int
     */
    const FORECAST_EXACT_DOC = 8;

    /**
     * Previsao de Risco
     *
     * @var int
     */
    const FORECAST_RISK = 15;

    /**
     * Previsao de perfil do vendo (Altura e Intensidade)
     *
     * @var int
     */
    const FORECAST_WIND_PROFILE = 16;

    /**
     * Previsao de ondas
     *
     * @var int
     */
    const FORECAST_WAVE = 27;

    /**
     * Previsao hidrologica
     *
     * @var int
     */
    const FORECAST_HYDROLOGY = 119;

    /**
     * Previsao landslide
     *
     * @var int
     */
    const FORECAST_LANDSLIDE = 121;
    
    /**
     * Previsao quota
     *
     * @var int
     */
    const FORECAST_QUOTA = 122;

    /**
     * Desconforto termico (?)
     *
     * @var int
     */
    const THERMAL_COMFORT = 9;

    /**
     * Risco de raios
     *
     * @var int
     */
    const RISK_LIGHTNING = 10;

    /**
     * Risco de Incêndio (Fórmula FMA)
     *
     * @var int
     */
    const RISK_FIRE = 12;

    /**
     * Ondas
     *
     * @var int
     */
    const WAVE = 13;

    /**
     * Arquivos
     *
     * @var int
     */
    const FILE = 14;

    /**
     * Dados observados em estações alocadas nas localidades dos clientes
     *
     * @var int
     */
    const DATA_WEATHER_STATION_CLIENT = 17;

    /**
     * Recorde
     *
     * @var int
     */
    const RECORD = 19;

    /**
     * Cidade de referencia
     *
     * @var int
     */
    const REFERENCE_CITY = 20;

    /**
     * Historico
     *
     * @var int
     */
    const HISTORY = 21;

    /**
     * Links
     *
     * @var int
     */
    const LINKS = 22;

    /**
     * Camadas do mapa
     *
     * @var int
     */
    const LAYER_MAP = 18;

    /**
     * Camada de mistura (pasquil)
     *
     * @var int
     */
    const LAYER_MIX_PASQUIL = 24;

    /**
     * Camada DTA (Dangerous Thunderstorm Alerts) - Earth Networks\n
     *
     * @var int
     */
    const LAYER_DTA = 26;

    /**
     * Tábua de Marés
     *
     * @var int
     */
    const TIDE_TABLE = 28;

    /**
     * Mosquito
     */
    const MOSQUITO = 33;

    /**
     * Tempo no momento
     */
    const WEATHER_AT_MOMENT = 59;

    /**
     * Meteoceonografia
     */
    const OFFSHORE_METEOCEANOGRAPHIC = 62;

    /**
     * Riscos Offshore
     */
    const OFFSHORE_RISKS = 63;

    /**
     * Modo aviação
     *
     * @var int
     */
    const AVIATION_MODE = 76;


    /**
     * Modo de gerenciamento meteorológico
     *
     * @var int
     */
    const METEOROLOGICAL_MANAGEMENT_MODE = 80;

    /**
     * Cinzas vulcânicas
     *
     * @var int
     */
    const VOLCANIC_ASH = 114;

    /**
     * Alerta de queimadas
     */
    const ALERT_FIRE = 117;

    /**
     * Alerta de Raios
     */
    const ALERT_LIGHTNING = 118;

    /**
     * Radar
     */
    const RADAR = 65;
}
