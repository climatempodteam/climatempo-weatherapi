<?php

namespace WeatherApi\Param;

/**
 * Class NewsParam
 * @package WeatherApi\Param
 * @author Guilherme Santos <guilhermedossantos91@gmail.com>
 */
class NewsParam
{
    const CALL_CONSULTANCY = "consultoria";
    const CALL_CANAL = "canal";
}