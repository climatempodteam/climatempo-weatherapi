<?php

namespace WeatherApi\Param;

/**
 * Class VariableParam
 *
 * @package WeatherApi\Param
 * @author  Guilherme Santos <guilhermedossantos91@gmail.com>
 * @author  Gustavo Santos <gustavo.santos@climatempo.com.br>
 * 
 * @version 1.2.0
 */
class VariableParam
{
    const TEMPERATURE = 1;
    const RAIN = 2;
    const LIGHTNING = 3;
    const HOURLY_RAIN = 4;
    const HOURLY_TEMPERATURE = 5;
    const HOURLY_WIND = 6;
    const HOURLY_GUST = 7;
    const HUMIDITY = 8;
    const RADIATION = 9;

    const VOLCANIC_ASH_0FL_200FL = 'vulcao_fl200_horario';
    const VOLCANIC_ASH_200FL_350FL = 'vulcao_fl350_horario';
    const VOLCANIC_ASH_350FL_550FL = 'vulcao_fl550_horario';

    const LANDING_ALIAS = 'landing';
    const VISIBILITY_ALIAS = 'visibility';
}