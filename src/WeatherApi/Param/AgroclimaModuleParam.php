<?php


namespace WeatherApi\Param;

/**
 * Class AgroclimaModuleParam
 *
 * @package WeatherApi\Param
 */
class AgroclimaModuleParam
{

    /**
     * Monitoramento
     *
     * @var int
     */
    const MONITORING = 1;

    /**
     * Previsão 72 horas
     *
     * @var int
     */
    const FORECAST_72_HOURS = 5;

    /**
     * Previsão 15 dias
     *
     * @var int
     */
    const FORECAST_15_DAYS = 6;

    /**
     * Comunicação semanal
     *
     * @var int
     */
    const WEEKLY_COMMUNICATION = 7;

    /**
     * Tendência 180 dias
     *
     * @var int
     */
    const TREND_180_DAYS = 8;

    /**
     * Boletim previsão
     *
     * @var int
     */
    const FORECAST_BULLETIN = 9;

    /**
     * Histórico
     * Histórico (1 safra retroativa)
     * Histórico (2 safra retroativa)
     *
     * @var int
     */
    const HISTORY = 10;
    const HISTORY_1_RETROACTIVE_HARVEST = 11;
    const HISTORY_2_RETROACTIVE_HARVEST = 12;

    /**
     * Balanço hídrico
     *
     * @var int
     */
    const HYDRIC_BALANCE = 13;

    /**
     * Alerta Fitosanitário
     *
     * @var int
     */
    const PHYTOSANITARY_ALERT = 14;

    /**
     * Atendimento via chat
     *
     * @var int
     */
    const LIVE_CHAT = 15;

    /**
     * Atendimento via telefone
     *
     * @var int
     */
    const TELEPHONE_CALL = 16;

    /**
     * Assistência meteorológica
     *
     * @var int
     */
    const METEOROLOGICAL_ASSISTANCE = 17;

    /**
     * Previsão climatica
     *
     * @var int
     */
    const FORECAST_CLIMATE = 18;

    /**
     * Previsão agricula
     *
     * @var int
     */
    const FORECAST_AGRICULTURAL = 22;

    /**
     * Historico balanço hidrico de 60 dias
     */
    const HISTORY_BALANC_HYDRIC_60_DAYS = 23;

    /**
     * Gerenciar safra
     */
    const MANAGE_HARVEST = 24;

    /**
     * gerenciar ponto
     */
    const MANAGE_POINT = 4;

    /**
     * Mostrar link do financeiro
     */
    const FINANCIAL = 25;

    /**
     * Dados de Evapotranspiração
     */
    const EVAPOTRANSPIRATION = 26;

    /**
     * Dados de NDVI por talhao
     */
    const NDVI = 27;

    /**
     * Dados de radar por talhão
     */
    const RADAR = 28;

    /**
     * Dados de alerta por talhão
     */
    const ALERT = 29;

    /**
     * Dados de alerta por talhão
     */
    const REMOVE_EDIT_POINT = 2;

    /**
     * tela de validação da previsao
     */
    const VALIDATION_FORECAST = 30;

    /**
     * Camada de tempo no momento
     */
    const LAYER_WEATHER_IN_MOMENT = 31;

    /**
     * Camada de histórico
     */
    const LAYER_HISTORIC = 32;

    /**
     * Pluviômetros
     */
    const PLUVIOMETER = 33;

    /**
     * Gerenciar sub-usuários
     */
    const MANAGER_SUBUSER = 34;
    
     /**
     * Janela de Pulverização
     */
    const SPRAY_WINDOW = 35;
    
     /**
     * Estação meteorologica
     */
    const METEOROLOGICAL_STATION = 36;

    /**
     * Monitoramento Alertas
     *
     * @var int
     */
    const MONITORING_ALERTS = 37;

    /**
     * Camadas GeoServer
     *
     * @var int
     */
    const LAYER_GEOSERVER = 38;


    /**
     * Dados histórico dinâmico retroativo
     *
     * @var int
     */
    const HISTORIC_DYNAMIC_RETROACTIVE = 39;

    /**
     * Camada de chuva próxima hora
     *
     * @var int
     */
    const LAYER_RAIN_NOWCASTING = 41;
    
    /**
     * Chave da opção que representa a busca está ou não limitada por cidade
     */
    const IS_LIMITED_BY_CITY_OPTION = 'isLimitedByCity';
}
