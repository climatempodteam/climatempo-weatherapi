<?php
namespace WeatherApi\V2;

/**
 * Enum of hosts
 * Can be changed to SDL in the future.
 */
abstract class Host
{
    const SERVER_PROD = "https://api-kong.climatempo.com.br/api/v2";
}

class Config
{
    /**
     * @var Host
     */
    private $currentHost;

    /**
     * constructor
     *
     * @param string $host
     */
    public function __construct($host)
    {
        $this->currentHost = $host;
    }

    /**
     * Get current host
     *
     * @return string
     */
    public function getCurrentHost()
    {
        return $this->currentHost;
    }

    /**
     * Get authentication route
     *
     * @return string
     */
    public function getAuthenticationPath()
    {
        return $this->currentHost . "/login";
    }

    /**
     * Factory to Production Envroinment
     *
     * @return Config
     */
    public static function createConfigProd()
    {
        return new static(Host::SERVER_PROD);
    }
  
}
