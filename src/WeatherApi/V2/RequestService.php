<?php

namespace WeatherApi\V2;

use WeatherApi\V2\Auth\Guard;

use GuzzleHttp\Client;
use Psr\Http\Message\RequestInterface;

class RequestService
{
    /**
     * Client HTTP
     *
     * @var Guzzle\Client;
     */
    private $client;

    /**
     * Guard class authenticator
     *
     * @var Guard $guard
     */
    private $guard;

    /**
     * constructor
     *
     * @param Guard $config
     */
    public function __construct(Guard $guard = null)
    {
        $this->guard = $guard;
        $this->client = new Client();
    }

    /**
     * @param RequestInterface $request
     * @return mixed|\Psr\Http\Message\ResponseInterface
     */
    public static function requestWithoutAuth(RequestInterface $request)
    {
        return (new static())->request($request, false);
    }

    /**
     * Make request to Internal API
     *
     * @param RequestInterface $request
     * @param boolean $auth
     * @return mixed|\Psr\Http\Message\ResponseInterface
     */
    public function request(RequestInterface $request, $auth = false)
    {
        if($auth) {
            $request = $request->withUri($this->guard->authenticateUrl($request->getUri()));
        }
        try {
            return $this->client->send($request);
        }catch (\GuzzleHttp\Exception\RequestException $exception) {
            return $exception->getResponse();
        }
    }
}