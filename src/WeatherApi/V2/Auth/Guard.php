<?php


namespace WeatherApi\V2\Auth;

use WeatherApi\V2\Config;
use WeatherApi\V2\Retrieve\RequestService;

use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Uri;

use Exception;

class Guard
{
    /**
     * @var Config
     */
    private $config;

    /**
     * @var string | null
     */
    private $username;

    /**
     * @var string | null
     */
    private $password;

    /**
     * @var string | null
     */
    private $accessToken;

    /**
     * @var string | null
     */
    private $tokenStamp;

    /**
     * Guard constructor.
     *
     * @param string $username
     * @param string $password
     * @param Config $config
     */
    public function __construct($username, $password, Config $config)
    {
        $this->username = $username;
        $this->password = $password;
        $this->config = $config;
    }

    /**
     * @param  $accessToken
     * @return $this
     */
    public function setAccessToken($accessToken)
    {
        $this->accessToken = $accessToken;
        return $this;
    }

    /**
     * @param  $tokenStamp
     * @return $this
     */
    public function setTokenStamp($tokenStamp)
    {
        $this->tokenStamp = $tokenStamp;
        return $this;
    }

    
    /**
     * @param  $oauthToken
     * @param  $redirect
     * @return Request
     */
    public function generateTokenRequest()
    {
        $uri = $this->config->getAuthenticationPath();
        
        return new Request(
            'POST',
            $uri,
            [],
            $this->getFields()
        );
    }

    /**
     * @return Config
     */
    public function getConfig()
    {
        return $this->config;
    }
    
    /**
     * @param  $uri
     * @return Uri
     */
    public function authenticateUrl($uri)
    {
        $this->refreshTokenIfExpired();
        $url = parse_url($this->config->getCurrentHost().$uri);
        // Check if have parameters sended on url
        $hasParam = isset($url['query']) ? '&' : '?';
        
        return new Uri($this->config->getCurrentHost().$uri.$hasParam."access_token=".$this->accessToken);
    }
    
    /**
     * Try make login
     *
     * @return string
     */
    public function tryAuth()
    {
        $request = $this->generateTokenRequest();
        $response = \GuzzleHttp\json_decode(
            (string)RequestService::requestWithoutAuth($request)->getBody()
        );
        return $response;
    }
    
    /**
     * refresh token if expired tokenStamp
     *
     * @return void
     */
    private function refreshTokenIfExpired()
    {
        if(!$this->tokenStamp || ($this->tokenStamp && time() > $this->tokenStamp)) {
            $response = $this->tryAuth();
            // TODO: Check if response is it.
            $this->accessToken = $response->access_token;
            $this->tokenStamp = $response->expires_in + time();
        }
    }
    
    /**
     * Get fields for authentication
     *
     * @return array
     */
    private function getFields()
    {
        return [
            "username" => $this->username,
            "password" => $this->password
        ];
    }
}