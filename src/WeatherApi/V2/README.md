# Weather API V2

The V2 uses Microservices architecture.

## How to use

```php
<?php

require '../../../vendor/autoload.php';

use WeatherApi\V2\Retrieve\RetrieveFactory;
use WeatherApi\V2\Auth\Guard;
use function WeatherApi\V2\createConfigProd;

// Without authentication routes
$factory = new RetrieveFactory(createConfigProd()); // Passing configuration of production
// Or
$factory = new RetrieveFactory(); // Passing no previous configuration

$response = $factory->example()->makeSomething();
assert(is_array($response)); // Pass

// Authentication route
// You can do this in many ways
// Example 1 - Simulating login and requests after it.
$guard = new Guard('username', 'password', createConfigProd());
$response = $guard->tryAuth();

if ($response->statusCode == 401) {
  throw new Exception("Invalid authentication", 1);
}
$guard->setAccessToken($response->access_token);
$guard->setTokenStamp($response->expires_at);

$factoryExample1 = new RetrieveFactory($guard->getConfig()); // Passing same configuration
$factoryExample1->setGuard($guard); // Done - You can pass inside constructor of RetrieveFactory too.

$factoryExample1->example()->makeSomethingAuthenticated(); // Waiting implementation of authentication.
assert(is_array($response)); // Ṕass

```

## Tests

```sh
/climatempo-weatherapi$ ./vendor/bin/phpunit --bootstrap vendor/autoload.php
```