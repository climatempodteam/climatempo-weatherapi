<?php

namespace WeatherApi\V2\Retrieve;

use GuzzleHttp\Psr7\Request;
use function GuzzleHttp\Psr7\stream_for;

use WeatherApi\V2\RequestService;
use WeatherApi\V2\Auth\Guard;
use WeatherApi\V2\Config;

/**
 * Abstract class to make requests authenticated in internal API
 */
abstract class AbstractRetrieve
{
    /**
     * @var Guard
     */
    private $auth;

    /**
     * @var Config
     */
    private $config;

    /**
     * @var RequestService
     */
    protected $http;

    private $defaultHeaders = [
        'Content-Type' => 'application/json'
    ];

    /**
     * Handler constructor.
     *
     * @param Guard $auth
     */
    public function __construct(Guard $auth = null, Config $config = null)
    {
        $this->http = new RequestService($auth);
        $this->auth = $auth;
        $this->config = $config;
    }

    /**
     * @param  array $body
     * @return \GuzzleHttp\Psr7\Stream
     */
    private function encodeBody(array $body = [])
    {
        return stream_for(\GuzzleHttp\json_encode($body));
    }

    /**
     * @param  array $headers
     * @return array
     */
    private function mergeWithDefaultHeaders($headers = [])
    {
        return array_merge($headers, $this->defaultHeaders);
    }

    /**
     * @param string $uri
     * @return string
     */
    private function mergeWithDefaultUri($uri)
    {
        if ($this->config) {
            return $this->config->getCurrentHost() . $uri;
        }
        return $uri;
    }

    /**
     * @param  $uri
     * @param  array $headers
     * @return Request
     */
    protected function get($uri, $params = [], $headers = [])
    {
        $uri = $this->mergeWithDefaultUri($uri);
        // Parsing params
        $hasParam = (bool)$params ? '?' : '';
        if ($hasParam) {
            $params = urldecode(http_build_query($params));
            $uri .= '?' . preg_replace("/[[0-9]+]/", '', $params);
        }

        return new Request(
            'GET',
            $uri, 
            $this->mergeWithDefaultHeaders($headers)
        );
    }

    /**
     * @param  $uri
     * @param  array $body
     * @param  array $headers
     * @return Request
     */
    protected function post($uri, $body = [], $headers = [])
    {
        $uri = $this->mergeWithDefaultUri($uri);
        return new Request('POST', $uri, $this->mergeWithDefaultHeaders($headers), $this->encodeBody($body));
    }

    /**
     * @param  $uri
     * @param  array $body
     * @param  array $headers
     * @return Request
     */
    protected function patch($uri, $body = [], $headers = [])
    {
        $uri = $this->mergeWithDefaultUri($uri);
        return new Request('PATCH', $uri, $this->mergeWithDefaultHeaders($headers), $this->encodeBody($body));
    }

    /**
     * @param  $uri
     * @param  array $body
     * @param  array $headers
     * @return Request
     */
    protected function put($uri, $body = [], $headers = [])
    {
        $uri = $this->mergeWithDefaultUri($uri);
        return new Request('PUT', $uri, $this->mergeWithDefaultHeaders($headers), $this->encodeBody($body));
    }

    /**
     * @param  $uri
     * @param  array $body
     * @param  array $headers
     * @return Request
     */
    protected function delete($uri, $body = [], $headers = [])
    {
        $uri = $this->mergeWithDefaultUri($uri);
        return new Request('DELETE', $uri, $this->mergeWithDefaultHeaders($headers), $this->encodeBody($body));
    }
}