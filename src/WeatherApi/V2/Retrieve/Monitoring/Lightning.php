<?php

namespace WeatherApi\V2\Retrieve\Monitoring;

use WeatherApi\V2\Retrieve\AbstractRetrieve;
use DateTime;

class Lightning extends AbstractRetrieve
{
    /**
     * Make request /monitoring/lightning to get lightning by localeId
     *
     * @param  int|array $localeId
     * @param  string|array $lightningType
     * @param  string|array $source
     * @param  DateTime  $dateBegin
     * @param  DateTime  $dateEnd
     * @return string
     */
    public function getLightningByLocaleId(
        $localeId,
        $lightningType = 'CG',
        $source = 'earth networks',
        DateTime $dateBegin = null,
        DateTime $dateEnd = null
    ) {
        $params = ['localeId' => $localeId, 'lightningType' => $lightningType, 'source' => $source];

        if ($dateBegin && $dateEnd) {
            $params['dateBegin'] = $dateBegin->format('Y-m-d H:i:s');
            $params['dateEnd'] = $dateEnd->format('Y-m-d H:i:s');
        }

        $response = $this->http->request(
            $this->get('/monitoring/lightning', $params),
            false
        );

        return $response->getBody()->getContents();
    }

    /**
     * Make request /monitoring/lightning/count to get lightning count by localeId
     *
     * @param  int|array $localeId
     * @param  string|array $lightningType
     * @param  string|array $source
     * @param  DateTime  $dateBegin
     * @param  DateTime  $dateEnd
     * @return string
     */
    public function getLightningCountByLocaleId(
        $localeId,
        $lightningType = 'CG',
        $source = 'earth networks',
        DateTime $dateBegin = null,
        DateTime $dateEnd = null
    ) {
        $params = ['localeId' => $localeId, 'lightningType' => $lightningType, 'source' => $source];

        if ($dateBegin && $dateEnd) {
            $params['dateBegin'] = $dateBegin->format('Y-m-d H:i:s');
            $params['dateEnd'] = $dateEnd->format('Y-m-d H:i:s');
        }

        $response = $this->http->request(
            $this->get('/monitoring/lightning/count', $params),
            false
        );

        return $response->getBody()->getContents();
    }

    /**
     * Returns data lightning by coverage (lat, lon) of last 24 hours.
     *
     * @param float $latitude
     * @param float $longitude
     * @param integer $distance
     * @param string|array $source
     * @param DateTime $dateBegin
     * @param DateTime $dateEnd
     * @return string
     */
    public function getLightningByCoverage(
        float $latitude,
        float $longitude,
        int $distance,
        $source = 'earth networks',
        DateTime $dateBegin = null,
        DateTime $dateEnd = null
    ) {
        $params = [
            'latitude' => $latitude,
            'longitude' => $longitude,
            'distance' => $distance,
            'source' => $source
        ];

        if ($dateBegin && $dateEnd) {
            $params['dateBegin'] = $dateBegin->format('Y-m-d H:i:s');
            $params['dateEnd'] = $dateEnd->format('Y-m-d H:i:s');
        }

        $response = $this->http->request(
            $this->get('/monitoring/lightning/coverage', $params),
            false
        );

        return $response->getBody()->getContents();
    }

    /**
     * Returns lightning data inside a polygon
     * @param array $geometry Polygon that you want to search inside
     * @param DateTime $dateBegin
     * @param DateTime $dateEnd
     * @param array $sources Array of lightning sources
     * @param int $page
     * @param int $perPage
     * @param array $lightningTypes
     */
    public function getLightningInsidePolygon(
        array $geometry,
        DateTime $dateBegin,
        DateTime $dateEnd,
        $page = 1,
        $perPage = 1000,
        $count = false,
        $sources = ['earth networks'],
        $lightningTypes = ['IC', 'CG']
    ) {
        $params = [
            'geometry' => $geometry,
            'sources' => $sources,
            'page' => $page,
            'perPage' => $perPage,
            'dateBegin' => $dateBegin->format('Y-m-d H:i:s'),
            'dateEnd' => $dateEnd->format('Y-m-d H:i:s'),
            'count' => $count,
            'lightningTypes' => $lightningTypes
        ];

        $response = $this->http->request(
            $this->post('/monitoring/lightning/geojson', $params),
            false
        );

        return $response->getBody()->getContents();
    }


}
