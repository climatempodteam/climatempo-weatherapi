<?php

namespace WeatherApi\V2\Retrieve\Example;

use WeatherApi\V2\Retrieve\AbstractRetrieve;
use GuzzleHttp\Psr7\Request;

/**
 * Example class that example how to use new version of SDK
 */
class Example extends AbstractRetrieve
{
    public function makeSomething()
    {
        return ['ok' => true];
    }

    public function makeSomethingAuthenticated()
    {
        return $this->auth->authenticateUrl();
        // return $this->http->request(new Request('GET', 'localhost'), true);
    }
}