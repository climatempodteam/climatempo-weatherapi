<?php

namespace WeatherApi\V2\Retrieve\History;

use WeatherApi\V2\Retrieve\AbstractRetrieve;
use DateTime;

class Lightning extends AbstractRetrieve
{
    /**
     * Make request /history/lightning to get lightning by localeId
     *
     * @param  int|array $localeId
     * @param  DateTime  $dateBegin
     * @param  DateTime  $dateEnd  
     * @param  string|array $lightningType
     * @param  string|array $source
     * @param  int $page
     * @param  int $perPage
     * @param  string $count STRING BOOL EVAL
     * @return string
     */
    public function getLightningByLocaleId(
        $localeId,
        DateTime $dateBegin,
        DateTime $dateEnd,
        $lightningType = 'CG',
        $source = 'earth networks',
        $page = 1,
        $perPage = 1000,
        $count = "true"
    ) {
        $params = [
            'localeId' => $localeId,
            'lightningType' => $lightningType,
            'source' => $source,
            'page' => $page,
            'perPage' => $perPage,
            'count' => $count,
            'dateBegin' => $dateBegin->format('Y-m-d H:i:s'),
            'dateEnd' => $dateEnd->format('Y-m-d H:i:s')
        ];

        $response = $this->http->request(
            $this->get('/history/lightning', $params),
            false
        );

        return $response->getBody()->getContents();
    }

    /**
     * Returns data lightning by coverage (lat, lon).
     *
     * @param float $latitude
     * @param float $longitude
     * @param integer $distance
     * @param DateTime $dateBegin
     * @param DateTime $dateEnd
     * @param string|array $source
     * @param  int $page
     * @param  int $perPage
     * @param  string $count STRING BOOL EVAL
     * @return string
     */
    public function getLightningByCoverage(
        $latitude,
        $longitude,
        $distance,
        DateTime $dateBegin,
        DateTime $dateEnd,
        $source = 'earth networks',
        $page = 1,
        $perPage = 1000,
        $count = "true"
    ) {
        $params = [
            'latitude' => $latitude,
            'longitude' => $longitude,
            'distance' => $distance,
            'source' => $source,
            'page' => $page,
            'perPage' => $perPage,
            'count' => $count,
            'dateBegin' => $dateBegin->format('Y-m-d H:i:s'),
            'dateEnd' => $dateEnd->format('Y-m-d H:i:s')
        ];

        $response = $this->http->request(
            $this->get('/history/lightning/coverage', $params),
            false
        );

        return $response->getBody()->getContents();
    }

    /**
     * Returns lightning data inside a polygon
     * @param array $geometry Polygon that you want to search inside
     * @param DateTime $dateBegin
     * @param DateTime $dateEnd
     * @param array $sources Array of lightning sources
     * @param int $page
     * @param int $perPage
     * @param array $lightningTypes
     * @param bool $useBq
     */
    public function getLightningInsidePolygon(
        array $geometry,
        DateTime $dateBegin,
        DateTime $dateEnd,
        $page = 1,
        $perPage = 1000,
        $count = false,
        $sources = ['earth networks'],
        $lightningTypes = ['IC', 'CG'],
        $useBq = false
    ) {
        $params = [
            'geometry' => $geometry,
            'sources' => $sources,
            'page' => $page,
            'perPage' => $perPage,
            'dateBegin' => $dateBegin->format('Y-m-d H:i:s'),
            'dateEnd' => $dateEnd->format('Y-m-d H:i:s'),
            'count' => $count,
            'lightningTypes' => $lightningTypes
        ];

        $url = '/history/lightning/geojson';

        if ($useBq) {
            $url .= '?xbq=true';
        }

        $response = $this->http->request(
            $this->post($url, $params),
            false
        );

        return $response->getBody()->getContents();
    }
}
