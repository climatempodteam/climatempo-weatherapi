<?php

namespace WeatherApi\V2\Retrieve\History;

use WeatherApi\V2\Retrieve\AbstractRetrieve;
use DateTime;

class Fire extends AbstractRetrieve
{
    /**
     * Get fire historic by the geometry
     * @param string $theGeom The geometry
     * @param DateTime $dateBegin Starting date
     * @param DateTime $dateEnd Ending date
     */
    public function getHistoricByTheGeom(
        string $theGeom,
        DateTime $dateBegin,
        DateTime $dateEnd
    ) {
        $params = [
            'thegeom' => $theGeom,
            'dateBegin' => $dateBegin->format('Y-m-d H:i:s'),
            'dateEnd' => $dateEnd->format('Y-m-d H:i:s')
        ];

        $response = $this->http->request(
            $this->post('/history/fire-focus/thegeom', $params),
            false
        );

        return $response->getBody()->getContents();
    }

    /**
     * Get fire historic by the geometry
     * @param string $geoJson Geo Json to search
     * @param DateTime $dateBegin Starting date
     * @param DateTime $dateEnd Ending date
     */
    public function getHistoricByGeoJson(
        string $geoJson,
        DateTime $dateBegin,
        DateTime $dateEnd
    ) {
        $params = [
            'geojson' => $geoJson,
            'dateBegin' => $dateBegin->format('Y-m-d H:i:s'),
            'dateEnd' => $dateEnd->format('Y-m-d H:i:s')
        ];

        $response = $this->http->request(
            $this->post('/history/fire-focus/geojson', $params),
            false
        );

        return $response->getBody()->getContents();
    }

    /**
     * Returns Fire Focus data inside a polygon
     * @param array $geometry Polygon that you want to search inside
     * @param DateTime $dateBegin
     * @param DateTime $dateEnd
     * @param boolean $count Wether or not you want to return the total
     * @param int $page
     * @param int $perPage
     */
    public function getFocusInsidePolygon(
        array $geometry,
        DateTime $dateBegin,
        DateTime $dateEnd,
        $page = 1,
        $perPage = 1000,
        $count = false
    ) {
        $params = [
            'geometry' => $geometry,
            'count' => $count,
            'page' => $page,
            'perPage' => $perPage,
            'dateBegin' => $dateBegin->format('Y-m-d H:i:s'),
            'dateEnd' => $dateEnd->format('Y-m-d H:i:s')
        ];

        $response = $this->http->request(
            $this->post('/history/fire-focus/polygon', $params),
            false
        );

        return $response->getBody()->getContents();
    }
}
