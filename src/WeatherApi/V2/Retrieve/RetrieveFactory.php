<?php

namespace WeatherApi\V2\Retrieve;

use WeatherApi\V2\Retrieve\Monitoring\Lightning as LightningMonitoring;
use WeatherApi\V2\Retrieve\History\Lightning as LightningHistory;
use WeatherApi\V2\Retrieve\History\Fire as FireHistory;
use WeatherApi\V2\Retrieve\Example\Example;

use WeatherApi\V2\Config;
use function WeatherApi\V2\createConfigProd;
use WeatherApi\V2\Auth\Guard;

/**
 * Class with responsability that generate instance of retrieves
 */
class RetrieveFactory
{
    /**
     * @var Guard
     */
    private $auth;

    /**
     * @var Config
     */
    private $config;

    /**
     * constructor
     *
     * @param Guard|null $guard
     * @param Config|null $config
     */
    public function __construct(Config $config = null, Guard $guard = null)
    {
        $this->config = $config;
        $this->auth = $guard;
    }

    /**
     * @param  Guard $guard
     * @return $this
     */
    public function setGuard(Guard $guard)
    {
        $this->auth = $guard;
        return $this;
    }

    /**
     * @param Config $config
     * @return $this
     */
    public function setConfig(Config $config)
    {
        $this->config = $config;
        return $this;
    }

    /**
     * @return Example
     */
    public function example()
    {
        return new Example($this->auth, $this->config);
    }

    /**
     * @return LightningMonitoring
     */
    public function monitoringLightning()
    {
        return new LightningMonitoring($this->auth, $this->config);
    }

    /**
     * @return LightningHistory
     */
    public function historyLightning()
    {
        return new LightningHistory($this->auth, $this->config);
    }

    /**
     * @return FireHistory
     */
    public function fireHistory()
    {
        return new FireHistory($this->auth, $this->config);
    }

    /**
     * @param  string $accessToken
     * @param  string $tokenStamp
     * @param  string $username
     * @param  string $password
     * @return static
     */
    public static function initializeWithAccessToken(
        string $accessToken,
        string $tokenStamp,
        string $username,
        string $password
    ) {
        $instance = new static();

        $config = createConfigProd();
        $guard = new Guard($username, $password, $config);
        $guard->setAccessToken($accessToken);
        $guard->setTokenStamp($tokenStamp);

        return $instance->setGuard($guard);
    }
}
