<?php

// require '../../../../vendor/autoload.php';

use PHPUnit\Framework\TestCase;

use WeatherApi\V2\Retrieve\RetrieveFactory;
use WeatherApi\V2\Config;
use WeatherApi\V2\Host;

final class HistoryLightningTest extends TestCase 
{
    private $instance;

    public function __construct()
    {
        parent::__construct();
        $this->instance = (new RetrieveFactory(new Config(Host::SERVER_PROD)))->historyLightning();
    }

    public function testLightningByLocale()
    {
        $dateBegin = new DateTime();
        $dateEnd = new DateTime();
        $dateEnd->modify('-1 day');

        $response = json_decode($this->instance->getLightningByLocaleId(3477, $dateBegin, $dateEnd));
        $this->assertEquals(false, $response->response->error);
    }

    public function testLightningByLocales()
    {
        $dateBegin = new DateTime();
        $dateEnd = new DateTime();
        $dateEnd->modify('-1 day');

        $response = json_decode($this->instance->getLightningByLocaleId([3477, 4545], $dateBegin, $dateEnd));
        $this->assertEquals(false, $response->response->error);
    }

    public function testLightningByCoverage()
    {
        $dateBegin = new DateTime();
        $dateEnd = new DateTime();
        $dateEnd->modify('-1 day');

        $response = json_decode($this->instance->getLightningByCoverage(3.3444, -4.3333, 10, $dateBegin, $dateEnd));
        $this->assertEquals(false, $response->response->error);

    }
}
