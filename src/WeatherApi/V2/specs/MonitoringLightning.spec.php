<?php

// require '../../../../vendor/autoload.php';

use PHPUnit\Framework\TestCase;

use WeatherApi\V2\Retrieve\RetrieveFactory;
use WeatherApi\V2\Config;
use WeatherApi\V2\Host;

final class MonitoringLightningTest extends TestCase 
{
    private $instance;

    public function __construct()
    {
        parent::__construct();
        $this->instance = (new RetrieveFactory(new Config(Host::SERVER_PROD)))->monitoringLightning();
    }

    public function testLightningByLocale()
    {
        $response = json_decode($this->instance->getLightningByLocaleId(3477));
        $this->assertEquals(false, $response->response->error);
    }

    public function testLightningByLocales()
    {
        $response = json_decode($this->instance->getLightningByLocaleId([3477, 4545]));
        $this->assertEquals(false, $response->response->error);
    }

    public function testLightningByLocaleCount()
    {
        $response = json_decode($this->instance->getLightningCountByLocaleId(3477));
        $this->assertEquals(false, $response->response->error);

    }

    public function testLightningByLocalesCount()
    {
        $response = json_decode($this->instance->getLightningCountByLocaleId([3477, 4545]));
        $this->assertEquals(false, $response->response->error);
        
    }

    public function testLightningByCoverage()
    {
        $response = json_decode($this->instance->getLightningByCoverage(3.3444, -4.3333, 10));
        $this->assertEquals(false, $response->response->error);

    }
}
