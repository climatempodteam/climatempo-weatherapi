<?php

namespace WeatherApi\Type;

/**
 * Class StringType
 *
 * @package WeatherApi\Type
 * @author  Guilherme Santos <guilhermedossantos91@gmail.com>
 */
class StringType
{
    /**
     * Converte os html entities individualmente
     *
     * @param String $string
     * @return String
     */
    public static function entitiesDecode($string)
    {
        $entities = [
            "à", "á", "â", "ã", "ä", "å", "ç",
            "è", "é", "ê", "ë", "ì", "í", "î",
            "ï", "ð", "ñ", "ò", "ó", "ô", "õ",
            "ö", "ø", "ù", "ú", "û", "ü", "ý",
            "À", "Á", "Â", "Ã", "Ä", "Å", "Ç",
            "È", "É", "Ê", "Ë", "Ì", "Í", "Î",
            "Ï", "Ò", "Ó", "Ô", "Õ", "Ö", "Ù",
            "Ú", "Û", "Ü"
        ];

        foreach ($entities as $char) {
            $string = str_replace(
                htmlentities($char),
                $char,
                $string
            );
        }

        return $string;
    }

    /**
     * Remove all accents
     *
     * @param string $string
     * @return string
     */
    public static function removeAccent($string)
    {
        $keys = [];
        $values = [];
        preg_match_all('/./u', "áàãâéêíóôõúüçÁÀÃÂÉÊÍÓÔÕÚÜÇ", $keys);
        preg_match_all('/./u', "aaaaeeiooouucAAAAEEIOOOUUC", $values);
        $mapping = array_combine($keys[0], $values[0]);
        $string = strtr($string, $mapping);
        $string = str_replace("'", "", $string);
        $string = strtolower($string);

        return $string;
    }

    /**
     * Get similar text percentage
     *
     * @param string $string1
     * @param string $string2
     * @return float|int
     */
    public static function similar($string1 = null, $string2 = null)
    {

        if (is_null($string1) || empty($string1)) {
            return 0;
        }

        if (is_null($string2) || empty($string2)) {
            return 0;
        }

        // lower case
        $nameSearch = strtolower($string1);
        $nameBeach = strtolower($string2);

        // remove utf-8 characters
        $nameSearch = self::removeAccent($nameSearch);
        $nameBeach = self::removeAccent($nameBeach);

        // position
        $position = strpos($nameBeach, $nameSearch);

        // search
        similar_text($nameSearch, $nameBeach, $percent);
        if ($position !== false) {
            $position = $position === 0 ? 1 : $position;
            $percent += (100 / $position);
        } else {
            $percent -= 30;
        }

        return $percent;
    }

    /**
     * @param string $string
     * @param string $separator
     * @return string
     */
    public static function urlEncode($string, $separator = null)
    {
        $string = strtolower($string);

        $aux = '';
        foreach (explode(" ", $string) as $key => $value) {
            if (!is_null($separator) && $key != 0) {
                $aux .= $separator . $value;
            } else {
                $aux .= $value;
            }
        }

        $aux = self::removeAccent($aux);
        return $aux;
    }
}